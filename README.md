# FLEGE

**FLEGE** (the **F**airly **L**ightweight **E**lectron **G**UI **E**ditor &mdash; pronounced "fledge") is an experimental/proof-of-concept project which attempts to provide an Electron-based GUI editor for the nascent [Bevy Engine](https://github.com/bevyengine), a game engine written in Rust.

## Why?
After years of playing with Unity and Unreal Engine but never being quite satisfied with either C# or C++, Rust and Bevy Engine are like a breath of fresh air, with a clean and concise syntax, lightning fast performance, and a delightfully ergonomic ECS framework.

But Bevy is still very early in development and lacking a GUI editor, and its [UI module](https://docs.rs/bevy_ui/0.5.0/bevy_ui/) &mdash; while already very promising &mdash; is still too barebones to serve as the basis for one. While I frankly don't yet have the confidence with Rust to meaningfully contribute to [that effort](https://github.com/bevyengine/bevy/issues/254), I _do_ have enough experience engineering UI for web applications that I knew I could put together a game engine UI in Electron with relatively minimal time and effort &mdash; _if_ I could figure out how to get that UI to interface with Bevy.

## How?
The meat of the project is split into a few main components:
 * `apps/main` is the Electron application, which contains the TypeScript half of the TS <-> Rust IPC and the core business logic for the editor itself
 * `apps/renderer` is the front-end project, built with [Angular](https://angular.io), which serves as the renderer process for the Electron app
 * `libs/bevy-plugin` contains the Rust half of the TS <-> Rust IPC, and exports a couple of Bevy plugins which bootstrap the ECS systems that manage communications between the Electron app and a consuming Bevy application
 * `libs/api` contains the TS <-> Rust IPC protocol, implemented with [Protocol Buffers](https://developers.google.com/protocol-buffers)

Additionally, the project contains a few libraries to help organize the front-end code:
 * `lib/ui` contains the more generic and unopinionated Angular UI modules used by `apps/renderer` to build out the front-end
 * `lib/styles` is an SCSS library of utilities and common styles used by most of the components in `lib/ui` and `apps/renderer`
 * `lib/utils` is a grab-bag of miscellaneous TypeScript helper functions, classes, and Angular modules used throughout the project

The development backbone of the project is [Nx](https://nx.dev/), a monorepo framework which nicely manages the complexity of the various build systems and glues all of the components together with a nice, ergonomic CLI.

## Development
### Prerequisites
Unfortunately, the project currently **only works on Windows**. Aligning the Bevy app's window with the Electron app's viewport currently involves some janky shenanigans that requires calling out to the native Windows API in a couple of places. If anyone would like to patch it to work on MacOS and/or Linux, I'll happily point you in the right direction and review your PR, but I don't currently have a way to test on those platforms.

1. [NodeJS v14.x](https://nodejs.org/en/) (the current LTS version at time of writing)
1. [Yarn v1.22.5](https://classic.yarnpkg.com/en/docs/install)
1. [Windows Build Tools](https://github.com/felixrieseberg/windows-build-tools)
   > **IMPORTANT NOTE:** I'm not going to lie to you, this can be an unmitigated nightmare to get working. You may or may not have any luck running the "one-line" command from the link above. If you do &mdash; great! You can move on to the next step.
	>
	> If not, you may need to manually install the following, and possibly some other things I'm forgetting at the moment. Google is your friend.
	>
	>  * [Visual Studio 2017 Build Tools](https://stackoverflow.com/a/57808976)
	>  * [Python 2.7](https://www.python.org/download/releases/2.7/)
1. [Rustup](https://www.rust-lang.org/learn/get-started)


Once all of the above is installed, clone the repo to your system and run the following command from the project root, which will install the JS dependencies and execute some post-install scripts to bootstrap the build system:
```sh
yarn
```

### Running the app locally
From the project root, run:
```sh
yarn start
```
