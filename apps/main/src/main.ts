import { app as electron } from "electron";
import * as frameless from "electron-frameless-window-plugin";

import squirrel from "./app/events/squirrel.events";
import events from "./app/events/electron.events";
import updateEvents from "./app/events/update.events";
import app from "./app/app";
import fsAdapter from "./app/fs";
import projects from "./app/projects";

(async function () {

	frameless.plugin({ setGlobal: true });

	if (squirrel.handleEvents())
		electron.quit();

	await app.init();

	events.init();
	fsAdapter.init();
	projects.init();

	if (!app.isDevelopmentMode())
		updateEvents.initAutoUpdateService();

})();
