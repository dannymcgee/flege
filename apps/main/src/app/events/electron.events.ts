import {
	app as electron,
	Display,
	ipcMain,
	Rectangle,
	screen,
	shell,
} from "electron";

import os from "os";
import path from "path";

import { MessageType, request } from "@flege/api";
import { environment } from "../../environments/environment";
import app from "../app";
import { Observable, ReplaySubject } from "rxjs";

export type IpcMessageStream = Observable<{
	msgType: MessageType;
	message: request.Any;
}>;

namespace events {
	// eslint-disable-next-line prefer-let/prefer-let
	export const ipcMessages$: IpcMessageStream = new ReplaySubject(1);

	export function init(): Electron.IpcMain {
		let mainWindow = app.getMainWindow();

		ipcMain.handle("ipc::message", (_: any, { msgType, message }) => {
			if (msgType === MessageType.SetViewportRect) {
				let viewportRect = normalizeViewportRect({ ...message });
				(ipcMessages$ as any).next({ msgType, message: viewportRect });
			}
			else {
				(ipcMessages$ as any).next({ msgType, message });
			}
		})

		mainWindow
			?.on("maximize", () => {
				mainWindow!.webContents.send("win::maximized-change", true);
			})
			.on("unmaximize", () => {
				mainWindow!.webContents.send("win::maximized-change", false);
			})
			.on("moved", () => {
				let bounds = mainWindow!.getBounds();

				if (bounds.y === 0) {
					mainWindow!.maximize();
					mainWindow!.webContents.send("win::maximized-change", true);
				}
			});

		ipcMain.handle("app::get-version", () => environment.version);

		ipcMain.handle("win::toggle-maximized", () => {
			if (mainWindow!.isMaximized()) {
				mainWindow!.unmaximize();
				return false;
			}
			mainWindow!.maximize();
			return true;
		});

		ipcMain.handle("app::open-settings", openSettings);

		ipcMain
			.on("win::minimize", () => {
				mainWindow!.minimize();
			})
			.on("win::close", () => {
				mainWindow!.close();
			})
			.on("win::set-size", (_, { width, height }) => {
				let current = mainWindow!.getBounds();

				let wDiff = width - current.width;
				let hDiff = height - current.height;
				let x = current.x + (wDiff * -1) / 2;
				let y = current.y + (hDiff * -1) / 2;

				mainWindow!.setBounds({ width, height, x, y }, false);
			})
			.on("app::open-devtools", () => {
				mainWindow!.webContents.openDevTools();
			})
			.on("quit", (_, code) => {
				electron.exit(code);
			});

		return ipcMain;
	}

	function openSettings() {
		let home = os.homedir();
		let settingsFile = path.join(home, ".flege/settings.json");

		shell.openExternal(`file://${settingsFile}`);
	}

	function normalizeViewportRect(
		rect: request.SetViewportRect
	): request.SetViewportRect {
		let win = app.getMainWindow()!;
		let bounds = win.getBounds();

		rect.x += bounds.x;

		let displays = screen
			.getAllDisplays()
			.sort((a, b) => {
				if (a.bounds.x < b.bounds.x) return -1;
				return 1;
			});

		let nearestIdx = nearestDisplayIdx(bounds, displays);

		let normX = 0;
		let normY = 0;

		for (let i = 0; i < nearestIdx; i++) {
			let d = displays[i];

			normX += (d.bounds.x + d.bounds.width) * d.scaleFactor;
			rect.x -= (d.bounds.x + d.bounds.width);
		}
		let d = displays[nearestIdx];

		normX += rect.x * d.scaleFactor;
		normY = (rect.y + bounds.y) * d.scaleFactor;

		// FIXME - ???
		if (nearestIdx === 0) {
			normY += 257;
		} else if (nearestIdx === 2) {
			normY += 263;
		}

		return {
			x: Math.round(normX) - 2,
			y: Math.round(normY) - 2,
			width: Math.round(rect.width) + 4,
			height: Math.round(rect.height) + 4,
			scaleFactor: d.scaleFactor,
		};
	}

	function nearestDisplayIdx(bounds: Rectangle, displays: Display[]): number {
		let winCenter = {
			x: bounds.x + (bounds.width / 2),
			y: bounds.y + (bounds.height / 2),
		};

		let displayCenters = displays.map(({ bounds }) => ({
			x: bounds.x + (bounds.width / 2),
			y: bounds.y + (bounds.height / 2),
		}));

		let activeIdx = displayCenters
			.reduce((acc, cur, idx) => {
				let distX = Math.abs(winCenter.x - cur.x);
				let distY = Math.abs(winCenter.y - cur.y);
				let dist = distX + distY;

				if (dist < acc.dist) {
					return { idx, dist };
				}
				return acc;
			}, {
				idx: -1,
				dist: Number.POSITIVE_INFINITY,
			})
			.idx;

		return activeIdx;
	}
}

export default events;
