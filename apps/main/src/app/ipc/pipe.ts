import cp from "child_process";
import { app as electron } from "electron";
import { Observable, Subject } from "rxjs";
import { filter, first, map, share } from "rxjs/operators";
import { Readable, Writable } from "stream";
import varint from "varint";

import {
	MessageType,
	Request,
	request,
	Response,
	response,
	Severity,
} from "@flege/api";

import { sleep } from "@flege/utils";
import app from "../app";

const MSB = 1 << 7;

export interface Pipe {
	tx: Sender;
	rx: Receiver;
	close(): Promise<void>;
}

export interface Sender {
	send(msgType: MessageType.SetViewportRect, message: request.SetViewportRect): Promise<void>;
	send(msgType: MessageType.WindowHandle, message: request.WindowHandle): Promise<void>;
	send(msgType: MessageType.PatchValue, message: request.PatchValue): Promise<void>;
	send(msgType: MessageType.Exit): Promise<void>;
	send(msgType: MessageType, message?: request.Any): Promise<void>;
}

export interface Receiver {
	recv(msgType: MessageType.WindowHandle): Promise<response.WindowHandle>;
	recv(msgType: MessageType.SceneDescription): Promise<response.SceneDescription>;

	recvAll(msgType: MessageType.WindowHandle): Observable<response.WindowHandle>;
	recvAll(msgType: MessageType.SceneDescription): Observable<response.SceneDescription>;
	recvAll(msgType: MessageType.Exit): Observable<void>;
}

class _Sender implements Sender {
	private _sending = false;
	private _exiting = false;

	constructor(
		private _stream: Writable
	) {}

	async send(msgType: MessageType, message?: request.Any) {
		if (this._exiting) return;

		while (this._sending) {
			if (this._exiting) return;
			await sleep(1);
		}

		this._sending = true;

		let header = Request.Header
			.encodeDelimited(new Request.Header({ msgType }))
			.finish();

		await this._write(header);

		if (msgType === MessageType.Exit) {
			this._exiting = true;
			return;
		}

		if (msgType === MessageType.PatchValue) {
			let { entity, component, values } = message as request.PatchValue;

			let request = Request
				.PatchValue
				.encode({ entity, component, values })
				.finish();

			// console.log("Sending message with length:", request.length);
			let delim = varint.encode(request.length);

			// for (let byte of delim) {
			// 	console.log("0b" + byte.toString(2).padStart(8, "0"));
			// }

			let msg = new Uint8Array([
				...delim,
				...request,
			]);

			// await sleep(100);

			await this._write(msg);
		}
		else {
			let request = getRequestType(msgType)!
				.encodeDelimited(message!)
				.finish();

			await this._write(request);
		}

		this._sending = false;
	}

	_destroy() {
		// eslint-disable-next-line no-async-promise-executor
		return new Promise<void>(async (resolve, reject) => {
			try {
				await this.send(MessageType.Exit);
			}
			catch (err) {
				reject(err);
			}

			this._stream.end(resolve);
		});
	}

	private _write(buf: Uint8Array) {
		return new Promise<void>((resolve, reject) => {
			this._stream.write(buf, (err) => {
				if (err) reject(err);
				else resolve();
			});
		});
	}
}

class _Receiver implements Receiver {
	private _destroying = false;
	private _streamSubject$: Subject<any>;
	private _stream$: Observable<{
		msgType: MessageType;
		message: response.Any;
	}>;

	private _reading = false;

	constructor(
		private _stream: Readable
	) {
		this._streamSubject$ = new Subject<any>();
		this._stream$ = this._streamSubject$.pipe(share());

		this._stream.on("readable", async () => {
			if (this._destroying) return;

			try {
				let buf = await this._readBuffer();
				let header = Response.Header.decode(buf);

				buf = await this._readBuffer();
				let response = ((msgType) => {
					switch (msgType) {
					case MessageType.WindowHandle:
						return Response.WindowHandle.decode(buf);
					case MessageType.SceneDescription:
						return Response.SceneDescription.decode(buf);
					default:
						throw new Error(
							`Unexpected MessageType: ${enumKey(MessageType, msgType)}. `
								+ `This message cannot be received.`
						);
					}
				})(header.msgType);

				if (!response) return;

				this._streamSubject$.next({
					msgType: header.msgType,
					message: response,
				});
			}
			catch (err) {
				app.getMainWindow()
					?.webContents
					.send("terminal::data", {
						severity: Severity.Error,
						timestamp: Date.now(),
						label: `IPC: ${err.name}`,
						message: err.message,
						error: err,
					});

				// FIXME: Bevy is sending diagnostic messages through stdout, which
				// is kind of a buzzkill for our IPC. Will have to try and replace
				// it with a custom diagnostics plugin that sends protobuf messages.

				// In the meantime, this little hack will at least spit the message
				// out to the front-end in a format that's sort of readable.
				if (err.name === "RangeError") {
					this._destroying = true;

					let buf = "";
					let chunk: any = null;
					this._stream.setEncoding("utf8");

					while ((chunk = this._stream.read())) {
						buf += chunk.toString();
					}

					app.getMainWindow()
						?.webContents
						.send("terminal::data", {
							severity: Severity.Error,
							timestamp: Date.now(),
							label: `IPC: ${err.name}`,
							message: buf,
						});

					this._streamSubject$.next({ msgType: MessageType.Exit });
				}
			}
		});
	}

	async recv(msgType: MessageType): Promise<any> {
		switch (msgType) {
			case MessageType.WindowHandle:
			case MessageType.SceneDescription:
				return this._stream$.pipe(
					filter(msg => msg?.msgType === msgType),
					first(),
					map(msg => msg.message),
				).toPromise();

			default:
				throw new Error(
					`Unexpected MessageType: ${enumKey(MessageType, msgType)}. `
						+ `This message cannot be received.`
				);
			}
	}

	recvAll(msgType: MessageType): Observable<any> {
		return this._stream$.pipe(
			filter(msg => msg?.msgType === msgType),
			map(msg => msg.message),
		);
	}

	_destroy() {
		this._destroying = true;
		this._streamSubject$.complete();

		return new Promise<void>(resolve => {
			this._stream.removeAllListeners("readable");

			if (this._stream.isPaused()) {
				this._stream.resume();
			}

			this._stream.unpipe();

			resolve();
		});
	}

	private async _readBuffer() {
		while (this._reading) {
			if (this._destroying) {
				throw new Error("Receiver is destroying");
			}
			await sleep(1);
		}

		this._reading = true;

		let lenBuf: number[] = [];
		while (!this._destroying) {
			let byte = this._stream.read(1)?.[0];
			if (byte == null) {
				await sleep(1);
				continue;
			}

			lenBuf.push(byte);

			if (!(byte & MSB)) break;
		}

		if (this._destroying) {
			throw new Error("Receiver is destroying");
		}

		let msgLen = readVarint(lenBuf);
		if (!msgLen) {
			throw new Error("Expected message length delimiter");
		}

		let message: Buffer|null = null;
		while (!message) {
			if (this._destroying) {
				throw new Error("Receiver is destroying");
			}
			message = this._stream.read(msgLen);
			if (!message) await sleep(1);
		}

		this._reading = false;

		return message;
	}
}

export function pipe(exePath: string): Pipe {
	let proc = cp.spawn(exePath, {
		stdio: [
			"pipe",
			"pipe",
			process.stderr,
		],
	})
		.on("exit", onChildExit)
		.on("close", onChildClose)
		.on("error", onError);

	let receiver = new _Receiver(proc.stdout);
	let sender = new _Sender(proc.stdin);

	let close = async () => {
		await sender._destroy();
		await receiver._destroy();
		proc.kill();
	}

	electron.once("will-quit", close);

	return {
		tx: sender,
		rx: receiver,
		close,
	};
}

function onError(err: Error) {
	console.log("onError:", { err });
}

function onChildExit(code?: any) {
	console.log("Child process exited", code);
}

function onChildClose(code?: any) {
	console.log("Child process closed", code);
}

function getRequestType(type: MessageType) {
	switch (type) {
	case MessageType.SetViewportRect: return Request.SetViewportRect;
	case MessageType.WindowHandle: return Request.WindowHandle;
	case MessageType.PatchValue: return Request.PatchValue;
	default:
		throw new Error(
			`Unexpected MessageType ${enumKey(MessageType, type)}. `
				+ `This message cannot be sent.`
		)
	}
}

/**
 * https://developers.google.com/protocol-buffers/docs/encoding#varints
 */
function readVarint(bytes: number[]): number {
	return bytes.reduce((acc, byte, idx) => {
		byte &= ~MSB;
		return acc + (byte << (7 * idx));
	}, 0);
}

function enumKey(enumObj: Record<string, any>, value: number): string {
	return enumObj[value] ?? value.toString(10);
}
