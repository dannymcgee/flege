import { ipcMain } from "electron";
import * as os from "os";
import { Subscription } from "rxjs";
import { switchMap } from "rxjs/operators";
import { User32 } from "win32-api";

import { MessageType, Request, Result, Severity } from "@flege/api";
import { flow, mapValues, seed, sleep } from "@flege/utils";
import app from "../app";
import events from "../events/electron.events";
import projects from "../projects";
import { Pipe, pipe } from "./pipe";

export namespace ipc {
	/* eslint-disable prefer-let/prefer-let */
	const user32 = User32.load();
	const windowNames = new Map<number, string>();
	const windowHandles = new Map<string, number>();
	/* eslint-enable prefer-let/prefer-let */

	let _pipe: Pipe|null = null;
	let subscription: Subscription|null = null;

	export async function init(exePath: string) {
		try {
			_pipe = pipe(exePath);
			let { tx, rx } = _pipe;

			subscription = events.ipcMessages$
				.pipe(switchMap(({ msgType, message }) => tx.send(msgType, message)))
				.subscribe();

			ipcMain.handle("scene::patch-value", patchValue);

			rx.recvAll(MessageType.SceneDescription)
				.subscribe(async ({ content: json }) => {
					try {
						await projects.writeScene(json);
					}
					catch (err) {
						handleError(err);
					}
				});

			rx.recvAll(MessageType.Exit).subscribe(close);

			await initViewportWindowSyncing();

			app.getMainWindow()?.webContents.send("win::viewport-active", true);
		}
		catch (err) {
			handleError(err);
		}
	}

	export async function close() {
		app.getMainWindow()?.webContents.send("win::viewport-active", false);

		await sleep(20);

		let handle = windowHandles.get("bevy");
		if (handle) {
			windowNames.delete(handle);
			windowHandles.delete("bevy");
		}

		subscription?.unsubscribe();
		subscription = null;

		ipcMain.removeHandler("scene::patch-value");

		await _pipe?.close();

		_pipe = null;
	}

	async function initViewportWindowSyncing() {
		if (!_pipe) return;
		let { tx, rx } = _pipe;

		// Get the window handle for the renderer and send it to the Bevy process
		let electronWindowHandle = getNativeWindowHandle();
		windowNames.set(electronWindowHandle, "electron");
		windowHandles.set("electron", electronWindowHandle);

		await tx.send(MessageType.WindowHandle, new Request.WindowHandle({
			handle: electronWindowHandle,
		}));

		// Get the window handle for the Bevy viewport
		let response = await rx.recv(MessageType.WindowHandle);
		let { handle: bevyWindowHandle } = response;
		windowNames.set(bevyWindowHandle, "bevy");
		windowHandles.set("bevy", bevyWindowHandle);

		// Setup the window event listeners
		let win = app.getMainWindow();

		win!.on("focus", () => {
			bringWindowsToTop(electronWindowHandle)
		});

		win!.on("move", () => {
			if (!_pipe) return;

			win?.webContents.send("win::viewport-active", false);
		});

		win!.on("moved", () => {
			win?.webContents.send("win::moved");
			if (!_pipe) return;

			win?.webContents.send("win::viewport-active", true);
		});
	}

	/**
	 * Invoke this whenever the main Electron window receives focus to bring the
	 * Bevy viewport up to the top of the z-order right underneath it.
	 *
	 * @param win Native window handle for the Electron renderer process
	 */
	function bringWindowsToTop(win: number): void {
		// Get the top three windows in the z-order
		for (let i = 0; i < 3; i++) {
			win = user32.GetWindow(win, 2) as number;

			if (windowNames.get(win) === "bevy") {
				// The Bevy viewport is right underneath the Electron window,
				// so we're all done
				return;
			}
		}

		// If we made it this far, the Bevy viewport is underneath some other window
		let handle = windowHandles.get("bevy");
		if (!handle) return;

		// Kick focus to the Bevy viewport, which will immediately return focus back
		// to the Electron window, which will cause this function to be invoked again
		user32.SetForegroundWindow(handle);
	}

	function getNativeWindowHandle(): number {
		let buf = app.getMainWindow()!.getNativeWindowHandle();
		switch (os.endianness()) {
			case "BE": return buf.readUInt32BE(0);
			case "LE": return buf.readUInt32LE(0);
		}
	}

	function handleError(err: Error) {
		app.getMainWindow()?.webContents.send("terminal::data", {
			severity: Severity.Error,
			timestamp: Date.now(),
			label: err.name,
			message: err.message,
			error: err,
		});
	}

	async function patchValue(
		_: any,
		entity: number,
		component: string,
		values: Record<string, { type: string; value: any; }>,
	): Promise<Result> {
		try {
			let { tx } = _pipe!;

			let serialized = flow(
				seed(values),
				mapValues(({ type, value }) => {
					switch (type) {
						case "Vec3": return {
							type,
							value: vec3Buffer(value),
						};
						case "Quat": return {
							type,
							value: quatBuffer(value),
						};
						case "f32": return {
							type,
							value: f32Buffer(value),
						};
						case "i32": return {
							type,
							value: i32Buffer(value),
						};
						case "u32": return {
							type,
							value: u32Buffer(value),
						};
						default: throw new Error(
							`PatchValue not implemented for type '${type}'`,
						);
					}
				}),
			)();

			await tx.send(MessageType.PatchValue, {
				entity,
				component,
				values: serialized,
			});

			return { success: true, value: undefined };
		}
		catch (err) {
			return {
				success: false,
				reason: err.name,
				info: {
					message: err.message,
					stack: err.stack,
				}
			};
		}
	}

	function f32Buffer(value: number): Buffer {
		let result = Buffer.alloc(4);
		result.writeFloatBE(value, 0);

		return result;
	}

	function i32Buffer(value: number): Buffer {
		let result = Buffer.alloc(4);
		result.writeInt32BE(value, 0);

		return result;
	}

	function u32Buffer(value: number): Buffer {
		let result = Buffer.alloc(4);
		result.writeUInt32BE(value, 0);

		return result;
	}

	function vec3Buffer(value: [number, number, number]): Buffer {
		let result = Buffer.alloc(4 * 3);
		value.forEach((component, idx) => {
			result.writeFloatBE(component, idx * 4);
		});

		return result;
	}

	function quatBuffer(value: [number, number, number, number]): Buffer {
		let result = Buffer.alloc(4 * 4);
		value.forEach((component, idx) => {
			result.writeFloatBE(component, idx * 4);
		});

		return result;
	}
}
