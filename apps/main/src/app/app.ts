import { app as electron, BrowserWindow, screen } from "electron";
import path from "path";
import url from "url";

import { environment } from "../environments/environment";
import { RENDERER_NAME, RENDERER_PORT } from "./constants";

namespace app {
	let mainWindow: BrowserWindow|null;

	export function init() {
		return new Promise<void>(resolve => {
			electron.on("window-all-closed", onWindowAllClosed);
			electron.on("activate", onActivate);

			electron.on("ready", () => {
				onReady();
				resolve();
			});
		});
	}

	export function getMainWindow(): BrowserWindow|null {
		return mainWindow;
	}

	export function isDevelopmentMode() {
		let isEnvironmentSet = "ELECTRON_IS_DEV" in process.env;
		let getFromEnvironment = parseInt(process.env.ELECTRON_IS_DEV!, 10) === 1;

		return isEnvironmentSet ? getFromEnvironment : !environment.production;
	}

	function onWindowAllClosed() {
		if (process.platform !== "darwin")
			electron.quit();
	}

	function onReady() {
		// This method will be called when Electron has finished
		// initialization and is ready to create browser windows.
		// Some APIs can only be used after this event occurs.
		initMainWindow();
		loadMainWindow();
	}

	function onActivate() {
		// On macOS it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		if (!mainWindow) onReady();
	}

	function initMainWindow() {
		let workAreaSize = screen.getPrimaryDisplay().workAreaSize;
		let width = Math.min(1440, workAreaSize.width ?? 1440);
		let height = Math.min(900, workAreaSize.height ?? 900);

		// Create the browser window.
		mainWindow = new BrowserWindow({
			width,
			height,
			show: false,
			frame: false,
			transparent: true,
			webPreferences: {
				contextIsolation: true,
				backgroundThrottling: false,
				preload: path.join(__dirname, "preload.js"),
			},
		});
		mainWindow.setMenu(null);
		mainWindow.center();

		mainWindow.once("ready-to-show", () => {
			mainWindow!.show();
			// mainWindow!.webContents.openDevTools();
		});

		// Emitted when the window is closed.
		mainWindow.on("closed", () => {
			// Dereference the window object, usually you would store windows
			// in an array if your app supports multi windows, this is the time
			// when you should delete the corresponding element.
			mainWindow = null;
		});
	}

	function loadMainWindow() {
		// load the index.html of the app.
		if (!electron.isPackaged) {
			console.log(`Loading URL: http://localhost:${RENDERER_PORT}`);
			mainWindow!.loadURL(`http://localhost:${RENDERER_PORT}`);
		} else {
			console.log(`Loading URL: ${url.format({
				pathname: path.join(__dirname, "..", RENDERER_NAME, "index.html"),
				protocol: "file:",
				slashes: true,
			})}`);
			mainWindow!.loadURL(
				url.format({
					pathname: path.join(__dirname, "..", RENDERER_NAME, "index.html"),
					protocol: "file:",
					slashes: true,
				})
			);
		}
	}
}

export default app;
