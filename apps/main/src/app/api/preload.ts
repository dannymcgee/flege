import { contextBridge, ipcRenderer, OpenDialogOptions } from "electron";

import {
	BevyScene,
	MessageType,
	NewProjectConfig,
	Project,
	request,
	response,
	TerminalLog,
} from "@flege/api";
import { Fn } from "@flege/utils";

contextBridge.exposeInMainWorld("app", {
	getVersion: () => ipcRenderer.invoke("app::get-version"),
	openDevTools: () => ipcRenderer.send("app::open-devtools"),
	openSettings: () => ipcRenderer.invoke("app::open-settings"),
});

contextBridge.exposeInMainWorld("fs", {
	selectFilesOrFolders: (config: OpenDialogOptions) => {
		return ipcRenderer.invoke("fs::select-files-or-folders", config);
	},
});

contextBridge.exposeInMainWorld("ipc", {
	send: (msgType: MessageType, message: request.Any) => {
		return ipcRenderer.invoke("ipc::message", { msgType, message });
	},
	on: (msgType: MessageType, listener: Fn<[response.Any], void>) => {
		ipcRenderer.on("ipc::message", (_, data) => {
			if (data.msgType === msgType) listener(data.message);
		});
	},
});

contextBridge.exposeInMainWorld("projects", {
	createNew: (config: NewProjectConfig) => {
		return ipcRenderer.invoke("projects::create-new", config);
	},
	open: (manifest: string) => ipcRenderer.invoke("projects::open", manifest),
	close: () => {
		return ipcRenderer.invoke("projects::close");
	},
	openSettings: (projectPath: string) => {
		return ipcRenderer.invoke("projects::open-settings", projectPath);
	},
	openCode: (project: Project) => {
		return ipcRenderer.invoke("projects::open-code", project);
	},
	rebuild: (project: Project) => {
		return ipcRenderer.invoke("projects::rebuild", project);
	},
});

contextBridge.exposeInMainWorld("scene", {
	onChange: (listener: Fn<[BevyScene], void>) => {
		ipcRenderer.on("scene::data", (_, data) => listener(data));
	},
	patchValue: (entity: number, component: string, values: Record<string, any>) => {
		return ipcRenderer.invoke("scene::patch-value", entity, component, values);
	},
});

contextBridge.exposeInMainWorld("terminal", {
	read: (listener: Fn<[TerminalLog], void>) => {
		ipcRenderer.on("terminal::data", (_, data) => listener(data));
	},
});

contextBridge.exposeInMainWorld("win", {
	toggleMaximized: () => ipcRenderer.invoke("win::toggle-maximized"),
	minimize: () => ipcRenderer.send("win::minimize"),
	close: () => ipcRenderer.send("win::close"),
	setSize: (size: { width: number; height: number; }) => {
		ipcRenderer.send("win::set-size", size);
	},
	onMaximizedChange: (listener: Fn<[boolean], void>) => {
		ipcRenderer.on("win::maximized-change", (_, value) => {
			listener(value);
		});
	},
	onMoved: (listener: Fn<[], void>) => {
		ipcRenderer.on("win::moved", () => {
			listener();
		});
	},
	onViewportActiveChange: (listener: Fn<[boolean], void>) => {
		ipcRenderer.on("win::viewport-active", (_, value) => {
			listener(value);
		});
	},
});
