import * as path from "path";

import { Project, Result } from "@flege/api";
import { readManifest } from "./read-manifest";
import { kebabCase } from "@flege/utils";
import { startFlegeEditor } from "./common";
import { setActive } from "./active-project";

export async function openProject(
	_: any,
	manifestPath: string,
): Promise<Result<Project>> {
	try {
		let projectPath = path.dirname(manifestPath);
		let projectName = projectPath.split(/\/|\\/).reverse()[0];
		let metadata = await readManifest(projectPath);

		let packageName = kebabCase(projectName);
		startFlegeEditor(projectPath, packageName);

		let value: Project = {
			name: projectName,
			path: projectPath,
			metadata,
		};

		setActive(value);

		return {
			success: true,
			value,
		}
	}
	catch (err) {
		setActive(null);

		return {
			success: false,
			reason: err.name,
			info: {
				message: err.message,
				stack: err.stack,
			}
		}
	}
}
