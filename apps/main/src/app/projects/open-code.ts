import cp from "child_process";
import path from "path";

import { Project, Result } from "@flege/api";
import { kebabCase } from "@flege/utils";

export async function openCode(_: any, project: Project) {
	return new Promise<Result>(resolve => {
		let name = kebabCase(project.name);
		let codeFolder = path.join(project.path, `${name}-shared`);

		// TODO: Make code editor user-configurable
		cp.exec(`code "${codeFolder}"`, (err) => {
			if (err) resolve({
				success: false,
				reason: err.name,
				info: {
					message: err.message,
					stack: err.stack,
				},
			});

			else resolve({
				success: true,
				value: undefined, // Why, TypeScript? Why??
			});
		})
	});
}
