import * as cp from "child_process";
import * as path from "path";

import { Severity, TerminalLog } from "@flege/api";
import { JsonObject } from "@flege/utils";

import app from "../app";
import ipc from "../ipc";

export function cargoNew(
	projectPath: string,
	name: string,
	args: string[],
) {
	return new Promise<void>((resolve, reject) => {
		let errors: string[] = [];
		let win = app.getMainWindow()!;

		let cargo = cp.spawn("cargo", [
			"new", name,
			...args,
		], {
			cwd: projectPath,
			stdio: "pipe",
		})
			.on("error", reject)
			.on("close", (code) => {
				if (code !== 0) {
					let err = new Error();
					err.name = "Cargo Error";
					err.message = errors.join("\n");

					cargo.stdout.removeAllListeners();
					cargo.stderr.removeAllListeners();

					reject(err);
				}
				else resolve();
			});

		cargo.stdout.setEncoding("utf-8");
		cargo.stderr.setEncoding("utf-8");

		cargo.stdout.on("data", (chunk) => {
			let log = toTerminalLog(JSON.parse(chunk));
			if (log) win.webContents.send("terminal::data", log);
		});
		cargo.stderr.on("data", (chunk) => {
			errors.push(chunk);
		});
	});
}

export function cargoBuild(
	projectPath: string,
	name: string,
	...extraArgs: string[]
) {
	return new Promise<void>((resolve, reject) => {
		let errors: string[] = [];
		let win = app.getMainWindow()!;

		let cargo = cp.spawn("cargo", [
			"build", "--bin", name,
			"--message-format", "json",
			...extraArgs,
		], {
			cwd: projectPath,
			stdio: "pipe",
		})
			.on("error", reject)
			.on("exit", (code) => {
				if (code !== 0) {
					let err = new Error();
					err.name = "Cargo Error";
					err.message = errors.join("\n");

					cargo.stdout.removeAllListeners();
					cargo.stderr.removeAllListeners();

					reject(err);
				}
				else resolve();
			});

		cargo.stdout.setEncoding("utf-8");
		cargo.stderr.setEncoding("utf-8");

		cargo.stdout.on("data", (chunk) => {
			try {
				let log = toTerminalLog(JSON.parse(chunk));
				if (log) win.webContents.send("terminal::data", log);
			}
			catch (err) {
				console.log(err.message);
			}
		});

		cargo.stderr.on("data", (chunk) => {
			errors.push(chunk);
		});
	});
}

export async function startFlegeEditor(
	projectPath: string,
	packageName: string,
) {
	let exe = path.join(projectPath, `${packageName}-editor.exe`);

	try {
		await cargoBuild(
			projectPath,
			`${packageName}-editor`,
			"-Z", "unstable-options",
			"--out-dir", ".",
		);
		ipc.init(exe);
	}
	catch(err) {
		logError(err);
	}
}

function logError(err: any) {
	app.getMainWindow()!.webContents.send("terminal::data", {
		severity: Severity.Error,
		timestamp: Date.now(),
		message: err.message,
		error: {
			name: err.name ?? "Unknown Error",
			message: err.message,
			stack: err.stack,
		},
	});
}

function toTerminalLog(json: JsonObject): TerminalLog|null {
	if (!("reason" in json)) {
		console.log(JSON.stringify(json, null, "   "));
		return null;
	}

	switch (json.reason) {

		case "compiler-message": {
			let message = (json.message as any)?.message as string;
			if (!message) break;

			let level = (json.message as any)?.level;
			let severity = ((level) => {
				switch (level) {
					case "warning": return Severity.Warning;
					case "help": return Severity.Info;
					case "error": return Severity.Error;
					default: return null;
				}
			})(level);
			if (!severity) break;

			let crate = (json.target as any)?.name as string|undefined;

			let span = (json.message as any)?.spans?.[0] as any;
			let location = span ? {
				crate,
				filename: path.basename(span.file_name),
				line: span.line_start as number,
				column: span.column_start as number,
			} : undefined;

			return {
				severity,
				timestamp: Date.now(),
				message,
				location,
			};
		}

		case "compiler-artifact": {
			let pkgName = (json.package_id as string)
			.replace(/\(.+\)$/, "")
			.trim();

			return {
				severity: Severity.Info,
				timestamp: Date.now(),
				label: "Compiled",
				message: pkgName,
			};
		}

		case "build-finished": {
			if (json.success) return {
				severity: Severity.Success,
				timestamp: Date.now(),
				label: "Done",
				message: `Build finished.`
			}

			return {
				severity: Severity.Error,
				timestamp: Date.now(),
				label: "Cargo Error",
				message: `Build failed.`,
			}
		}

		case "build-script-executed": return null;

	}

	console.log(JSON.stringify(json, null, "   "));

	return null;
}
