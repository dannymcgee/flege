import * as cp from "child_process"

import { CargoMetadata } from "@flege/api";
import { JsonObject, titleCase } from "@flege/utils";

export async function readManifest(projectPath: string) {
	let targetTriple = await getHostTargetTriple(projectPath);
	return getMetadata(projectPath, targetTriple);
}

function getMetadata(projectPath: string, targetTriple: string) {
	return new Promise<CargoMetadata>((resolve, reject) => {
		cp.exec(`cargo metadata --filter-platform ${targetTriple} --format-version 1`, {
			cwd: projectPath,
			maxBuffer: 100 * 1024 * 1024,
		}, (err, stdout, stderr) => {
			if (err) {
				err.name = titleCase(err.name);
				return reject(err);
			}
			if (stderr) {
				console.warn(stderr);
			}
			try {
				let json = JSON.parse(stdout);
				resolve(json);
			}
			catch (err) {
				err.name = titleCase(err.name);
				reject(err);
			}
		});
	});
}

function getHostTargetTriple(projectPath: string) {
	return new Promise<string>((resolve, reject) => {
		cp.exec("rustc --print cfg", {
			cwd: projectPath,
		}, (err, stdout) => {
			if (err) {
				err.name = titleCase(err.name);
				reject(err);
			}
			if (!stdout) {
				reject(new Error("Unable to determine host target"))
			}

			let t = stdout
				.split("\n")
				.reduce((acc, cur) => {
					let m = cur.match(/target_(.+?)="(.+?)"/);
					if (!m) return acc;

					let [, key, value] = m;
					acc[key] = value;

					return acc;
				}, {} as JsonObject);

			resolve(`${t.arch}-${t.vendor}-${t.os}-${t.env}`);
		});
	});
}
