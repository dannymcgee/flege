import { Project } from "@flege/api";
import { kebabCase } from "@flege/utils";

import ipc from "../ipc";
import { startFlegeEditor } from "./common";

export async function rebuildProject(_: any, project: Project) {
	try {
		await ipc.close();

		let packageName = kebabCase(project.name);
		await startFlegeEditor(project.path, packageName);

		return { success: true };
	}
	catch (err) {
		return {
			success: false,
			reason: err.name,
			info: {
				message: err.message,
				stack: err.stack,
			},
		};
	}
}
