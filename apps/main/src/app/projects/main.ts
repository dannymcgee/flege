import { ipcMain, shell } from "electron";
import path from "path"
import app from "../app";

import ipc from "../ipc";

import { setActive } from "./active-project";
import { createProject } from "./create-project";
import { openCode } from "./open-code";
import { openProject } from "./open-project";
import { rebuildProject } from "./rebuild-project";

export function init(): void {
	ipcMain.handle("projects::create-new", createProject);
	ipcMain.handle("projects::open", openProject);
	ipcMain.handle("projects::close", closeProject);
	ipcMain.handle("projects::open-settings", openSettings);
	ipcMain.handle("projects::open-code", openCode);
	ipcMain.handle("projects::rebuild", rebuildProject);
}

async function closeProject() {
	try {
		await ipc.close();

		setActive(null);

		app.getMainWindow()
			?.webContents
			.send("scene::data", { entities: [] });
	}
	catch (err) {
		console.log(err);
	}
}

function openSettings(_: any, projectPath: string) {
	let settingsFile = path.join(projectPath, ".flege/settings.json");

	shell.openExternal(`file://${settingsFile}`);
}
