import { promises as fs } from "fs";
import * as path from "path";

import { NewProjectConfig, Project, Result } from "@flege/api";
import { kebabCase, pascalCase, snakeCase } from "@flege/utils";
import { setActive } from "./active-project";
import { cargoNew, startFlegeEditor } from "./common";
import { readManifest } from "./read-manifest";

export async function createProject(
	_: any,
	config: NewProjectConfig
): Promise<Result<Project>> {
	try {
		let projectPath = path.join(config.path, config.name);
		let name = kebabCase(config.name);

		await fs.mkdir(projectPath);
		await createWorkspace(projectPath, name);

		// Setup the lib for user content that will be shared between all binaries
		let sharedPath = path.join(projectPath, `${name}-shared`);
		await cargoNew(projectPath, `${name}-shared`, ["--lib"]);
		await addDepsToProject(sharedPath);
		await createUserLibBoilerplate(config.name, sharedPath);

		// Dependency declaration for the shared lib
		let sharedDep = `${name}-shared = { path = "../${name}-shared" }`;

		// Setup the project for the actual game
		let gamePath = path.join(projectPath, `${name}-game`);
		await cargoNew(projectPath, `${name}-game`, ["--bin", "--vcs", "none"]);
		await addDepsToProject(gamePath, [sharedDep]);
		await createGameBoilerplate(gamePath);

		// Setup the project for the scene editor
		let editorPath = path.join(projectPath, `${name}-editor`);
		await cargoNew(projectPath, `${name}-editor`, ["--bin", "--vcs", "none"]);
		await addDepsToProject(editorPath, [sharedDep]);
		await createEditorBoilerplate(config.name, editorPath);

		// Read the workspace metadata to send back to the front-end
		let metadata = await readManifest(projectPath);

		// Build and run the scene editor binary
		startFlegeEditor(projectPath, name);

		let value: Project = {
			name: config.name,
			path: path.join(config.path, config.name),
			metadata,
		};

		setActive(value);

		return {
			success: true,
			value,
		};
	}

	catch (err) {
		setActive(null);

		return {
			success: false,
			reason: err.name,
			info: {
				message: err.message,
				stack: err.stack,
			},
		};
	}
}

async function createWorkspace(projectPath: string, name: string) {
	let cargoToml =
`[workspace]
members = [
	"${name}-game",
	"${name}-editor",
	"${name}-shared",
]
`;

	await fs.writeFile(path.join(projectPath, "Cargo.toml"), cargoToml);

	await fs.mkdir(path.join(projectPath, "assets"));
	await fs.writeFile(path.join(projectPath, "assets", ".gitkeep"), "");

	await fs.mkdir(path.join(projectPath, ".flege"));
	await fs.writeFile(path.join(projectPath, ".flege", "settings.json"), "{}");
}

function resolveFlegePluginPath(projectPath: string): string {
	let absPath = path.resolve(process.cwd(), "./libs/bevy-plugin");
	let relPath = path.relative(projectPath, absPath);

	return relPath.replace(/\\/g, "/");
}

async function addDepsToProject(
	projectPath: string,
	additionalDeps: string[] = [],
) {
	let flegePluginPath = resolveFlegePluginPath(projectPath);

	let cargoTomlPath = path.join(projectPath, "Cargo.toml");
	let cargoToml = await fs.readFile(cargoTomlPath);
	let cargoTomlLines = cargoToml.toString().split("\n");

	let depsIdx = cargoTomlLines.indexOf("[dependencies]");
	if (depsIdx === -1) {
		throw new Error("Couldn't find dependencies in cargo.toml");
	}

	cargoTomlLines.splice(
		depsIdx + 1,
		0,
		`bevy = "0.5"`,
		`bevy-flege = { path = "${flegePluginPath}" }`,
		...additionalDeps,
	);

	await fs.writeFile(cargoTomlPath, cargoTomlLines.join("\n"));
}

async function createUserLibBoilerplate(
	projectName: string,
	projectPath: string,
) {
	let name = pascalCase(projectName);
	let filePath = path.join(projectPath, "src/lib.rs");
	let content =
`use bevy::prelude::*;

#[derive(Debug)]
pub struct ${name}ScenePlugin;

impl Plugin for ${name}ScenePlugin {
	fn build(&self, app: &mut AppBuilder) {
	}
}

#[derive(Debug)]
pub struct ${name}GameplayPlugin;

impl Plugin for ${name}GameplayPlugin {
	fn build(&self, app: &mut AppBuilder) {
	}
}
`;

	await fs.writeFile(filePath, content);
}

async function createGameBoilerplate(_projectPath: string) {
	// TODO
}

async function createEditorBoilerplate(
	projectName: string,
	projectPath: string,
) {
	let sharedPkgName = `${snakeCase(projectName)}_shared`;
	let pluginName = `${pascalCase(projectName)}ScenePlugin`;
	let filePath = path.join(projectPath, "src/main.rs");
	let content =
`use bevy::prelude::*;
use bevy_flege::{FlegeEditorPlugin, FlegeViewportPlugin};
use ${sharedPkgName}::${pluginName};

fn main() {
	App::build()
		.add_plugin(FlegeViewportPlugin)
		.add_plugin(FlegeEditorPlugin)
		.add_plugin(${pluginName})
		.add_plugins(DefaultPlugins)
		.run();
}
`;

	await fs.writeFile(filePath, content);
}
