import { Project } from "@flege/api";

let currentProject: Project|null = null;

export function getActive() {
	return currentProject;
}

export function setActive(value: Project|null) {
	currentProject = value;
}
