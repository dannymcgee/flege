import { promises as fs } from "fs";
import path from "path";

import { elementId } from "@flege/utils";
import app from "../app";
import { getActive } from "./active-project";

export async function writeScene(content: string) {
	let project = getActive();
	if (!project) throw new Error("No active project");

	let assetsPath = path.join(project.path, "assets");
	let filename = `.scene-${elementId()}.json`;
	let filePath = path.join(assetsPath, filename);

	await fs.writeFile(filePath, content);

	let entities = JSON.parse(content);
	app.getMainWindow()
		?.webContents
		.send("scene::data", { entities });
}
