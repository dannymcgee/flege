/* eslint-disable prefer-let/prefer-let */
import * as main from "./main";
import * as active from "./active-project";
import * as scene from "./scene";

namespace projects {
	export const init = main.init;
	export const getActive = active.getActive;
	export const writeScene = scene.writeScene;
}

export default projects;
