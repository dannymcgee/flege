import { dialog, OpenDialogOptions } from "electron";
import { ipcMain } from "electron/main";
import { promises as fs } from "fs";
import os from "os";
import path from "path";

import { Result } from "@flege/api";
import app from "../app";

namespace fsAdapter {

export function init(): void {
	ipcMain.handle("fs::select-files-or-folders", selectFilesOrFolders);
}

async function selectFilesOrFolders(
	_: any,
	config: OpenDialogOptions
): Promise<Result<string[]>> {
	try {
		let homeDir = os.homedir();
		let win = app.getMainWindow()!;
		let defaultPath = path.join(homeDir, "Documents", "FLEGE Projects");

		await ensurePathExists(defaultPath);

		let { canceled, filePaths } = await dialog.showOpenDialog(win, {
			defaultPath,
			...config,
		});

		if (canceled || !filePaths.length) return {
			success: false,
			reason: "Canceled",
		}

		return {
			success: true,
			value: filePaths,
		}
	}
	catch (err) {
		return {
			success: false,
			reason: err.name,
			info: {
				message: err.message,
				stack: err.stack,
			},
		};
	}
}

async function ensurePathExists(p: string) {
	try { await fs.stat(p); }
	catch (err) {
		if (err.message.includes("ENOENT")) {
			await fs.mkdir(p, { recursive: true });
		} else {
			throw err;
		}
	}
}

}

export default fsAdapter;
