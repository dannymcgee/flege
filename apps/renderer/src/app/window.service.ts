import { Injectable, OnDestroy } from "@angular/core";
import { anim } from "@flege/styles";

import { BehaviorSubject, merge } from "rxjs";
import {
	debounceTime,
	distinctUntilChanged,
	filter,
	first,
	map,
	shareReplay,
	switchMapTo,
} from "rxjs/operators";
import { ProjectsService } from "./projects.service";

@Injectable({
	providedIn: "root",
})
export class WindowService implements OnDestroy {
	private _maximized$ = new BehaviorSubject(false);
	maximized$ = this._maximized$.pipe(shareReplay({ refCount: false }));
	get maximized() { return this._maximized$.value; }

	private _viewportActive$ = new BehaviorSubject(false);
	viewportActive$ = this.initViewportActiveStream();
	get viewportActive() { return this._viewportActive$.value; }

	constructor(
		private _projects: ProjectsService,
	) {
		win.onMaximizedChange(value => {
			this._maximized$.next(value);
		});

		win.onViewportActiveChange(value => {
			this._viewportActive$.next(value);
		});
	}

	ngOnDestroy(): void {
		this._maximized$.complete();
		this._viewportActive$.complete();
	}

	toggleMaximized(): Promise<boolean> {
		this._viewportActive$.next(false);
		return win.toggleMaximized();
	}

	minimize(): void {
		win.minimize();
	}

	close(): void {
		win.close();
	}

	openDevTools(): void {
		app.openDevTools();
	}

	setSize(size: { width: number; height: number }): void {
		win.setSize(size);
	}

	private initViewportActiveStream() {
		let twoFrames = 2 / 60 * 1000;

		let activated$ = this._viewportActive$.pipe(
			filter(value => value),
			debounceTime(twoFrames),
		);

		let deactivated$ = this._viewportActive$.pipe(
			filter(value => !value),
		);

		let maximizeChanged$ = this._maximized$.pipe(
			debounceTime(anim.frameTime(1)),
			switchMapTo(this._projects.current$.pipe(
				first(),
				map(current => !!current),
			)),
		);

		return merge(
			activated$,
			deactivated$,
			maximizeChanged$,
		).pipe(
			distinctUntilChanged(),
			shareReplay({ refCount: false }),
		);
	}
}
