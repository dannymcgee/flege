import { Injectable } from "@angular/core";

import { BehaviorSubject } from "rxjs";
import { share } from "rxjs/operators";

import { BevyComponent, BevyEntity } from "@flege/api";
import { flow, Fn, partition, seed } from "@flege/utils";

export interface Entity extends BevyEntity {
	name: string;
	components: ECSComponent[];
	children?: Entity[];
}

export type ECSComponent = BevyComponent & {
	id: string;
};

@Injectable({
	providedIn: "root",
})
export class SceneService {
	private _entities$ = new BehaviorSubject<Entity[]>([]);
	entities$ = this._entities$.pipe(share());

	private _inspecting$ = new BehaviorSubject<Entity|null>(null);
	inspecting$ = this._inspecting$.pipe(share());

	constructor() {
		scene.onChange(scene => {
			let entities = flow(
				seed(scene.entities),
				truncateComponentTypeNames,
				addEntityNames,
				parseToTree,
			)();

			this._entities$.next(entities);

			let found = findEntity(
				ent => ent.entity === this._inspecting$.value?.entity,
				entities
			);
			if (!found) this._inspecting$.next(null);
		});
	}

	inspect(entityId: number): void {
		let entity = findEntity(ent => ent.entity === entityId, this._entities$.value);
		if (!entity) {
			console.warn(`Couldn't find entity with ID '${entityId}'`);
		}
		this._inspecting$.next(entity ?? null);
	}

	async patchComponentValues(
		component: string,
		values: Record<string, { type: string; value: any; }>,
	) {
		let entity = this._inspecting$.value!.entity;
		let result = await scene.patchValue(entity, component, values);

		if (!result.success) {
			let err = new Error();
			err.name = result.reason;
			err.message = result.info?.message ?? "";
			err.stack = result.info?.stack;

			console.error(err);
		}
	}
}

function truncateComponentTypeNames(entities: BevyEntity[]) {
	return entities.map(ent => ({
		...ent,
		components: ent.components.map(truncateTypeName),
	}));
}

function truncateTypeName(component: BevyComponent) {
	return {
		...component,
		id: component.type,
		type: component.type.match(/::(\w+)>?$/)![1],
	};
}

function addEntityNames(entities: Omit<Entity, "name">[]): Entity[] {
	return entities.map(ent => {
		let nameCmp = ent.components.find(cmp => cmp.type === "Name");
		let name = nameCmp && "struct" in nameCmp
			? nameCmp.struct.name.value as string
			: ent.entity.toString(10);

		return { ...ent, name };
	});
}

function parseToTree(entities: Entity[]): Entity[] {
	let [topLevel, children] = partition<Entity>(ent => {
		return !ent.components.some(cmp => cmp.type === "Parent");
	})(entities);

	return topLevel.map(ent => {
		let childList: number[]|undefined = ent.components
			.find(cmp => cmp.type === "Children")
			?.["tuple_struct"]?.[0]?.list
			?.map(obj => obj.value);

		if (childList) {
			let ch = childList
				.map(id => children.find(ent => ent.entity === id))
				.filter(child => !!child)
				.map((ent: Entity) => ({
					...ent,
					components: ent.components.filter(cmp => cmp.type !== "Parent"),
				}));

			return {
				...ent,
				children: parseToTree(ch),
			};
		}

		return ent;
	});
}

function findEntity(
	pred: Fn<[Entity], boolean>,
	entities: Entity[],
): Entity|null {
	let result = entities.find(pred);
	if (result) return result;

	let children = entities.reduce((acc, ent) => {
		return acc.concat(ent.children ?? []);
	}, [] as Entity[]);

	if (!children.length) return null;

	return findEntity(pred, children);
}
