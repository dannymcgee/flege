import {
	Component,
	EventEmitter,
	HostBinding,
	Output,
	ViewEncapsulation,
} from "@angular/core";

import { DetectChanges } from "@flege/utils";
import { ProjectsService } from "../projects.service";
import { WindowService } from "../window.service";

@Component({
	selector: "flege-titlebar",
	templateUrl: "./titlebar.component.html",
	styleUrls: ["./titlebar.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class TitlebarComponent {
	@HostBinding("class")
	readonly hostClass = "titlebar";

	@HostBinding("attr.role")
	readonly role = "banner";

	@Output() createProject = new EventEmitter<void>();
	@Output() openProject = new EventEmitter<void>();

	@DetectChanges()
	currentProject$ = this._projects.current$;

	@DetectChanges()
	maximized$ = this._window.maximized$;

	blockingDialog = false;
	nonBlockingDialog = false;
	warningDialog = false;

	constructor(
		private _projects: ProjectsService,
		private _window: WindowService,
	) {}

	closeProject(): void {
		this._projects.close();
	}

	toggleMaximized(): void {
		this._window.toggleMaximized();
	}

	minimize(): void {
		this._window.minimize();
	}

	close(): void {
		this._window.close();
	}

	openDevTools(): void {
		this._window.openDevTools();
	}

	openEditorSettings(): void {
		app.openSettings();
	}

	openProjectSettings(): void {
		this._projects.openSettings();
	}

	openProjectCode(): void {
		this._projects.openCode();
	}
}
