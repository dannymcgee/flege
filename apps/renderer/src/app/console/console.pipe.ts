import { Pipe, PipeTransform } from "@angular/core";
import { Severity } from "@flege/api";
import { IconName } from "@flege/ui";

@Pipe({
	name: "severityIcon",
})
export class SeverityIconPipe implements PipeTransform {
	transform(value: Severity): IconName|null {
		switch (value) {
			case Severity.Debug: return null; // TODO
			case Severity.Error: return "Error";
			case Severity.Info: return "Info";
			case Severity.Success: return "Confirm";
			case Severity.Warning: return "Warning";
		}
	}
}
