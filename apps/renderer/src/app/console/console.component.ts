import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { Severity, TerminalLog as RawTerminalLog } from "@flege/api";

import { Queue } from "@flege/utils";

interface TerminalLog extends RawTerminalLog {
	message: SafeHtml;
}

@Component({
	selector: "flege-console",
	templateUrl: "./console.component.html",
	styleUrls: ["./console.component.scss"]
})
export class ConsoleComponent implements OnInit {
	messages = new Queue<TerminalLog>();

	@ViewChild("messagesElement")
	_messagesElement: ElementRef<HTMLElement>;

	@ViewChild("promptInput")
	_promptInput: ElementRef<HTMLInputElement>;

	constructor(
		private _sanitizer: DomSanitizer,
	) {}

	// eslint-disable-next-line @angular-eslint/no-empty-lifecycle-method
	ngOnInit(): void {
		terminal.read(data => {
			this.messages.enqueue({
				...data,
				message: this._sanitizer.bypassSecurityTrustHtml(data.message),
			});

			if (data.error) {
				console.error(data.error);
			}

			requestAnimationFrame(() => {
				this.updateScroll();
			});
		});
	}

	clear(): void {
		this.messages.clear()
	}

	logTestMessages(): void {
		this.logWarning("Uhh, you might want to take a look at this");
		this.logError("NOW IS THE TIME TO PANIC");
		this.logInfo("The human head weighs eight pounds");
		this.logSuccess("That wasn't so hard, was it?");
		this.logDebug("Made it here");
	}

	logWarning(message: string): void {
		this.log(Severity.Warning, message, "Test");
		this.clearInput();
	}

	logError(message: string): void {
		this.log(Severity.Error, message, "Test");
		this.clearInput();
	}

	logInfo(message: string): void {
		this.log(Severity.Info, message, "Test");
		this.clearInput();
	}

	logSuccess(message: string): void {
		this.log(Severity.Success, message, "Test");
		this.clearInput();
	}

	logDebug(message: string): void {
		this.log(Severity.Debug, message, "Test");
		this.clearInput();
	}

	private clearInput(): void {
		this._promptInput.nativeElement.value = "";
	}

	private updateScroll(): void {
		let { nativeElement: element } = this._messagesElement;
		let { clientHeight, scrollHeight } = element;
		let diff = scrollHeight - clientHeight;

		if (diff > 0) {
			element.scrollBy({
				top: diff,
				behavior: "auto",
			});
		}
	}

	private log(
		severity: Severity,
		message: string,
		label?: string,
	) {
		this.messages.enqueue({
			severity,
			message,
			label,
			timestamp: Date.now(),
		});
	}
}
