import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { NewProjectConfig, Project, Result } from "@flege/api";

export interface NewProjectEvent {
	stage: "bootstrapping"|"compiling"|"done"|"error";
	progress: {
		indeterminate: false;
		total: number;
		completed: number;
	} | {
		indeterminate: true;
	}
	message: string;
}

@Injectable({
	providedIn: "root",
})
export class ProjectsService {
	private _project$ = new BehaviorSubject<Project|null>(null);

	get current() { return this._project$.value; }
	get current$() { return this._project$.asObservable(); }

	async createNew(config: NewProjectConfig) {
		let result = await projects.createNew(config);

		if (result.success) {
			this._project$.next(result.value);
			console.log("project:", result.value);

			return result;
		}
		return result;
	}

	async open(manifestPath: string): Promise<Project> {
		let result = await projects.open(manifestPath);
		if (result.success) {
			this._project$.next(result.value);

			return result.value;
		}

		let err = new Error();
		err.name = result.reason;
		err.message = result.info?.message ?? "";

		throw err;
	}

	async close() {
		await projects.close();
		this._project$.next(null);
	}

	openSettings(): void {
		let projectPath = this._project$.value?.path;
		if (!projectPath) return;

		projects.openSettings(projectPath);
	}

	async openCode(): Promise<Result> {
		let project = this._project$.value;
		if (!project) return {
			success: false,
			reason: "NoProject",
		};

		return projects.openCode(project);
	}

	async rebuild(): Promise<Result> {
		let project = this._project$.value;
		if (!project) return {
			success: false,
			reason: "NoProject",
		};

		return projects.rebuild(project);
	}

	/* TODO: Design some other mechanism to display the compilation progress
	createNew(config: NewProjectConfig): Observable<NewProjectEvent> {
		let project: Project;

		let stream$ = new Subject<NewProjectEvent>();
		let total = 0;
		let completed = 0;
		let packages = new Set<string>();
		let donePackages = new Set<string>();

		stream$.next({
			stage: "bootstrapping",
			progress: {
				indeterminate: true,
			},
			message: "Bootstrapping...",
		});

		projects.createNew(config)
			.then(result => {
				if (!result.success) {
					stream$.next({
						stage: "error",
						progress: {
							indeterminate: false,
							total: 1,
							completed: 1,
						},
						message: result.reason,
					});
					let err = new Error();

					err.name = result.reason;
					err.message = result.info?.message ?? "";

					stream$.error(err);
				}
				else {
					project = result.value;

					let metadata = result.value.metadata;
					metadata.resolve.nodes.forEach(node => {
						packages.add(node.id);

						if (node.dependencies) {
							node.dependencies.forEach(dep => {
								packages.add(dep);
							});
						}
					});

					total = packages.size;

					stream$.next({
						stage: "compiling",
						progress: {
							indeterminate: false,
							total,
							completed,
						},
						message: "Compiling..."
					});
				}
			});

		terminal.read((data: any) => {
			if (data.reason === "compiler-artifact") {
				let packageId = data.package_id as string;

				if (packages.has(packageId) && !donePackages.has(packageId)) {
					++completed;
				}

				donePackages.add(packageId);
				packages.delete(packageId);

				stream$.next({
					stage: "compiling",
					progress: {
						indeterminate: false,
						total,
						completed,
					},
					message: `Compiling ${cleanId(packageId)}`,
				});
			}
			else if (data.reason === "build-finished") {
				if (data.success) {
					stream$.next({
						stage: "done",
						progress: {
							indeterminate: false,
							total,
							completed: total,
						},
						message: "Build finished successfully",
					});

					this._project$.next(project);
					stream$.complete();
				}
				else {
					stream$.next({
						stage: "error",
						progress: {
							indeterminate: false,
							total,
							completed,
						},
						message: "Build failed",
					});
					stream$.error(data);
				}
			}
		});

		return stream$.pipe(shareReplay());
	}
	*/
}

// function cleanId(pkgId: string): string {
// 	return pkgId.replace(/ ?\(.+\)$/, "");
// }
