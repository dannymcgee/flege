import { NgModule } from "@angular/core";
import { CdkTreeModule } from "@angular/cdk/tree";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import {
	AccordionModule,
	ButtonModule,
	DialogModule,
	FLEGE_THEME,
	IconButtonModule,
	IconModule,
	MenuModule,
	PanelModule,
	ThemeModule,
	ThemeService,
	UiFormsModule,
} from "@flege/ui";

import {
	A11yModule,
	CaseConversionModule,
	GlobalFocusManager,
	UnwrapModule,
} from "@flege/utils";

import { AppComponent } from "./app.component";
import { ConsoleComponent } from "./console/console.component";
import { SeverityIconPipe } from "./console/console.pipe";
import { CreatingProjectDialog } from "./dialogs/creating-project/creating-project.dialog";
import { ErrorDialog } from "./dialogs/error/error.dialog";
import { NewProjectDialog } from "./dialogs/new-project/new-project.dialog";
import { InspectorComponent } from "./inspector/inspector.component";
import { ComponentInspector } from "./inspector/components/base.inspector";
import { CameraComponentInspector } from "./inspector/components/camera/camera.inspector";
import { GenericComponentInspector } from "./inspector/components/generic/generic.inspector";
import { LightComponentInspector } from "./inspector/components/light/light.inspector";
import { NameComponentInspector } from "./inspector/components/name/name.inspector";
import { TransformComponentInspector } from "./inspector/components/transform/transform.inspector";
import { Rad2DegPipe } from "./inspector/components/transform/transform.pipe";
import { SceneHierarchyComponent } from "./scene-hierarchy/scene-hierarchy.component";
import { TitlebarComponent } from "./titlebar/titlebar.component";
import { WindowFrameComponent } from "./window-frame/window-frame.component";

@NgModule({
	imports: [
		A11yModule,
		AccordionModule,
		BrowserModule,
		BrowserAnimationsModule,
		ButtonModule,
		CaseConversionModule,
		CdkTreeModule,
		DialogModule,
		FormsModule,
		IconButtonModule,
		IconModule,
		MenuModule,
		PanelModule,
		ThemeModule.withTheme(FLEGE_THEME, "dark"),
		UiFormsModule,
		UnwrapModule,
	],
	declarations: [
		AppComponent,
		CameraComponentInspector,
		ComponentInspector,
		ConsoleComponent,
		CreatingProjectDialog,
		ErrorDialog,
		GenericComponentInspector,
		InspectorComponent,
		LightComponentInspector,
		NameComponentInspector,
		NewProjectDialog,
		Rad2DegPipe,
		SceneHierarchyComponent,
		SeverityIconPipe,
		TitlebarComponent,
		TransformComponentInspector,
		WindowFrameComponent,
	],
	bootstrap: [
		AppComponent,
	],
})
export class AppModule {
	constructor(
		private _globalFocusManager: GlobalFocusManager,
		private _theme: ThemeService,
	) {}
}
