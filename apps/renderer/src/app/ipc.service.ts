import { Injectable } from "@angular/core";

import { MessageType, request } from "@flege/api";

@Injectable({
	providedIn: "root",
})
export class IpcService {
	send(msgType: MessageType.SetViewportRect, message: request.SetViewportRect): Promise<void>;

	async send(msgType: MessageType, message: request.Any) {
		return ipc.send(msgType, message);
	}
}
