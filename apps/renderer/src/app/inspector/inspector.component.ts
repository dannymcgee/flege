import { Component, TrackByFunction } from "@angular/core";

import { map, tap } from "rxjs/operators";

import { filter, flow, seed, sort } from "@flege/utils";
import { ECSComponent, Entity, SceneService } from "../scene.service";

const HIDDEN_COMPONENT_TYPES = [
	"GlobalTransform",
	"VisibleEntities",
	"Children",
	"Parent",
	"PreviousParent",
];

@Component({
	selector: "flege-inspector",
	templateUrl: "./inspector.component.html",
	styleUrls: ["./inspector.component.scss"],
})
export class InspectorComponent {
	components$ = this._scene.inspecting$.pipe(
		tap(ent => this.entity = ent),
		map(ent => ent == null ? [] : flow(
			seed(ent.components),
			filter(cmp => !HIDDEN_COMPONENT_TYPES.includes(cmp.type)),
			sort((a, b) => {
				if (a.type === "Name") return -2;
				else if (b.type === "Name") return 2;
				else if (a.type === "Transform") return -1;
				else if (b.type === "Transform") return 1;
				return a.type.localeCompare(b.type);
			}),
		)()),
	);

	entity: Entity|null = null;

	constructor(
		private _scene: SceneService,
	) {}

	trackById: TrackByFunction<ECSComponent> = (_, it) => {
		let ent = this.entity?.entity?.toString(10) ?? "_";
		return ent + it.id;
	}
}
