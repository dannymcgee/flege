import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { BevyComponent, BevyStruct } from "@flege/api";
import { flow, mapValues, seed } from "@flege/utils";
import { ECSComponent } from "../../scene.service";

@Component({
	selector: "flege-component",
	templateUrl: "./base.inspector.html",
	styles: [`:host { display: contents; }`],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentInspector {
	@Input()
	get component() { return this._component; }
	set component(value) {
		this.struct = this.processStruct(value);
		this._component = value;
	}
	private _component: ECSComponent;

	struct: BevyStruct|null = null;

	processStruct(component: BevyComponent): BevyStruct|null {
		if (!("struct" in component)) return null;

		return flow(
			seed(component.struct),
			mapValues(({ type, value }) => {
				let m = type.match(/::(\w+)>?$/);
				let typeClean = m?.[1] ?? type;

				return {
					type: typeClean,
					value,
				}
			}),
		)();
	}
}
