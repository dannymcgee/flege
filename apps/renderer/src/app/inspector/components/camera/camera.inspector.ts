import { Component, Input } from "@angular/core";
import { FromArray } from "@flege/utils";
import { Matrix4 } from "@math.gl/core";

@Component({
	selector: "flege-camera-component",
	templateUrl: "./camera.inspector.html",
	styleUrls: [
		"../base.inspector.scss",
		"./camera.inspector.scss",
	],
})
export class CameraComponentInspector {
	@Input() id: string;
	@Input() name: string;
	@Input() @FromArray() projMat: Matrix4;
}
