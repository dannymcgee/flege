import { Pipe, PipeTransform } from "@angular/core";

const CONVERSION_FACTOR = 57.295779513;

export function rad2deg(radians: number): number {
	if (radians == null) return 0;
	return radians * CONVERSION_FACTOR;
}

export function deg2rad(degrees: number): number {
	if (degrees == null) return 0;
	return degrees / CONVERSION_FACTOR;
}

@Pipe({
	name: "rad2deg",
})
export class Rad2DegPipe implements PipeTransform {
	transform = rad2deg;
}
