/* eslint-disable @angular-eslint/no-input-rename */
import { Component, Input, OnChanges, OnInit } from "@angular/core";

import { Euler, Quaternion } from "@math.gl/core";

import { Changes } from "@flege/utils";
import { SceneService } from "../../../scene.service";
import { deg2rad } from "./transform.pipe";

type Vec3Array = [number, number, number];
type QuatArray = [number, number, number, number];

class Vec3 {
	get x() { return this._x; }
	set x(value) {
		this._x = value;
		this._scene.patchComponentValues(this._id, {
			[this._fieldName]: {
				type: "Vec3",
				value: [value, this.y, this.z],
			},
		});
	}

	get y() { return this._y; }
	set y(value) {
		this._y = value;
		this._scene.patchComponentValues(this._id, {
			[this._fieldName]: {
				type: "Vec3",
				value: [this.x, value, this.z],
			},
		});
	}

	get z() { return this._z; }
	set z(value) {
		this._z = value;
		this._scene.patchComponentValues(this._id, {
			[this._fieldName]: {
				type: "Vec3",
				value: [this.x, this.y, value],
			},
		});
	}

	constructor(
		private _id: string,
		private _fieldName: string,
		private _scene: SceneService,
		private _x: number,
		private _y: number,
		private _z: number,
	) {}
}

@Component({
	selector: "flege-transform-component",
	templateUrl: "./transform.inspector.html",
	styleUrls: [
		"../base.inspector.scss",
		"./transform.inspector.scss",
	],
})
export class TransformComponentInspector implements OnInit, OnChanges {
	@Input() id: string;
	@Input("translation") _translation: Vec3Array;
	@Input("rotation") _rotation: QuatArray;
	@Input("scale") _scale: Vec3Array;

	translation: Vec3;
	scale: Vec3;

	rotationMode: "Quaternion"|"Euler" = "Euler";
	quat: Quaternion;
	euler: Euler;

	constructor(
		private _scene: SceneService,
	) {}

	ngOnInit(): void {
		this.translation = new Vec3(
			this.id,
			"translation",
			this._scene,
			...this._translation
		);
		this.scale = new Vec3(
			this.id,
			"scale",
			this._scene,
			...this._scale,
		);
	}

	ngOnChanges(changes: Changes<this>): void {
		if (changes._rotation) {
			this.quat = new Quaternion(...this._rotation);
			this.euler = new Euler().fromQuaternion(this.quat);
		}
	}

	toggleRotationMode(): void {
		this.rotationMode = this.rotationMode === "Quaternion"
			? "Euler"
			: "Quaternion";
	}

	updateRotation(axis: "x"|"y"|"z", degrees: number): void {
		let radians = deg2rad(degrees);
		let delta = radians - this.euler[axis];
		let { x, y, z } = this.euler;


		switch (axis) {
			case "x":
				this.euler.set(radians, y, z);
				this.quat.rotateX(delta);
				break;
			case "y":
				this.euler.set(x, radians, z);
				this.quat.rotateY(delta);
				break;
			case "z":
				this.euler.set(x, y, radians);
				this.quat.rotateZ(delta);
				break;
		}

		this._scene.patchComponentValues(this.id, {
			rotation: {
				type: "Quat",
				value: [...this.quat],
			},
		});
	}
}
