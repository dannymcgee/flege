import { Component, Input } from "@angular/core";
import { FromBevyColor } from "@flege/utils";

import { Color } from "chroma-js";

interface Range {
	start: number;
	end: number;
}

@Component({
	selector: "flege-light-component",
	templateUrl: "./light.inspector.html",
	styleUrls: [
		"../base.inspector.scss",
		"./light.inspector.scss",
	],
})
export class LightComponentInspector {
	@Input() id: string;
	@Input() @FromBevyColor() color: Color;
	@Input() depth: Range;
	@Input() fov: number;
	@Input() intensity: number;
	@Input() range: number;
}
