import { Component, HostBinding, Input } from "@angular/core";

import { BevyStruct } from "@flege/api";
import {
	filter,
	flow,
	isNotNull,
	mapEntries,
	seed,
	titleCase,
} from "@flege/utils";

interface GenericInput {
	type: string;
	/** Required if `multi` is true */
	ariaLabel?: string;
	readonly: boolean;
	value: any;
}

interface GenericField {
	label: string;
	multi: boolean;
	inputs: GenericInput[];
}

@Component({
	selector: "flege-generic-component",
	templateUrl: "./generic.inspector.html",
	styleUrls: [
		"../base.inspector.scss",
		"./generic.inspector.scss",
	],
})
export class GenericComponentInspector {
	@Input() id: string;
	@Input() type: string;

	@Input()
	get struct() { return this._struct; }
	set struct(value) {
		this._struct = value;
		this.fields = this.processFields(value);
	}
	private _struct: BevyStruct|null;

	@HostBinding("style.display")
	get display() {
		if (this.fields.length) return null;
		return "none";
	}

	fields: GenericField[] = [];

	processFields(struct: BevyStruct|null): GenericField[] {
		if (!struct) return [];

		return flow(
			seed(struct),
			mapEntries(([key, { type, value }]) => {
				let label = titleCase(key);

				if (/^[fui](?:[0-9]{1,2}|size)$/.test(type)) return {
					label,
					multi: false,
					inputs: [{
						type: "number",
						readonly: false,
						value,
					}],
				};

				if (type === "String") return {
					label,
					multi: false,
					inputs: [{
						type: "text",
						readonly: false,
						value,
					}],
				};

				// TODO: Request useful information from the IPC
				if (type === "HandleId") return {
					label,
					multi: false,
					inputs: [{
						type: "text",
						readonly: true,
						value: value!["Id"][0],
					}],
				}

				if (type === "bool") return {
					label,
					multi: false,
					inputs: [{
						type: "checkbox",
						readonly: false,
						value,
					}],
				}

				console.warn("Unimplemented struct type:", {
					[key]: { type, value },
				});

				return null;
			}),
			filter(isNotNull),
		)();
	}
}
