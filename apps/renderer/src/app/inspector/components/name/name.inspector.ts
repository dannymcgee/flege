import { Component, Input } from "@angular/core";

@Component({
	selector: "flege-name-component",
	templateUrl: "./name.inspector.html",
	styleUrls: ["./name.inspector.scss"],
})
export class NameComponentInspector {
	@Input() id: string;
	@Input() value: string;
}
