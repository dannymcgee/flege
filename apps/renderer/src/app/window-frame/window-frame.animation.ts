import { animate, style, transition, trigger } from "@angular/animations";

export const MAXIMIZE_RESTORE = trigger("maximizeRestore", [
	transition("false => true", [
		style({
			width: "calc(100% - 128px)",
			height: "calc(100% - 128px)",
			margin: "64px",
		}),
		animate("200ms 0ms cubic-bezier(0,1,.66,1)", style({
			width: "100%",
			height: "100%",
			margin: "0",
		})),
	]),
]);
