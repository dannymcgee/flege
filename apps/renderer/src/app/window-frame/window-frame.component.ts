import { Component } from "@angular/core";

@Component({
	selector: "flege-window-frame",
	template: `<ng-content></ng-content>`,
	styleUrls: ["./window-frame.component.scss"],
})
export class WindowFrameComponent {}
