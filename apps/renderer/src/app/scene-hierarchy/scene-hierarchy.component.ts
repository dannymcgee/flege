import { Component, TrackByFunction } from "@angular/core";
import { NestedTreeControl } from "@angular/cdk/tree";

import { Entity, SceneService } from "../scene.service";

@Component({
	selector: "flege-scene-hierarchy",
	templateUrl: "./scene-hierarchy.component.html",
	styleUrls: ["./scene-hierarchy.component.scss"]
})
export class SceneHierarchyComponent {
	entities$ = this._scene.entities$;
	inspecting$ = this._scene.inspecting$;

	treeControl = new NestedTreeControl<Entity>(node => node.children);
	hovered = -1;

	constructor(
		private _scene: SceneService,
	) {}

	hasChild(_: number, node: Entity) {
		return !!node.children?.length;
	}

	entityId: TrackByFunction<Entity> = (_, ent) => ent.entity;

	inspect(entityId: number): void {
		this._scene.inspect(entityId);
	}
}
