import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
	selector: "flege-error-dialog",
	template: `
		<ui-dialog role="alert"
			(close)="close.emit()"
		>
			<ui-dialog-heading icon="Error">
				{{ error?.name || 'Error' }}
			</ui-dialog-heading>
			<div [innerHtml]="message"></div>
			<ui-dialog-footer>
				<button ui-btn="warning"
					(click)="close.emit()"
				>
					Okay
				</button>
			</ui-dialog-footer>
		</ui-dialog>
	`,
	styleUrls: [],
})
export class ErrorDialog implements OnInit {
	@Input() error: Error;
	// eslint-disable-next-line @angular-eslint/no-output-native
	@Output() close = new EventEmitter<void>();

	message: string;

	ngOnInit(): void {
		this.message = this.error.message
			.split("\n")
			.map(line => line.trim())
			.filter(line => !!line)
			.map(line => line.replace(/^error: /, ""))
			.map(line => line.replace(/`(.+)`/, `<code>$1</code>`))
			.map(line => `<p>${line}</p>`)
			.join("\n");
	}
}
