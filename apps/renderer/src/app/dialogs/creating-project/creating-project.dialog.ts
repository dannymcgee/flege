import { Component, Input } from "@angular/core";

@Component({
	selector: "flege-creating-project-dialog",
	template: `
		<ui-dialog loader
			[indeterminate]="(progress?.indeterminate == null
				? true
				: progress!.indeterminate)"
			[total]="progress?.total"
			[completed]="progress?.completed"
		>
			<ui-dialog-heading icon="Folder">
				Creating Project
			</ui-dialog-heading>
			<code *ngIf="message">{{ message }}</code>
		</ui-dialog>
	`,
	styleUrls: ["./creating-project.dialog.scss"],
})
export class CreatingProjectDialog {
	@Input() message?: string;
	@Input() progress?: {
		indeterminate: boolean;
		total?: number;
		completed?: number;
	} | null;
}
