import { Component, EventEmitter, Output } from "@angular/core";

import { NewProjectConfig } from "@flege/api";

@Component({
	selector: "flege-new-project-dialog",
	templateUrl: "./new-project.dialog.html",
	styleUrls: ["./new-project.dialog.scss"]
})
export class NewProjectDialog {
	@Output() confirm = new EventEmitter<NewProjectConfig>();
	@Output() cancel = new EventEmitter<void>();

	name: string|null = null;
	path: string|null = null;

	async selectPath() {
		let result = await fs.selectFilesOrFolders({
			properties: ["openDirectory"],
		});

		if (result.success) {
			this.path = result.value[0];
		}
	}

	submit(): void {
		this.confirm.emit({
			name: this.name!,
			path: this.path!,
		});
	}
}
