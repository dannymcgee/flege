import {
	AfterViewInit,
	Component,
	ElementRef,
	HostListener,
	ViewChild,
} from "@angular/core";

import { MessageType, NewProjectConfig } from "@flege/api";
import { Throttle } from "@flege/utils";
import { IpcService } from "./ipc.service";
import { ProjectsService } from "./projects.service";
import { MAXIMIZE_RESTORE } from "./window-frame/window-frame.animation";
import { WindowService } from "./window.service";

@Component({
	selector: "flege-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"],
	animations: [MAXIMIZE_RESTORE],
})
export class AppComponent implements AfterViewInit {
	MessageType = MessageType;

	maximized$ = this._window.maximized$;
	viewportActive$ = this._window.viewportActive$;
	currentProject$ = this._projects.current$;

	newProjectDialog = false;
	readingManifest = false;
	error: Error|null = null;

	creatingProject = false;
	creatingProjectMessage?: string;
	creatingProjectProgress?: {
		indeterminate: true;
	} | {
		indeterminate: false;
		total: number;
		completed: number;
	} | null;

	@ViewChild("viewportElement", { read: ElementRef })
	_viewportElement: ElementRef<HTMLElement>;

	constructor(
		private _ipc: IpcService,
		private _projects: ProjectsService,
		private _window: WindowService,
	) {}

	ngAfterViewInit(): void {
		this.updateViewportRect();

		win.onMoved(() => {
			this.updateViewportRect();
		});
	}

	async createProject(config: NewProjectConfig) {
		this.newProjectDialog = false;

		this.creatingProjectMessage = "Bootstrapping..."
		this.creatingProject = true;

		let result = await this._projects.createNew(config);
		this.creatingProject = false;

		if (!result.success) {
			let err = new Error();
			err.name = result.reason;
			err.message = result.info?.message ?? "";

			this.error = err;
		}
	}

	async openProject() {
		let result = await fs.selectFilesOrFolders({
			properties: ["openFile"],
			filters: [{
				name: "cargo",
				extensions: ["toml"],
			}],
		});
		if (!result.success) return;

		this.readingManifest = true;

		try {
			await this._projects.open(result.value[0]);
			this.readingManifest = false;
		}
		catch (err) {
			this.readingManifest = false;
			this.error = err;
		}
	}

	async rebuildProject() {
		await this._projects.rebuild();
		this.updateViewportRect();
	}

	@Throttle()
	onViewportMousemove(_event: MouseEvent): void {
		if (!this._window.viewportActive) return;
		// TODO
	}

	@HostListener("window:resize")
	@Throttle()
	updateViewportRect(): void {
		let { nativeElement: element } = this._viewportElement;
		let { width, height, x, y } = element.getBoundingClientRect();
		this._ipc.send(MessageType.SetViewportRect, {
			x,
			y,
			width,
			height,
			scaleFactor: -1,
		});
	}
}
