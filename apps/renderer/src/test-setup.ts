import "jest-preset-angular";
import { noop } from "rxjs";

const GLOBAL_MOCKS = {
	app: {
		value: {
			getVersion: async () => "",
			openDevTools: noop,
			platform: "Jest",
		},
	},
	ipc: {
		value: {
			send: async (_: string) => "",
			on: noop,
		},
	},
	win: {
		value: {
			toggleMaximized: async () => true,
			onMaximizedChange: noop,
			minimize: noop,
			close: noop,
			setSize: noop,
		},
	},
	projects: {
		value: {
			createNew: () => Promise.resolve(),
		},
	},
};

Object.defineProperties(window, GLOBAL_MOCKS);
Object.defineProperties(global, GLOBAL_MOCKS);
