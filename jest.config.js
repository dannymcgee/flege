module.exports = {
	projects: [
		"<rootDir>/apps/renderer",
		"<rootDir>/apps/main",
		"<rootDir>/libs/api",
		"<rootDir>/libs/styles",
		"<rootDir>/libs/ui",
		"<rootDir>/libs/utils",
	],
};
