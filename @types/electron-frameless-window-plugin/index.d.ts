declare module "electron-frameless-window-plugin" {
	interface WindowOptions {
		/**
		 * Apply to each window through 'browser-window-created' event
		 * @default false
		 */
		setGlobal?: boolean;
		/**
		 * Rewrite transparent window instance's methods (`.getNormalBounds()`,
		 * `.maximize()`, `.unmaximize()`, `.isMaximized()`). Fix where 'maximize'
		 * and 'unmaximize' events cannot be emitted when calling `maximize()` and
		 * `unmaximize()` methods. Also fix `isMaximized()` and
		 * `getNormalBounds()` methods.
		 * @default true
		 */
		fixTransparent?: boolean;
		/**
		 * Fix the behavior double clicking on the title bar and dragging the
		 * title bar when window is maximized
		 * @default true
		 */
		fixDragRegion?: boolean;
		/**
		 * Disable to display window menu when right click on the title bar.
		 * @default true
		 */
		noDragRegionMenu?: boolean;
		/**
		 * Add window instance method .hideFromFullScreen(). It is useful on macOS
		 * when we click the close button and want the window to be hidden instead
		 * of closed
		 * @default true
		 */
		easyHideFromFullScreen?: boolean;
		/**
		 * The BrowserWindow instance you need to apply single.
		 * @default undefined
		 */
		browserWindow?: Electron.BrowserWindow;
	}

	/**
	 * Fix the problems about electron frameless and transparent window, such as
	 * using `maximize()` can not emit 'maximize' event.
	 *
	 * @example
	 * // Apply globally
	 * const { app } = require('electron')
	 * const { plugin } = require('electron-frameless-window-plugin')
	 *
	 * app.on('ready', () => {
	 *   ...
	 * })
	 *
	 * plugin({
	 *   setGlobal: true
	 * })
	 *
	 * @example
	 * // Apply to a single window
	 * function createWindow () {
	 *   const framelessPlugin = require('electron-frameless-window-plugin')
	 *   const mainWindow = new BrowserWindow(windowOptions)
	 *
	 *   framelessPlugin.plugin({
	 *     browserWindow: mainWindow,
	 *     fixDragRegion: false
	 *   })
	 * }
	 *
	 * app.on('ready', () => {
	 *   createWindow()
	 * })
	 */
	function plugin(options: WindowOptions): void;
}
