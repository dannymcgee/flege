import type { OpenDialogOptions } from "electron";

import type {
	BevyScene,
	MessageType,
	NewProjectConfig,
	Project,
	request,
	response,
	Result,
	TerminalLog,
} from "../../libs/api/src";

import type { Fn, JsonObject } from "../../libs/utils/src";

declare global {

namespace app {
	function getVersion(): Promise<string>;
	function openDevTools(): void;
	function openSettings(): Promise<void>;
}

namespace fs {
	function selectFilesOrFolders(config: OpenDialogOptions): Promise<Result<string[]>>;
}

namespace ipc {
	function send(type: MessageType, message: request.Any): Promise<void>;
	function on(type: MessageType, listener: Fn<[response.Any], void>): void;
}

namespace projects {
	function createNew(config: NewProjectConfig): Promise<Result<Project>>;
	/** @param manifest Absolute path to the `Cargo.toml` file */
	function open(manifest: string): Promise<Result<Project>>;
	function close(): Promise<void>;
	function openSettings(projectPath: string): Promise<void>;
	function openCode(project: Project): Promise<Result>;
	function rebuild(project: Project): Promise<Result>;
}

namespace scene {
	function onChange(listener: Fn<[BevyScene], void>): void;
	function patchValue(
		entity: number,
		component: string,
		values: Record<string, {
			type: string;
			value: any;
		}>,
	): Promise<Result>;
}

namespace terminal {
	function read(listener: Fn<[TerminalLog], void>): void;
}

namespace win {
	function toggleMaximized(): Promise<boolean>;
	function minimize(): void;
	function close(): void;
	function setSize(size: { width: number; height: number; }): void;
	function onMaximizedChange(listener: Fn<[boolean], void>): void;
	function onMoved(listener: Fn<[], void>): void;
	/** Invoked when the Bevy viewport is ready/not ready to show */
	function onViewportActiveChange(listener: Fn<[boolean], void>): void;
}

}
