import * as chalk from "chalk";
import { promises as fs } from "fs"
import * as path from "path";

let outPath = path.join(
	process.cwd(),
	"libs/utils/src/lib/fp.types.ts",
);

(async function () {

	let overloads: string[] = [
		`// NOTE: Generated from \`tools/generate-flow-fn-overloads.ts\``,
		`export interface Flow {`
	];

	for (let i = 50; i >= 2; i--) {
		let result = `\t<A extends any[]`;

		for (let a = 1; a <= i; a++) {
			result += `, R${a}`;
		}

		// result += `>(\n\t\tf1: (...args: A) => R1`;
		result += `>(f1: (...args: A) => R1`;

		for (let f = 2; f <= i; f++) {
			// result += `,\n\t\tf${f}: (a: R${f-1}) => R${f}`;
			result += `, f${f}: (a: R${f-1}) => R${f}`;
		}

		// result += `\n\t): (...args: A) => R${i};`
		result += `): (...args: A) => R${i};`

		overloads.push(result);
		result = "";
	}

	overloads.push(`}`, ``);

	let fileContent = overloads.join("\n");

	try {
		await fs.writeFile(outPath, fileContent);
	}
	catch (err) {
		console.log(`${
			chalk.bold.inverse.redBright(` ${err.name} `)
		} ${
			chalk.bold.redBright(err.message)
		}`);

		process.exit(1);
	}
})();
