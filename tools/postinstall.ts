import * as cp from "child_process";
import { promises as fs } from "fs";
import * as path from "path";

const TOOLS_PATH = path.resolve(process.cwd(), "tools");

(async function() {
	let { schemasGenerated, buildersCompiled } = await areToolsBuilt();

	if (!schemasGenerated) await generateSchemas();
	if (!buildersCompiled) await compileBuilders();
})();

async function areToolsBuilt() {
	let buildersRoot = path.resolve(TOOLS_PATH, "./builders");
	let buildersPaths = (await fs.readdir(buildersRoot))
		.map(p => path.resolve(buildersRoot, p));

	if (!buildersPaths.length) return {
		schemasGenerated: true,
		buildersCompiled: true,
	};

	let schemasGenerated = buildersPaths.map(() => false);
	let buildersCompiled = buildersPaths.map(() => false);

	await Promise.all(buildersPaths.map(async (builder, idx) => {
		let files = await fs.readdir(path.resolve(builder, "./build"));

		if (files.includes("schema.json")) schemasGenerated[idx] = true;
		if (files.includes("impl.js")) buildersCompiled[idx] = true;
	}));

	return {
		schemasGenerated: schemasGenerated.every(val => val),
		buildersCompiled: buildersCompiled.every(val => val),
	}
}

async function generateSchemas() {
	console.log("Generating builder schemas...");
	return promisifyCommand("yarn", ["generate-schemas"]);
}

async function compileBuilders() {
	console.log("Compiling builders...");
	return promisifyCommand("yarn", ["compile-builders"]);
}

function promisifyCommand(cmd: string, args: string[]) {
	return new Promise<void>((resolve, reject) => {
		cp.spawn(cmd, args, {
			stdio: "inherit",
			shell: true,
		})
			.on("error", reject)
			.on("exit", resolve)
			.on("close", resolve);
	});
}
