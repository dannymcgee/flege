import {
	Enum,
	Field,
	MapField,
	Namespace,
	NamespaceBase,
	OneOf,
	parse,
	ReflectionObject,
	Type,
} from "protobufjs";

interface TsReflectionObject {
	name: string;
	proto: ReflectionObject;
	parent?: ReflectionObject;
}

interface TsEnum extends TsReflectionObject {
	values: Map<string, number>;
}

interface TsType extends TsReflectionObject {
	fields: TsField[];
	oneOfs: TsOneOf[];
}

interface TsFieldDecorator {
	name: string;
	args: string[];
	typeAnnotation: string;
}

interface TsField extends TsReflectionObject {
	id: number;
	comment?: string;
	decorator: TsFieldDecorator;
}

interface TsOneOf extends TsReflectionObject {
	fieldNames: string[];
}

export class Transpiler {
	private _imports = new Map<string, Set<string>>();
	private _enums: TsEnum[] = [];
	private _types: TsType[] = [];

	constructor(src: string) {
		let parsed = parse(src, { alternateCommentMode: true });
		let root: NamespaceBase;

		if (parsed.package) {
			root = parsed.root.nested[parsed.package] as NamespaceBase;
		} else {
			root = parsed.root;
		}

		root.nestedArray.forEach(it => this.parse(it));
	}

	generate(): string {
		let result: string[] = [];
		let depth = 0;
		let addLines = (...lines: string[]) => {
			let indent = "\t".repeat(depth);

			lines.forEach(line => {
				result.push(indent + line);
			});
		};

		// Emit import statement
		this._imports.forEach((targets, source) => {
			let stmt = `import { `;
			stmt += Array.from(targets)
				.sort((a, b) => a.localeCompare(b))
				.join(", ");
			stmt += ` } from "${source}";`;

			addLines(stmt);
		});

		if (this._imports.size) addLines("");

		// Emit enum declarations
		this._enums.forEach(en => {
			addLines(`export enum ${en.name} {`);
			depth++;

			en.values.forEach((value, key) => {
				addLines(`${upperSnakeToPascal(key)} = ${value},`);
			});

			depth--;
			addLines("}", "");
		});

		// Emit namespaced interfaces for nested message types
		this._types.forEach(typ => {
			let children = this._types.filter(t => t.parent === typ.proto);
			if (!children.length) return;

			addLines(`export namespace ${pascalToCamel(typ.name)} {`);
			depth++;

			children.forEach(child => {
				addLines(`export interface ${child.name} {`);
				depth++;

				child.fields.forEach(field => {
					if (field.comment) {
						addLines(`/** ${field.comment.trim()} */`);
					}
					else if (field.decorator.args.includes(`"bytes"`)) {
						addLines(
							`/** The type of \`unknown\` is \`Buffer\` in the main process, `
								+ `or \`Uint8Array\` in a renderer. */`
						);
					}
					addLines(`${
						field.name
					}: ${
						field.decorator.typeAnnotation
							.replace(/[A-Z][a-zA-Z0-9]*_/g, "")
					};`);
				});

				depth--;
				addLines(`}`);
			});

			addLines(`export type Any = ${
				children
					.filter(ch => ch.name !== "Header")
					.map(ch => ch.name)
					.join("|")
			};`);

			depth--;
			addLines("}", "");
		});

		// Emit class declarations
		this._types.forEach(typ => {
			let exportStmt: string;
			let name: string;

			if (typ.parent && typ.parent instanceof Type) {
				exportStmt = "";
				name = `${typ.parent.name}_${typ.name}`;
			} else {
				exportStmt = "export ";
				name = typ.name;
			}

			addLines(
				`@Type.d("${name}")`,
				`${exportStmt}class ${name} extends Message<${name}> {`,
			);
			depth++;

			let children = this._types.filter(t => t.parent === typ.proto);
			if (children.length) {
				children.forEach(child => {
					addLines(`static ${child.name} = ${name}_${child.name};`);
				});
				addLines("");
			}

			typ.fields.forEach(f => {
				if (f.decorator.args.includes(`"bytes"`)) {
					addLines(
						`/** The type of \`unknown\` is \`Buffer\` in the main process, `
							+ `or \`Uint8Array\` in a renderer. */`
					);
				}

				if (f.comment) {
					addLines(`/** ${f.comment.trim()} */`);
				}

				let line = `@${f.decorator.name}.d(`;
				line += `${f.id}, `;
				line += f.decorator.args.join(", ");
				line += `) ${f.name}: ${f.decorator.typeAnnotation};`;

				addLines(line);
			});
			if (typ.fields.length) addLines("");

			typ.oneOfs.forEach(o => {
				addLines(
					`@OneOf.d(${o.fieldNames.map(n => `"${n}"`).join(", ")})`,
					`${o.name}: string;`,
					``,
				);
			});

			depth--;
			addLines("}", "");
		});

		// Format the final output
		return result.reduce((acc, line, idx) => {
			if (result[idx + 1]?.endsWith("}") && !line.trim()) {
				return acc;
			}
			acc += line.replace(/\s+$/, "");

			if (idx < (result.length - 1)) {
				acc += "\n";
			}

			return acc;
		}, "");
	}

	private parse(obj: ReflectionObject, parent?: ReflectionObject): void {
		if (obj instanceof Enum) {
			this.parseEnum(obj, parent);
		}
		else if (obj instanceof Type) {
			this.parseType(obj, parent);
		}
	}

	private parseEnum(en: Enum, parent?: ReflectionObject): void {
		let values = new Map<string, number>();

		Object.entries(en.values)
			.forEach(([key, value]) => {
				values.set(key, value);
			});

		this._enums.push({
			name: en.name,
			proto: en,
			parent,
			values,
		});
	}

	private parseType(typ: Type, parent?: ReflectionObject): void {
		this.addImport("protobufjs", ["Message", "Type"]);

		typ.nestedArray.forEach(nested => this.parse(nested, typ));

		let fields = typ.fieldsArray.map(f => this.parseField(f, typ));
		let oneOfs = typ.oneofsArray.map(o => this.parseOneOf(o, typ));

		this._types.push({
			name: typ.name,
			proto: typ,
			parent,
			fields,
			oneOfs,
		});
	}

	private parseField(field: Field, parent: Type): TsField {
		return {
			name: field.name,
			comment: field.comment,
			proto: field,
			parent,
			id: field.id,
			decorator: this.resolveFieldType(field, parent),
		};
	}

	private parseOneOf(oneOf: OneOf, parent?: ReflectionObject): TsOneOf {
		this.addImport("protobufjs", ["OneOf"]);

		return {
			name: oneOf.name,
			proto: oneOf,
			parent,
			fieldNames: oneOf.fieldsArray.map(f => f.name),
		};
	}

	private resolveFieldType(field: Field|MapField, parent: Type): TsFieldDecorator {
		if (field instanceof MapField) {
			this.addImport("protobufjs", ["MapField"]);

			let name = "MapField";
			let args = [`"${field.keyType}"`];
			let tsKeyType = this.tsTypeForScalar(field.keyType);
			let tsValueType = "";

			if (isScalarType(field.type)) {
				args.push(`"${field.type}"`);
				tsValueType = this.tsTypeForScalar(field.type);
			}
			else {
				tsValueType = this.resolveMessageType(field.type, parent);
				args.push(tsValueType);
			}

			let typeAnnotation = `Record<${tsKeyType}, ${tsValueType}>`;

			return { name, args, typeAnnotation };
		}

		this.addImport("protobufjs", ["Field"]);

		let name = "Field";

		if (isScalarType(field.type)) {
			let typeAnnotation = this.tsTypeForScalar(field.type);
			let args = [`"${field.type}"`];

			return { name, args, typeAnnotation };
		}

		let typeName = this.resolveMessageType(field.type, parent);

		return {
			name,
			args: [typeName],
			typeAnnotation: typeName,
		};
	}

	private addImport(source: string, targets: string[]): void {
		if (!this._imports.has(source)) {
			this._imports.set(source, new Set<string>());
		}

		let imports = this._imports.get(source)
		targets.forEach(target => {
			imports.add(target);
		});
	}

	private tsTypeForScalar(scalarType: string): string {
		if (/^(float|double|[us]?(int|fixed)32)$/.test(scalarType)) {
			return "number";
		}
		if (/^[us]?(int|fixed)64$/.test(scalarType)) {
			this.addImport("long", ["Long"]);
			return "Long";
		}
		switch (scalarType) {
			case "bool": return "boolean";
			case "bytes": return "unknown";
			case "string": return "string";
			default: {
				throw new Error(`'${scalarType}' is not a scalar type.`);
			}
		}
	}

	private resolveMessageType(messageType: string, parent: Type): string {
		// let siblingTypes = this._types.filter(t => t.parent === parent);
		// let match = siblingTypes.find(t => t.name === messageType);
		let p = findParent(this._types, parent, t => t.name === messageType);
		if (p) {
			return `${p.name}_${messageType}`;
		}
		return messageType;
	}
}

function findParent(
	haystack: TsType[],
	parent: Type|Namespace,
	pred: (it: TsType) => boolean,
) {
	let siblings = haystack.filter(t => t.parent === parent);
	let match = siblings.find(pred);

	if (match) {
		return parent;
	}

	if (!parent.parent || parent.parent === parent.root) {
		return null;
	}

	return findParent(haystack, parent.parent, pred);
}

function isScalarType(typ: string): boolean {
	return /^(float|double|[us]?(int|fixed)(32|64))|bool|bytes|string$/
		.test(typ);
}

function upperSnakeToPascal(value: string): string {
	let result = "";

	for (let i = 0; i < value.length; i++) {
		let char = value.charAt(i);

		if (i === 0) {
			result += char;
			continue;
		}

		if (char === "_") continue;

		if (value.charAt(i - 1) === "_") {
			result += char;
			continue;
		}

		result += char.toLowerCase();
	}

	return result;
}

function pascalToCamel(value: string): string {
	return value.charAt(0).toLowerCase() + value.substring(1);
}
