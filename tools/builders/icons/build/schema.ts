import { JsonObject } from "@angular-devkit/core"

export interface Options extends JsonObject {
	pathToSvgs: string;
	outputPath: string;
	outFileName: string;
}
