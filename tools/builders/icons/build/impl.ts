import { createBuilder, BuilderContext } from "@angular-devkit/architect";
import { promises as fs } from "fs";
import * as path from "path";
import { parse, Node } from "svg-parser";

import { Options } from "./schema";

export default createBuilder(runBuilder);

async function runBuilder(opts: Options, ctx: BuilderContext) {
	try {
		let { workspaceRoot } = ctx;
		let svgsPath = path.join(workspaceRoot, opts.pathToSvgs);
		let outPath = path.join(workspaceRoot, opts.outputPath);
		let outFile = path.join(outPath, opts.outFileName);

		let files = (await fs.readdir(svgsPath))
			.filter(f => path.extname(f) === ".svg")
			.map(filename => path.join(svgsPath, filename));

		let svgs = (await Promise.all(files.map(f => fs.readFile(f))))
			.map((buf, idx) => ({
				name: path.basename(files[idx], ".svg"),
				content: buf.toString(),
			}));

		let defs = svgs.map(({ name, content }) => {
			let ast = parse(content);
			return {
				name,
				svg: emitNode(ast.children[0]),
			};
		});

		let outContent = [
			"// This file is automatically generated! Run `nx build-icons <project>` to update it.",
			"",
			"export const ICONS = {",
			...defs.map(({ name, svg }) => `\t${name}: \`${svg}\`,`),
			"};",
			"",
		].join("\n");

		await fs.writeFile(outFile, outContent);

		return { success: true };
	}
	catch (err) {
		return {
			success: false,
			error: err.stack,
		}
	}
}

function emitNode(node: Node): string {
	if (node.type !== "element") return "";

	// Opening tag name
	let result = `<${node.tagName} `;

	// Tag attributes
	if (node.tagName === "svg") {
		if ("xmlns" in node.properties) {
			result += `xmlns="${node.properties.xmlns}" `
		}
		if ("viewBox" in node.properties) {
			result += `viewBox="${node.properties.viewBox}" `;
		}
	} else {
		let props = Object
			.entries(node.properties)
			.filter(([k]) => (
				k !== "fill" &&
				k !== "style" &&
				!k.startsWith("stroke") &&
				!k.startsWith("data-")
			));

		props.forEach(([key, value]) => {
			result += `${key}="${value}" `;
		});
	}

	// Append children
	if (node.children.length) {
		result += ">";
		result += node.children.filter(isNode).map(emitNode);
		result += `</${node.tagName}>`;
	} else {
		result += `/>`;
	}

	return result;
}

function isNode(node: string|Node): node is Node {
	return typeof node !== "string";
}
