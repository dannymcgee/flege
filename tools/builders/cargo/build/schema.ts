export interface Options {
	/** Watch source directory and rebuild on changes. */
	watch?: boolean;
	/**
	 * Directory for all generated artifacts and intermediate files. Defaults to
	 * `/{nx_workspace_root}/dist/apps/{project_name}/`
	 */
	targetDir?: string;
}
