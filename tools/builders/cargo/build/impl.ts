import {
	BuilderContext,
	BuilderOutput,
	createBuilder,
} from "@angular-devkit/architect";
import * as chalk from "chalk";
import * as cp from "child_process";
import * as chokidar from "chokidar";
import { promises as fs, Stats } from "fs";
import * as path from "path";
import { from, Observable } from "rxjs";
import { debounceTime, filter, switchMap } from "rxjs/operators";

import { Options as CLIOptions } from "./schema";

interface Options extends CLIOptions {
	cwd: string;
	srcRoot: string;
}

export default createBuilder(runBuilder);

function runBuilder(opts: CLIOptions, ctx: BuilderContext) {
	return new Promise<BuilderOutput>(async (resolve) => {
		const success = () => resolve({ success: true });
		const error = (err: Error) => {
			console.log(chalk.bold.redBright(err.stack));
			resolve({ success: false });
		};

		let options = await normalizeOptions(opts, ctx).catch(error) as Options;

		// Watch src directory and re-build on changes
		if (options.watch) {
			await buildLib(options).catch(error);
			console.log("Build completed. Watching for changes...");

			fromChokidar(options.srcRoot)
				.pipe(
					debounceTime(100),
					filter(e => e.name === "change"),
					filter(e => e.path.endsWith(".rs")),
					switchMap(() => from(buildLib(options))),
				)
				.subscribe({
					next: () => {
						console.log("Build completed. Watching for changes...");
					},
					complete: success,
					error,
				});
		}

		// Build once and resolve
		else {
			buildLib(options)
				.then(success)
				.catch(error);
		}
	});
}

function buildLib(options: Options) {
	return new Promise<void>((resolve, reject) => {
		cp.spawn("cargo", [
			"build",
			"--target-dir", options.targetDir,
		], {
			stdio: "inherit",
			cwd: options.cwd,
		})
			.on("close", resolve)
			.on("exit", resolve)
			.on("error", reject);
	});
}

async function normalizeOptions(
	cliOptions: CLIOptions,
	ctx: BuilderContext,
): Promise<Options> {
	let { workspaceRoot } = ctx;
	let result = {} as Options;

	// Parse the workspace configuration
	let workspaceJson = await fs
		.readFile(path.join(workspaceRoot, "angular.json"))
		.then(buffer => JSON.parse(buffer.toString()));

	// Find the project configuration
	let project = workspaceJson?.projects?.[ctx.target.project];
	if (!project)
		throw new Error(`Couldn't find project "${ctx.target.project}"`);

	// Set the "options" that aren't exposed to the user
	let projectPath = path.join(workspaceRoot, project.root);
	result.cwd = path.relative(process.cwd(), projectPath);
	result.srcRoot = path.join(workspaceRoot, project.sourceRoot);

	// Parse the options specified in the workspace config
	let target = project?.architect?.[ctx.target.target];
	let targetOptions = target?.options;
	let configuration = target?.configurations?.[ctx.target.configuration];

	// Mixin the default options
	result = {
		...result,
		watch: false,
		targetDir: `dist/apps/${ctx.target.project}`,
	}

	// Mixin the taget options
	// (e.g. architect > [target] > options)
	if (targetOptions) result = {
		...result,
		...targetOptions,
	};

	// Mixin the configuration options
	// (e.g. architect > [target] > configurations > [configuration])
	if (configuration) result = {
		...result,
		...configuration,
	};

	// Mixin any args passed via command line
	// (Caveat: the options argument is passed with any unset keys that are
	// specified in the schema explicitly set to `undefined`, and the spread
	// operator will treat those `undefined`s as actual values which should
	// overwrite the respective keys in the source object. To avoid that, we'll
	// need to mix-in the actual values manually.)
	Object.entries(cliOptions)
		.filter(([, value]) => value !== undefined)
		.forEach(([key, value]) => {
			result[key] = value;
		});

	// Convert all paths to be relative to the `cargo` process's cwd
	let absTargetDir = path.join(workspaceRoot, result.targetDir);
	result.targetDir = path.relative(projectPath, absTargetDir);

	return result;
}

interface ChokidarEvent {
	name: "add"|"addDir"|"change"|"unlink"|"unlinkDir";
	path: string;
	stats?: Stats;
}

function fromChokidar(path: string) {
	return new Observable<ChokidarEvent>((subscriber) => {
		let watcher = chokidar.watch(path);

		watcher.on("error", (err) => {
			subscriber.error(err);
		});

		watcher.on("all", (name, path, stats) => {
			subscriber.next({ name, path, stats });
		});

		return () => {
			watcher.close();
		}
	});
}
