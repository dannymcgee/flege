export function rem(pxSize: number): string {
	return `${pxSize / 16}rem`;
}
