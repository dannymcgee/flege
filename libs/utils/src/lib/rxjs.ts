import { ElementRef } from "@angular/core";

import { fromEvent, Observable, ReplaySubject } from "rxjs";
import { filter } from "rxjs/operators";

import { Predicate, partition } from "./array";
import { ModifierKey } from "./types";

export function partitionValue<T>(
	predicate: Predicate<T>,
	source: Observable<T[]>,
): [Observable<T[]>, Observable<T[]>] {
	let subsetA$ = new ReplaySubject<T[]>();
	let subsetB$ = new ReplaySubject<T[]>();

	let next = (value: T[]): void => {
		let [subsetA, subsetB] = partition(predicate)(value);

		subsetA$.next(subsetA);
		subsetB$.next(subsetB);
	};
	let complete = (): void => {
		subsetA$.complete();
		subsetB$.complete();
	};
	let error = complete;

	source.subscribe({ next, error, complete });

	return [
		subsetA$.asObservable(),
		subsetB$.asObservable(),
	];
}

export function fromKeydown(
	eventTarget: ElementRef<Element> | Element | Document | Window,
	primaryKey?: string | RegExp,
	modifiers?: ModifierKey[],
): Observable<KeyboardEvent> {
	let target = eventTarget instanceof ElementRef
		? eventTarget.nativeElement
		: eventTarget;

	let keydown$ = fromEvent<KeyboardEvent>(target, "keydown");

	if (primaryKey) {
		let predicate: Predicate<KeyboardEvent> =
			typeof primaryKey === "string"
				? evt => evt.key.toUpperCase() === primaryKey.toUpperCase()
				: evt => primaryKey.test(evt.key);

		keydown$ = keydown$.pipe(filter(predicate));
	}

	if (modifiers) {
		keydown$ = keydown$.pipe(
			filter(evt => modifiers.every(mod => evt.getModifierState(mod))),
		);
	}

	return keydown$;
}
