/* eslint-disable @typescript-eslint/ban-types */
import { ɵmarkDirty as markDirty } from "@angular/core";
import { Observable } from "rxjs";

import { decorateMethod } from "../decorate";

export function DetectChanges() {
	return (proto: Object, propName: string) => {
		let subscriptionSym = Symbol("detectChangesSubscription");

		decorateMethod(proto, "ngOnInit", function () {
			if (!this[propName] || !(this[propName] instanceof Observable)) {
				throw new Error(
					`[@DetectChanges() ${proto.constructor.name}.${propName}] `
						+ `Expected initialized observable, found ${this[propName]}`
				);
			}
			this[subscriptionSym] = this[propName].subscribe(() => {
				markDirty(this);
			});
		});

		decorateMethod(proto, "ngOnDestroy", function () {
			this[subscriptionSym].unsubscribe();
		});
	}
}
