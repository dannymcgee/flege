/* eslint-disable @typescript-eslint/ban-types */
import "reflect-metadata";
import { Matrix4, Quaternion, Vector3 } from "@math.gl/core";

export function FromArray() {
	return (proto: Object, propName: string) => {
		let symbol = Symbol(propName);

		let typeName: string = Reflect
			.getMetadata("design:type", proto, propName)
			.name;

		let Ctor = (typeName => {
			switch (typeName) {
				case "Vector3": return Vector3;
				case "Quaternion": return Quaternion;
				case "Matrix4": return Matrix4;
				default:
					throw new Error(
						`FromArray not implemented for type '${typeName}'`
					);
			}
		})(typeName);

		Object.defineProperty(proto, propName, {
			get() {
				return this[symbol];
			},
			set(value) {
				if (!(value instanceof Ctor)) {
					this[symbol] = new Ctor(value);
				} else {
					this[symbol] = value;
				}
			},
			enumerable: true,
			configurable: false,
		});
	}
}
