import {
	Directive,
	ElementRef,
	EventEmitter,
	HostBinding,
	Input,
	OnDestroy,
	Output,
} from "@angular/core";
import { Fn } from "../../types";

import { KeybindRegistry } from "./keybind-registry.service";

@Directive({
	selector: "[keybind]",
})
export class KeybindDirective implements OnDestroy {
	@HostBinding("ariaKeyShortcuts")
	@Input()
	get keybind() { return this._keybind; }
	set keybind(value) {
		this.registerKeybind(value);
		this._keybind = value;
	}
	private _keybind: string;

	/**
	 * An optional hook for executing keybind-specific behavior. If this output
	 * is not listened to, the host element will instead emit a synthetic "click"
	 * event when the shortcut is pressed.
	 */
	@Output() keyShortcut = new EventEmitter<KeyboardEvent>();

	private get _element() {
		return this._elementRef.nativeElement;
	}

	constructor(
		private _elementRef: ElementRef<HTMLElement>,
		private _registry: KeybindRegistry,
	) {}

	ngOnDestroy(): void {
		if (this.keybind) this._registry.unregister(this.keybind);
	}

	private registerKeybind(value: string): void {
		let normalized = this.normalize(value);
		let prevNormalized = this.normalize(this._keybind);

		if (normalized && prevNormalized && normalized !== prevNormalized) {
			this._registry.unregister(prevNormalized);
		}

		let handler: Fn<[KeyboardEvent?], void> =
			this.keyShortcut.observers.length
				? event => this.keyShortcut.emit(event)
				: () => {
					if (this._element && !this._element["disabled"]) {
						this._element.click();
					}
				};

		this._registry.register(normalized, handler);
	}

	private normalize(rawKeybind?: string): string {
		if (!rawKeybind) return "";

		return rawKeybind
			.split("+")
			.map(str => str.trim().replace(/^Ctrl$/i, "Control"))
			.join("+");
	}
}
