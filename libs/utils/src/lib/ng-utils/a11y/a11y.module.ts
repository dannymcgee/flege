import { NgModule } from "@angular/core";
import { KeybindDirective } from "./keybind.directive";

@NgModule({
	imports: [],
	declarations: [
		KeybindDirective,
	],
	exports: [
		KeybindDirective,
	],
})
export class A11yModule {}
