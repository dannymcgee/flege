import { Injectable } from "@angular/core";
import { Subscription } from "rxjs";

import { partition } from "../../array";
import { fromKeydown } from "../../rxjs";
import { Fn, ModifierKey, MODIFIER_KEYS } from "../../types";


@Injectable({
	providedIn: "root",
})
export class KeybindRegistry {
	private _registry = new Map<string, Subscription[]>();

	register(keybind: string, handler: Fn<[KeyboardEvent?], void>): void {
		if (this._registry.has(keybind)) {
			console.warn(`Duplicate binding registered for key shortcut ${keybind}`);
		} else {
			this._registry.set(keybind, []);
		}

		let keys = keybind.split("+").map(str => str.trim());
		let [modifiers, primary] = partition(isModifier)(keys);
		let primaryKey = primary[0];

		let subscription = fromKeydown(
			window,
			primaryKey,
			modifiers as ModifierKey[]
		)
			.subscribe(handler);

		this._registry.get(keybind)!.push(subscription);
	}

	unregister(keybind: string): void {
		if (this._registry.has(keybind)) {
			this._registry.get(keybind)!.forEach(sub => sub.unsubscribe());
			this._registry.delete(keybind);
		}
	}
}

function isModifier(key: string|ModifierKey): key is ModifierKey {
	return MODIFIER_KEYS.includes(key as any);
}
