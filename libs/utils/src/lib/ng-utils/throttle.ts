import { noop } from "rxjs";
import { decorateMethod } from "../decorate";
import { Fn } from "../types";

export function Throttle() {
	return (
		proto: Object,
		methodName: string,
		descriptor: TypedPropertyDescriptor<Fn<any[], void>>,
	) => {
		let frameRequestSym = Symbol("frameRequest");
		let originalMethod = (proto[methodName] ?? noop) as Fn<any[], void>;

		decorateMethod(proto, "ngOnDestroy", function () {
			if (this[frameRequestSym]) {
				cancelAnimationFrame(this[frameRequestSym]);
			}
		});

		descriptor.value = function (...args: any[]) {
			if (this[frameRequestSym]) {
				cancelAnimationFrame(this[frameRequestSym]);
			}
			this[frameRequestSym] = requestAnimationFrame(() => {
				originalMethod.call(this, ...args);
			});
		}
		descriptor.configurable = false;

		return descriptor;
	}
}
