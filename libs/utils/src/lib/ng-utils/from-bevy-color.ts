/* eslint-disable @typescript-eslint/ban-types */
import * as chroma from "chroma-js";

interface BevyColor {
	Rgba: {
		red: number;
		green: number;
		blue: number;
		alpha: number;
	}
}

export function FromBevyColor() {
	return (proto: Object, propName: string) => {
		let symbol = Symbol(propName);

		Object.defineProperty(proto, propName, {
			get() {
				return this[symbol];
			},
			set(value: BevyColor) {
				if (!value) return;

				let { Rgba } = value;
				let { red, green, blue, alpha } = Rgba;
				let normalized = [red, green, blue, alpha]
					.map(channel => Math.round(channel * 255));
				let rgb = normalized.slice(0, 3) as [number, number, number];
				let a = normalized[3];

				this[symbol] = chroma
					.rgb(...rgb)
					.alpha(a);
			},
			enumerable: true,
			configurable: false,
		});
	}
}
