import { noop } from "rxjs";
import { decorateMethod } from "../decorate";
import { Fn } from "../types";

export function Debounce(ms: number) {
	return (
		proto: Object,
		methodName: string,
		descriptor: TypedPropertyDescriptor<Fn<any[], void>>,
	) => {
		let timeoutSym = Symbol("timeout");
		let originalMethod = (proto[methodName] ?? noop) as Fn<any[], void>;

		decorateMethod(proto, "ngOnDestroy", function () {
			if (this[timeoutSym]) {
				clearTimeout(this[timeoutSym]);
			}
		});

		descriptor.value = function (...args: any[]) {
			if (this[timeoutSym]) {
				clearTimeout(this[timeoutSym]);
			}
			this[timeoutSym] = setTimeout(() => {
				originalMethod.call(this, ...args);
			}, ms);
		}
	}
}
