import { Injectable } from "@angular/core";
import { array } from "../array";
import { Loop } from "./loop";

class Stack<T> extends Array<T> {
	constructor(private _capacity?: number) {
		super();
	}

	peek(): T|null {
		if (this.length) return this[this.length - 1];
		return null;
	}

	push(...items: T[]): number {
		super.push(...items);
		if (!this._capacity) return this.length;

		while (this.length > this._capacity) {
			this.pop();
		}
		return this.length;
	}
}

@Injectable({
	providedIn: "root",
})
export class GlobalFocusManager {
	private _focusHistory = new Stack<HTMLElement>(20);

	constructor() {
		this.watchForChanges();
	}

	getMostRecentValidFocusTarget(): HTMLElement|null {
		this.drainNullRefs();
		if (!this._focusHistory.length) return null;

		let candidate = this._focusHistory.pop()!;
		if (array(document.querySelectorAll("*")).includes(candidate))
			return candidate;

		return this.getMostRecentValidFocusTarget();
	}

	@Loop()
	private watchForChanges(): void {
		this.drainNullRefs();

		let last = this._focusHistory.peek();
		let current = document.activeElement;
		if (current && current !== document.body && current !== last) {
			this._focusHistory.push(current as HTMLElement);
		}
	}

	private drainNullRefs(): void {
		while (this._focusHistory.length && !this._focusHistory.peek()) {
			this._focusHistory.pop();
		}
	}
}
