import {
	Directive,
	EmbeddedViewRef,
	Input,
	TemplateRef,
	ViewContainerRef,
} from "@angular/core";

interface UnwrapDirectiveContext<T> {
	$implicit: T;
	unwrap: T;
}

@Directive({
	selector: "[unwrap]",
})
export class UnwrapDirective<T> {

	@Input("unwrap")
	get value(): T { return this._value; }
	set value(value: T) {
		this._value = value;
		this.updateView();
	}
	private _value: T;

	private _viewRef?: EmbeddedViewRef<UnwrapDirectiveContext<T>>;

	constructor(
		private _templateRef: TemplateRef<UnwrapDirectiveContext<T>>,
		private _viewContainerRef: ViewContainerRef,
	) {}

	private updateView(): void {
		if (!this._viewRef || this._viewRef.context?.$implicit == null) {
			this._viewContainerRef.clear();

			this._viewRef = this._viewContainerRef
				.createEmbeddedView(this._templateRef, {
					$implicit: this.value,
					unwrap: this.value,
				});
		}
		else {
			this._viewRef.context.$implicit = this._value;
			this._viewRef.context.unwrap = this._value;
			setTimeout(() => {
				this._viewRef!.detectChanges();
			});
		}
	}

}
