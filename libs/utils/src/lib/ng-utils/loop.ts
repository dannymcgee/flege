/* eslint-disable @typescript-eslint/ban-types */
import { decorateMethod } from "../decorate";
import { Fn } from "../types";

export function Loop() {
	return (
		proto: Object,
		methodName: string,
		descriptor: TypedPropertyDescriptor<Fn<any[], void>>,
	) => {
		let frameRequestSym = Symbol("frameRequest");

		decorateMethod(proto, "ngOnInit", function () {
			this[frameRequestSym] = requestAnimationFrame(() => {
				this[methodName].call(this);
			});
		});

		decorateMethod(proto, "ngOnDestroy", function () {
			if (this[frameRequestSym]) {
				cancelAnimationFrame(this[frameRequestSym]);
			}
		});

		return decorateMethod(proto, methodName, function () {
			this[frameRequestSym] = requestAnimationFrame(() => {
				this[methodName].call(this);
			});
		}, descriptor);
	}
}
