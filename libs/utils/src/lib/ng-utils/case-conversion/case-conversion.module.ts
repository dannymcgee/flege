import { NgModule } from "@angular/core";

import {
	CamelCasePipe,
	KebabCasePipe,
	PascalCasePipe,
	SnakeCasePipe,
	TitleCasePipe,
} from "./case-conversion.pipe";

@NgModule({
	declarations: [
		CamelCasePipe,
		KebabCasePipe,
		PascalCasePipe,
		SnakeCasePipe,
		TitleCasePipe,
	],
	exports: [
		CamelCasePipe,
		KebabCasePipe,
		PascalCasePipe,
		SnakeCasePipe,
		TitleCasePipe,
	],
})
export class CaseConversionModule {}
