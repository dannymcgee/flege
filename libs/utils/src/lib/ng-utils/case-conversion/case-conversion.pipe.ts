import { Pipe, PipeTransform } from "@angular/core";
import { camelCase, kebabCase, pascalCase, snakeCase, titleCase } from "../../case-conversion";

@Pipe({
	name: "titleCase",
})
export class TitleCasePipe implements PipeTransform {
	transform(value: string): string {
		if (!value) return "";
		return titleCase(value);
	}
}

@Pipe({
	name: "kebabCase",
})
export class KebabCasePipe implements PipeTransform {
	transform(value: string): string {
		if (!value) return "";
		return kebabCase(value);
	}
}

@Pipe({
	name: "camelCase",
})
export class CamelCasePipe implements PipeTransform {
	transform(value: string): string {
		if (!value) return "";
		return camelCase(value);
	}
}

@Pipe({
	name: "pascalCase",
})
export class PascalCasePipe implements PipeTransform {
	transform(value: string): string {
		if (!value) return "";
		return pascalCase(value);
	}
}

@Pipe({
	name: "snakeCase",
})
export class SnakeCasePipe implements PipeTransform {
	transform(value: string): string {
		if (!value) return "";
		return snakeCase(value);
	}
}
