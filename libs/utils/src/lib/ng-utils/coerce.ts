import { coerceBooleanProperty } from "@angular/cdk/coercion";
import { Fn } from "../types";

type Coerced<Ctor> = Ctor extends (args?: any) => infer R ? R : never;

export function Coerce(
	typeHint: typeof Number | typeof Boolean
): PropertyDecorator {
	return (proto, propName: string) => {
		let symbol = Symbol(propName);
		let coerce: Fn<[any], Coerced<typeof typeHint>|null>;

		switch (typeHint) {
			case Number:
				coerce = (value: any) => {
					if (value == null) return null;
					return Number(value);
				};
				break;
			case Boolean:
				coerce = coerceBooleanProperty;
				break;
		}

		Object.defineProperty(proto, propName, {
			get(): Coerced<typeof typeHint> {
				return this[symbol];
			},
			set(value: any) {
				this[symbol] = coerce(value);
			},
			enumerable: true,
			configurable: false,
		});
	}
}
