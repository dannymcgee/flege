/* eslint-disable @typescript-eslint/ban-types */
import { noop } from "rxjs";
import { Fn } from "./types";

const SIDE_EFFECTS_MAP = new WeakMap<Object, Map<string, Fn<any[], void>[]>>();

export function decorateProperty(
	proto: Object,
	propName: string,
	sideEffect: Fn,
): void {
	let sideEffects = getSideEffects(proto, propName);
	sideEffects.push(sideEffect);

	if (Object.getOwnPropertyDescriptor(proto, propName)?.configurable) {
		createPropDescriptor(proto, propName);
	}
}

export function decorateMethod(
	proto: Object,
	propName: string,
	sideEffect: Fn,
	descriptor?: TypedPropertyDescriptor<any>,
): typeof descriptor {
	let sideEffects = getSideEffects(proto, propName);
	sideEffects.push(sideEffect);

	descriptor ??= Object.getOwnPropertyDescriptor(proto, propName);
	if (descriptor?.configurable) {
		modifyMethodDescriptor(proto, propName, descriptor);
	} else if (!descriptor) {
		createMethodDescriptor(proto, propName);
	}

	return descriptor;
}

function getSideEffects(proto: Object, propName: string) {
	if (!SIDE_EFFECTS_MAP.has(proto))
		SIDE_EFFECTS_MAP.set(proto, new Map<string, Fn<any[], void>[]>());

	let sideEffects = SIDE_EFFECTS_MAP.get(proto)!;

	if (!sideEffects.has(propName))
		sideEffects.set(propName, []);

	return sideEffects.get(propName)!;
}

function createPropDescriptor(proto: Object, propName: string) {
	let sym = Symbol(propName);

	Object.defineProperty(proto, propName, {
		get() {
			return this[sym];
		},
		set(value: any) {
			let sideEffects = getSideEffects(proto, propName);
			sideEffects.forEach(sideEffect => {
				sideEffect.call(this, value);
			});
			this[sym] = value;
		},
		enumerable: true,
		configurable: false,
	});
}

function createMethodDescriptor(proto: Object, propName: string) {
	let originalMethod = (proto[propName] ?? noop) as Fn<any[], any>;

	Object.defineProperty(proto, propName, {
		value(...args: any[]) {
			let sideEffects = getSideEffects(proto, propName);
			sideEffects.forEach(sideEffect => {
				sideEffect.call(this);
			});

			return originalMethod.call(this, ...args);
		},
		enumerable: true,
		configurable: false,
	});
}

function modifyMethodDescriptor(
	proto: Object,
	propName: string,
	descriptor: TypedPropertyDescriptor<any>,
) {
	let originalMethod = (proto[propName] ?? noop) as Fn<any[], any>;

	descriptor.value = function (...args: any[]) {
		let sideEffects = getSideEffects(proto, propName);
		sideEffects.forEach(sideEffect => {
			sideEffect.call(this);
		});

		return originalMethod.call(this, ...args);
	}
	descriptor.configurable = false;
}
