import { Fn } from "./types";
import { Flow } from "./fp.types";

// This was basically stolen from lodash/fp, but adapted to take advantage of ES6+ features.
// V8 is almost comically slow at executing low-level `for` and `while` loops;
// Array.prototype methods like `reduce` are much, much faster.

export const flow: Flow = (...funcs: Fn<any[], any>[]) => {
	return (...args: any[]) => {
		let init = funcs.length > 0
			? funcs.shift()!(...args)
			: args[0];

		return funcs.reduce((accum, func) => func(accum), init);
	}
}

/**
 * Convenience function to seed a `flow` pipe with an initial value in a way
 * that's more type-inference friendly than passing it as an argument to the
 * function returned by `flow`.
 */
export const seed = <T>(value: T) => () => value;

export function tap<T>(func: Fn<[T], void>) {
	return (value: T) => {
		func(value);

		return value;
	}
}

export function debug<T>(prefix?: string) {
	return (value: T) => {
		if (prefix) {
			console.log(prefix, value);
		} else {
			console.log(value);
		}

		return value;
	}
}
