import { ElementRef } from "@angular/core";
import { coerceElement } from "@angular/cdk/coercion";

export type ElementId = string;

/**
 * Returns a formatted string of all HTML and text contents of a DOM node.
 * Can be useful for sanity-check console logs while writing tests,
 * since you can't just inspect the DOM with devtools.
 */
export function printDomTree(node: Node, indent = 0): string {
	let indentText = "    ";
	let indents: string = Array(indent).fill(indentText).join("");

	let content = "";
	if (node instanceof Element) {
		let classContent = node.className ? ` class="${node.className}"` : "";
		content += `<${node.tagName.toLowerCase()}${classContent}`;

		let atts = Array
			.from(node.attributes)
			.filter((attr) => attr.name !== "class");
		if (atts.length) {
			atts.forEach((attr) => {
				content += `\n${indents}${indentText}`;
				content += `${attr.name}="${attr.value}"`;
			});
			content += `\n${indents}`;
		}
		content += ">";
	} else if (node instanceof Text) {
		content = node.textContent?.trim() ?? "";
	}

	let result = content ? `\n${indents}${content}` : "";

	indent++;
	let childContent = "";
	Array.from(node.childNodes).forEach((child) => {
		childContent += printDomTree(child, indent);
	});

	result += childContent;

	if (node instanceof Element) {
		if (childContent) {
			result += `\n${indents}`;
		}
		result += `</${node.tagName.toLowerCase()}>`;
	}

	return result;
}

export function findEncapsulationId(
	element: ElementRef | Element,
	prefix?: "_ngcontent-" | "_nghost-"
): string | null {
	let { attributes } = coerceElement(element);

	let findForPrefix = (attributes: NamedNodeMap, prefix: string): string | null => {
		let pattern = new RegExp(`^${prefix}(.+)`);

		for (let attr of Array.from(attributes)) {
			if (attr.name.startsWith(prefix)) {
				return attr.name.match(pattern)![1];
			}
		}
		return null;
	};

	if (prefix) return findForPrefix(attributes, prefix);

	return findForPrefix(attributes, "_nghost-")
		|| findForPrefix(attributes, "_ngcontent-");
}

export function elementId(prefix?: string): ElementId {
	let random = (): string => Math.round(Math.random() * Date.now()).toString(16);
	let pre = prefix ?? random();

	return `${pre}-${random()}`;
}
