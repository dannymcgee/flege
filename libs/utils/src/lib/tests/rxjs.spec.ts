import { combineLatest, of } from "rxjs";
import { partitionValue } from "../rxjs";

describe("partitionValue", () => {
	it("should partition an observable of arrays by applying a predicate", (done) => {
		let nums$ = of([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
		let [evens$, odds$] = partitionValue((val: number) => val % 2 === 0, nums$);

		combineLatest([evens$, odds$])
			.subscribe(([evens, odds]) => {
				expect(evens).toEqual([0, 2, 4, 6, 8]);
				expect(odds).toEqual([1, 3, 5, 7, 9]);

				done();
			});
	});
});
