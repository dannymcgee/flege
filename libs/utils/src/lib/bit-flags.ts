export class Flag extends Number {
	private _value: number;

	constructor(value: number) {
		super(value);
		this._value = value;
	}

	and(value: number|Flag) {
		return new Flag(this._value |= value as number);
	}

	or(value: number|Flag) {
		return new Flag(this._value | value as number);
	}

	is(value: number|Flag) {
		return !!(this._value & value as number);
	}
}

export class BitFlags<K extends string> {
	readonly flags: Record<K, Flag>;

	constructor(...flags: K[]) {
		let result = {} as Record<K, Flag>;

		flags.forEach((flag, idx) => {
			result[flag] = new Flag(1 << idx);
		});

		this.flags = result;
	}
}
