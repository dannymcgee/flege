import { BitFlags, Flag } from "./bit-flags";

export const Case = new BitFlags(
	"Title",
	"Upper",
	"Camel",
	"Spaced",
	"Kebab",
	"Snake",
).flags;

export function identifyCase(text: string): Flag {
	let result = new Flag(0);

	if (/^[A-Z]/.test(text)) {
		result = result.and(Case.Title);
	}

	let alphas = text.split("").filter(char => /[a-zA-Z]/.test(char));
	if (alphas.every(char => /[A-Z]/.test(char))) {
		result = result.and(Case.Upper);
	}
	else if (!/[-_\s]/.test(text)) {
		return result.and(Case.Camel);
	}

	if (text.includes(" ")) {
		result = result.and(Case.Spaced);
	}

	if (text.includes("-")) {
		result = result.and(Case.Kebab);
	}

	if (text.includes("_")) {
		result = result.and(Case.Snake);
	}

	return result;
}

export function camelToKebabCase(text: string): string {
	return text.replace(/([a-z])([A-Z0-9])/g, "$1-$2").toLowerCase();
}

export function kebabCase(text: string): string {
	let sourceCase = identifyCase(text);

	if (sourceCase.is(Case.Camel)) {
		return camelToKebabCase(text);
	}

	return text
		.replace(/[^a-zA-Z0-9]/g, "-")
		.replace(/-+/g, "-")
		.toLowerCase();
}

export function titleCase(text: string): string {
	let sourceCase = identifyCase(text);

	if (sourceCase.is(Case.Camel)) {
		let result = "";

		for (let i = 0; i < text.length; i++) {
			let char = text.charAt(i);

			if (i === 0) {
				result += char.toUpperCase();
			} else if (/[A-Z]/.test(char)) {
				result += ` ${char}`;
			} else {
				result += char;
			}
		}

		return result;
	}

	return text
		.replace(/[^a-zA-Z0-9]/g, " ")
		.replace(/ +/g, " ")
		.split(" ")
		.map(word => word
			.split("")
			.map((char, idx) => idx === 0
				? char.toUpperCase()
				: char.toLowerCase()
			)
			.join("")
		)
		.join(" ");
}

export function camelCase(text: string): string {
	let sourceCase = identifyCase(text);
	let result = text;

	if (sourceCase.is(Case.Camel)) {
		if (sourceCase.is(Case.Title)) {
			result = result.charAt(0).toLowerCase() + result.substring(1);
		}
		return result;
	}

	if (sourceCase.is(Case.Kebab.or(Case.Snake).or(Case.Spaced))) {
		result = result
			.replace(/[^a-zA-Z0-9]/g, " ")
			.replace(/ +/g, " ")
			.split(" ")
			.map((word, w) => word
				.split("")
				.map((char, c) => c === 0 && w !== 0
					? char.toUpperCase()
					: char.toLowerCase()
				)
			)
			.join("");
	}

	return result;
}

export function pascalCase(text: string): string {
	let sourceCase = identifyCase(text);
	let result = text;

	if (sourceCase.is(Case.Camel)) {
		if (!sourceCase.is(Case.Title)) {
			result = result.charAt(0).toUpperCase() + result.substring(1);
		}
		return result;
	}

	if (sourceCase.is(Case.Kebab.or(Case.Snake).or(Case.Spaced))) {
		result = result
			.replace(/[^a-zA-Z0-9]/g, " ")
			.replace(/ +/g, " ")
			.split(" ")
			.map(word => word
				.split("")
				.map((char, c) => c === 0
					? char.toUpperCase()
					: char.toLowerCase()
				)
				.join("")
			)
			.join("");
	}

	return result;
}

export function snakeCase(text: string): string {
	let sourceCase = identifyCase(text);

	if (sourceCase.is(Case.Camel)) {
		return text.replace(/([a-z])([A-Z0-9])/g, "$1_$2").toLowerCase();
	}

	return text
		.replace(/[^a-zA-Z0-9]/g, "_")
		.replace(/_+/g, "_")
		.toLowerCase();
}
