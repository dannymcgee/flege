export class Queue<T> extends Array<T> {
	constructor(
		private _capacity?: number
	) {
		super();
	}

	enqueue(...items: T[]) {
		this.push(...items);

		if (this._capacity == null) return;

		while (this.length > this._capacity) {
			this.shift();
		}
	}

	dequeue(): T|null {
		return this.shift() ?? null;
	}

	clear(): void {
		while (this.length) this.shift();
	}
}
