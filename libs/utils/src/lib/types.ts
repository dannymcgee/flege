import { QueryList, SimpleChange } from "@angular/core";
import { Observable } from "rxjs";
import { keys } from "./object";

export type Obj = { [key: string]: any; };
export type Fn<Args extends any[] = [], R = void> = (...args: Args) => R;

/** Patched interface for Angular QueryList with correct typing for the `changes` property */
export interface QueryList$<T> extends QueryList<T> {
	changes: Observable<QueryList$<T>>;
}

export type Changes<T extends Record<string, any>> = {
	[propName in keyof T]: SimpleChange;
}

export enum ModifierKey {
	Alt = "Alt",
	AltGraph = "AltGraph",
	CapsLock = "CapsLock",
	Control = "Control",
	Fn = "Fn",
	FnLock = "FnLock",
	Hyper = "Hyper",
	Meta = "Meta",
	NumLock = "NumLock",
	ScrollLock = "ScrollLock",
	Shift = "Shift",
	Super = "Super",
	Symbol = "Symbol",
	SymbolLock = "SymbolLock",
}

export const MODIFIER_KEYS = keys(ModifierKey);

export type JsonValue =
	| string
	| number
	| boolean
	| null
	| JsonObject
	| JsonValue[];

export interface JsonObject {
	[key: string]: JsonValue;
}
