export * from "./lib/ts/api";

// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { JsonValue } from "@flege/utils";

export interface Success<T = undefined> {
	success: true;
	value: T;
}
export interface Failure {
	success: false;
	reason: string;
	info?: {
		message?: string;
		stack?: string;
	};
}
export type Result<T = undefined> = Success<T>|Failure;

export enum Severity {
	Info    = "info",
	Debug   = "debug",
	Warning = "warning",
	Error   = "error",
	Success = "success",
}

export interface TerminalLog {
	severity: Severity;
	timestamp: number|Date;
	label?: string;
	message: string|any;
	error?: Error;
	location?: {
		crate?: string;
		filename: string;
		line: number;
		column: number;
	}
}

export interface BevyScene {
	entities: BevyEntity[];
}

export interface BevyEntity {
	entity: number;
	components: BevyComponent[];
}

export type BevyComponent = {
	type: string;
	struct: BevyStruct;
} | {
	type: string;
	tuple_struct: BevyStruct[];
}

export type BevyStruct = Record<string, BevyValue>;

export interface BevyValue {
	type: string;
	value: JsonValue;
}

export interface NewProjectConfig {
	name: string;
	path: string;
}

export interface Project {
	name: string;
	path: string;
	metadata: CargoMetadata;
}

export interface CargoDependency {
	/** The name of the dependency. */
	name: string;
	/** The source ID of the dependency. May be null, see description for the package source. */
	source: string;
	/** The version requirement for the dependency. Dependencies without a version requirement have a value of "*". */
	req: string;
	/** The dependency kind. "dev", "build", or null for a normal dependency. */
	kind: "dev"|"build"|null;
	/** If the dependency is renamed, this is the new name for the dependency as a string.  null if it is not renamed. */
	rename: string|null;
	/** Boolean of whether or not this is an optional dependency. */
	optional: boolean;
	/** Boolean of whether or not default features are enabled. */
	uses_default_features: boolean;
	/** Array of features enabled. */
	features: string[];
	/** The target platform for the dependency. null if not a target dependency. */
	target: string;
	/** The file system path for a local path dependency. not present if not a path dependency. */
	path: string;
	/** A string of the URL of the registry this dependency is from. If not specified or null, the dependency is from the default registry (crates.io). */
	registry: string|null;
}

export interface CargoTarget {
	/**
	 * Array of target kinds.
	 * - lib targets list the `crate-type` values from the
	 *   manifest such as "lib", "rlib", "dylib",
	 *   "proc-macro", etc. (default ["lib"])
	 * - binary is ["bin"]
	 * - example is ["example"]
	 * - integration test is ["test"]
	 * - benchmark is ["bench"]
	 */
	kind: string[];
	/**
	 * Array of crate types.
	 * - lib and example libraries list the `crate-type` values
	 *   from the manifest such as "lib", "rlib", "dylib",
	 *   "proc-macro", etc. (default ["lib"])
	 * - all other target kinds are ["bin"]
	 */
	crate_types: string[];
	/** The name of the target. */
	name: string;
	/** Absolute path to the root source file of the target. */
	src_path: string;
	/** The Rust edition of the target. Defaults to the package edition. */
	edition: string;
	/** Array of required features. This property is not included if no required features are set. */
	"required-features"?: string[];
	/** Whether the target should be documented by `cargo doc`. */
	doc: boolean;
	/** Whether or not this target has doc tests enabled, and the target is compatible with doc testing. */
	doctest: boolean;
	/** Whether or not this target should be built and run with `--test` */
	test: boolean;
}

export interface CargoPackage {
	/** The name of the package. */
	name: string;
	/** The version of the package. */
	version: string;
	/** The Package ID, a unique identifier for referring to the package. */
	id: string;
	/** The license value from the manifest, or null. */
	license: string;
	/** The license-file value from the manifest, or null. */
	license_file: string;
	/** The description value from the manifest, or null. */
	description: string;
	/**
	 * The source ID of the package. This represents where
	 * a package is retrieved from.
	 * This is null for path dependencies and workspace members.
	 * For other dependencies, it is a string with the format:
	 * - "registry+URL" for registry-based dependencies.
	 *   Example: "registry+https://github.com/rust-lang/crates.io-index"
	 * - "git+URL" for git-based dependencies.
	 *   Example: "git+https://github.com/rust-lang/cargo?rev=5e85ba14aaa20f8133863373404cb0af69eeef2c#5e85ba14aaa20f8133863373404cb0af69eeef2c"
	 */
	source: string|null;
	/** Array of dependencies declared in the package's manifest. */
	dependencies: CargoDependency[];
	/** Array of Cargo targets. */
	targets: CargoTarget[];
	/** Set of features defined for the package. Each feature maps to an array of features or dependencies it enables. */
	features: Record<string, string[]>;
	/** Absolute path to this package's manifest. */
	manifest_path: string;
	/** Package metadata. This is null if no metadata is specified. */
	// eslint-disable-next-line @typescript-eslint/ban-types
	metadata: object|null;
	/** List of registries to which this package may be published. Publishing is unrestricted if null, and forbidden if an empty array. */
	publish: string[] | null;
	/** Array of authors from the manifest. Empty array if no authors specified. */
	authors: string[];
	/** Array of categories from the manifest. */
	categories: string[];
	/** Array of keywords from the manifest. */
	keywords: string[];
	/** The readme value from the manifest or null if not specified. */
	readme: string;
	/** The repository value from the manifest or null if not specified. */
	repository: string;
	/** The homepage value from the manifest or null if not specified. */
	homepage: string;
	/** The documentation value from the manifest or null if not specified. */
	documentation: string;
	/** The default edition of the package. Note that individual targets may have different editions. */
	edition: string;
	/** Optional string that is the name of a native library the package is linking to. */
	links: string|null;
}

export interface CargoDepGraphNode {
	/** The Package ID of this node. */
	id: string;
	/** The dependencies of this package, an array of Package IDs. */
	dependencies: string[];
	/** The dependencies of this package. This is an alternative to "dependencies" which contains additional information. In particular, this handles renamed dependencies. */
	deps: any[];
	/** Array of features enabled on this package. */
	features: string[];
}

export interface CargoMetadata {
	/** Array of all packages in the workspace. It also includes all feature-enabled dependencies unless --no-deps is used. */
	packages: CargoPackage[];
	/** Array of members of the workspace. Each entry is the Package ID for the package. */
	workspace_members: string[];
	/**
	 * The resolved dependency graph for the entire workspace. The enabled
	 * features are based on the enabled features for the "current" package.
	 * Inactivated optional dependencies are not listed.
	 *
	 * This is null if --no-deps is specified.
	 *
	 * By default, this includes all dependencies for all target platforms.
	 * The `--filter-platform` flag may be used to narrow to a specific
	 * target triple.
	 */
	resolve: {
		/** Array of nodes within the dependency graph. Each node is a package. */
		nodes: CargoDepGraphNode[];
		/** The root package of the workspace. This is null if this is a virtual workspace. Otherwise it is the Package ID of the root package. */
		root: string;
	};
	/** The absolute path to the build directory where Cargo places its output. */
	target_directory: string;
	/** The version of the schema for this metadata structure. This will be changed if incompatible changes are ever made. */
	version: number;
	/** The absolute path to the root of the workspace. */
	workspace_root: string;
	/** Workspace metadata. This is null if no metadata is specified. */
	// eslint-disable-next-line @typescript-eslint/ban-types
	metadata: object|null;
}
