import { Field, MapField, Message, Type } from "protobufjs";

export enum MessageType {
	Exit = 0,
	WindowHandle = 1,
	SetViewportRect = 2,
	SceneDescription = 3,
	PatchValue = 4,
}

export namespace request {
	export interface Header {
		msgType: MessageType;
	}
	export interface WindowHandle {
		handle: number;
	}
	export interface SetViewportRect {
		width: number;
		height: number;
		x: number;
		y: number;
		scaleFactor: number;
	}
	export interface PatchedValue {
		type: string;
		/** The type of `unknown` is `Buffer` in the main process, or `Uint8Array` in a renderer. */
		value: unknown;
	}
	export interface PatchValue {
		entity: number;
		component: string;
		values: Record<string, PatchedValue>;
	}
	export type Any = WindowHandle|SetViewportRect|PatchedValue|PatchValue;
}

export namespace response {
	export interface Header {
		msgType: MessageType;
	}
	export interface WindowHandle {
		handle: number;
	}
	export interface SceneDescription {
		content: string;
	}
	export type Any = WindowHandle|SceneDescription;
}

@Type.d("Request_Header")
class Request_Header extends Message<Request_Header> {
	@Field.d(1, MessageType) msgType: MessageType;
}

@Type.d("Request_WindowHandle")
class Request_WindowHandle extends Message<Request_WindowHandle> {
	@Field.d(2, "uint32") handle: number;
}

@Type.d("Request_SetViewportRect")
class Request_SetViewportRect extends Message<Request_SetViewportRect> {
	@Field.d(3, "uint32") width: number;
	@Field.d(4, "uint32") height: number;
	@Field.d(5, "int32") x: number;
	@Field.d(6, "int32") y: number;
	@Field.d(7, "float") scaleFactor: number;
}

@Type.d("Request_PatchedValue")
class Request_PatchedValue extends Message<Request_PatchedValue> {
	@Field.d(11, "string") type: string;
	/** The type of `unknown` is `Buffer` in the main process, or `Uint8Array` in a renderer. */
	@Field.d(12, "bytes") value: unknown;
}

@Type.d("Request_PatchValue")
class Request_PatchValue extends Message<Request_PatchValue> {
	@Field.d(8, "uint32") entity: number;
	@Field.d(9, "string") component: string;
	@MapField.d(10, "string", Request_PatchedValue) values: Record<string, Request_PatchedValue>;
}

@Type.d("Request")
export class Request extends Message<Request> {
	static Header = Request_Header;
	static WindowHandle = Request_WindowHandle;
	static SetViewportRect = Request_SetViewportRect;
	static PatchedValue = Request_PatchedValue;
	static PatchValue = Request_PatchValue;
}

@Type.d("Response_Header")
class Response_Header extends Message<Response_Header> {
	@Field.d(1, MessageType) msgType: MessageType;
}

@Type.d("Response_WindowHandle")
class Response_WindowHandle extends Message<Response_WindowHandle> {
	@Field.d(2, "uint32") handle: number;
}

@Type.d("Response_SceneDescription")
class Response_SceneDescription extends Message<Response_SceneDescription> {
	@Field.d(3, "string") content: string;
}

@Type.d("Response")
export class Response extends Message<Response> {
	static Header = Response_Header;
	static WindowHandle = Response_WindowHandle;
	static SceneDescription = Response_SceneDescription;
}
