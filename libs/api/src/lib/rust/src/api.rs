#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Request {
}
/// Nested message and enum types in `Request`.
pub mod request {
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Header {
        #[prost(enumeration="super::MessageType", tag="1")]
        pub msg_type: i32,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct WindowHandle {
        #[prost(uint32, tag="2")]
        pub handle: u32,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetViewportRect {
        #[prost(uint32, tag="3")]
        pub width: u32,
        #[prost(uint32, tag="4")]
        pub height: u32,
        #[prost(int32, tag="5")]
        pub x: i32,
        #[prost(int32, tag="6")]
        pub y: i32,
        #[prost(float, tag="7")]
        pub scale_factor: f32,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct PatchedValue {
        #[prost(string, tag="11")]
        pub r#type: ::prost::alloc::string::String,
        #[prost(bytes="vec", tag="12")]
        pub value: ::prost::alloc::vec::Vec<u8>,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct PatchValue {
        #[prost(uint32, tag="8")]
        pub entity: u32,
        #[prost(string, tag="9")]
        pub component: ::prost::alloc::string::String,
        #[prost(map="string, message", tag="10")]
        pub values: ::std::collections::HashMap<::prost::alloc::string::String, PatchedValue>,
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Response {
}
/// Nested message and enum types in `Response`.
pub mod response {
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Header {
        #[prost(enumeration="super::MessageType", tag="1")]
        pub msg_type: i32,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct WindowHandle {
        #[prost(uint32, tag="2")]
        pub handle: u32,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SceneDescription {
        #[prost(string, tag="3")]
        pub content: ::prost::alloc::string::String,
    }
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum MessageType {
    Exit = 0,
    WindowHandle = 1,
    SetViewportRect = 2,
    SceneDescription = 3,
    PatchValue = 4,
}
