pub(crate) mod api;
pub use api::*;

impl From<i32> for MessageType {
	fn from(n: i32) -> Self {
		use MessageType::*;

		match n {
			x if x == Exit as i32 => Exit,
			x if x == SetViewportRect as i32 => SetViewportRect,
			x if x == WindowHandle as i32 => WindowHandle,
			x if x == PatchValue as i32 => PatchValue,
			_ => panic!("Into<MessageType> not implemented for value: {}", n),
		}
	}
}
