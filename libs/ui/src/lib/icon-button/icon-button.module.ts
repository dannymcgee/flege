import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IconButtonComponent } from "./icon-button.component";
import { IconModule } from "../icon/icon.module";

@NgModule({
	imports: [
		CommonModule,
		IconModule,
	],
	declarations: [
		IconButtonComponent,
	],
	exports: [
		IconButtonComponent,
	],
})
export class IconButtonModule {}
