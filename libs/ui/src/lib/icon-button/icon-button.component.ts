import {
	ChangeDetectionStrategy,
	Component,
	HostBinding,
	Input,
	ViewEncapsulation,
} from "@angular/core";
import { a11y } from "@flege/styles";
import { IconName, IconSize } from "../icon/icon.types";

@Component({
	selector: "[ui-icon-btn]",
	templateUrl: "./icon-button.component.html",
	styleUrls: ["./icon-button.component.scss"],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconButtonComponent {
	@HostBinding("class") hostClass = "ui-icon-btn";

	@Input("ui-icon-btn") icon: IconName;
	@Input() iconSize: IconSize = "md";

	@HostBinding("style.width")
	@HostBinding("style.height")
	@Input()
	get size() { return this._size; }
	set size(value: string|number) {
		if (typeof value === "number") {
			this._size = a11y.rem(value);
		} else if (!/(p[xt]|r?em|%|v[wh]|fr)$/.test(value)) {
			this._size = a11y.rem(parseInt(value, 10));
		} else {
			this._size = value;
		}
	}
	private _size = a11y.rem(32);

	@HostBinding()
	@Input() disabled = false;
}
