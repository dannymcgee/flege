import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	Directive,
	Host,
	HostBinding,
	HostListener,
	Inject,
	Optional,
	SkipSelf,
	TemplateRef,
	ViewEncapsulation,
} from "@angular/core";

import {
	CdkAccordion,
	CdkAccordionItem,
	ɵangular_material_src_cdk_accordion_accordion_a as CDK_ACCORDION,
} from "@angular/cdk/accordion";

import { UniqueSelectionDispatcher } from "@angular/cdk/collections";

import { ACCORDION_TRIGGER, BLOCK_FIRST_ENTER_ANIM } from "./accordion.animation";

@Directive({
	selector: "[uiAccordionToolbar]"
})
export class AccordionToolbarDirective {
	@HostBinding("class") hostClass = "ui-accordion-header__toolbar";

	@HostListener("click", ["$event"])
	_preventBubbling(event: Event): void {
		event.stopPropagation();
	}
}

@Component({
	selector: "ui-accordion-header, [uiAccordionHeader]",
	template: `
		<span class="ui-accordion-header__title">
			<ng-content></ng-content>
		</span>
		<ng-content select="[uiAccordionToolbar]"></ng-content>
		<div class="ui-accordion-header__icon"
			[class.ui-accordion-header__icon--expanded]="accordion.expanded"
		>
			<ui-icon icon="ChevronRightSmall"></ui-icon>
		</div>
	`,
	styleUrls: [],
})
export class AccordionHeaderComponent {
	@HostBinding("class") hostClass = "ui-accordion-header";

	constructor(
		@Host() public accordion: AccordionComponent,
	) {}

	@HostListener("click")
	_toggle(): void {
		this.accordion.toggle();
	}
}

@Directive({
	selector: "ng-template[uiAccordionContent]",
})
export class AccordionContentDirective {
	@HostBinding("class") hostClass = "ui-accordion-content";

	constructor(
		public _template: TemplateRef<any>,
	) {}
}

@Component({
	selector: "ui-accordion, [ui-accordion]",
	template: `
		<ng-content
			select="ui-accordion-header, [ui-accordion-header]"
		></ng-content>
		<section class="ui-accordion-body"
			*ngIf="expanded"
			@accordion
		>
			<ng-content></ng-content>
		</section>
	`,
	styleUrls: ["./accordion.component.scss"],
	animations: [ACCORDION_TRIGGER, BLOCK_FIRST_ENTER_ANIM],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccordionComponent extends CdkAccordionItem {
	@HostBinding("class") hostClass = "ui-accordion";

	@HostBinding("@blockFirstEnterAnim")
	blockFirstEnterAnim = true;

	constructor(
		@Optional() @Inject(CDK_ACCORDION) @SkipSelf()
		public accordionGroup: CdkAccordion,
		changeDetector: ChangeDetectorRef,
		dispatcher: UniqueSelectionDispatcher,
	) {
		super(accordionGroup, changeDetector, dispatcher);
	}
}
