import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IconModule } from "../icon";

import { AccordionGroupComponent } from "./accordion-group/accordion-group.component";
import {
	AccordionContentDirective,
	AccordionHeaderComponent,
	AccordionComponent,
	AccordionToolbarDirective,
} from "./accordion/accordion.component";


@NgModule({
	imports: [
		CommonModule,
		IconModule,
	],
	declarations: [
		AccordionComponent,
		AccordionContentDirective,
		AccordionGroupComponent,
		AccordionHeaderComponent,
		AccordionToolbarDirective,
	],
	exports: [
		AccordionComponent,
		AccordionContentDirective,
		AccordionGroupComponent,
		AccordionHeaderComponent,
		AccordionToolbarDirective,
	]
})
export class AccordionModule { }
