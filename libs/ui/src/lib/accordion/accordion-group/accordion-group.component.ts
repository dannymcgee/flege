import {
	Component,
	ViewEncapsulation,
	ChangeDetectionStrategy,
	HostBinding,
} from "@angular/core";

import {
	CdkAccordion,
	ɵangular_material_src_cdk_accordion_accordion_a as CDK_ACCORDION,
} from "@angular/cdk/accordion";

@Component({
	selector: "ui-accordion-group,[ui-accordion-group]",
	template: `<ng-content></ng-content>`,
	styleUrls: ["./accordion-group.component.scss"],
	providers: [{
		provide: CDK_ACCORDION,
		useExisting: AccordionGroupComponent,
	}],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccordionGroupComponent extends CdkAccordion {
	@HostBinding("class") hostClass = "ui-accordion-group";
}
