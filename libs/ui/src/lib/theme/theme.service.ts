import { DOCUMENT } from "@angular/common";
import { Inject, Injectable } from "@angular/core";

import { BehaviorSubject, Observable } from "rxjs";
import { shareReplay } from "rxjs/operators";

import { floatToHex, camelToKebabCase, RGB, Hex, entries } from "@flege/utils";
import { ReadonlyThemeColorStore, ThemeColorStore } from "./theme.store";

import {
	ColorSchemeName,
	COLOR_SCHEME_NAME,
	ThemeColor,
	ThemeColorShade,
	ThemeDefinition,
	THEME_DEFINITION,
} from "./theme.types";

@Injectable({
	providedIn: "root",
})
export class ThemeService {
	get colorScheme(): ColorSchemeName {
		return this._colorScheme$.value;
	}
	get colorScheme$(): Observable<ColorSchemeName> {
		return this._colorScheme$.pipe(shareReplay({ refCount: false }));
	}
	get currentColors(): ReadonlyThemeColorStore {
		return this._store as ReadonlyThemeColorStore;
	}

	private _store = new ThemeColorStore();
	private _colorScheme$: BehaviorSubject<ColorSchemeName>;
	private _styles: CSSStyleDeclaration;

	constructor(
		@Inject(DOCUMENT) private _document: Document,
		@Inject(THEME_DEFINITION) private _theme: ThemeDefinition,
		@Inject(COLOR_SCHEME_NAME) initialScheme: ColorSchemeName
	) {
		this._styles = this._document.documentElement.style;
		this._colorScheme$ = new BehaviorSubject(initialScheme);
		this.setColorScheme(initialScheme);
	}

	setColorScheme(scheme: ColorSchemeName): void {
		let colorScheme = this._theme[scheme];
		if (colorScheme === undefined) {
			console.warn(`No color scheme defined for key "${scheme}"!`);

			return;
		}
		let { colors, vars } = colorScheme;

		for (let [name, shades] of entries(colors)) {
			if (!shades) continue;

			for (let [shade, value] of entries(shades)) {
				if (!value) continue;

				let color = this._store.set(name, shade, value);

				this._styles.setProperty(`--${name}-${shade}`, color.rgb.join(", "));
			}
		}

		this._colorScheme$.next(scheme);

		if (!vars) return;

		for (let [key, value] of entries(vars)) {
			let name = camelToKebabCase(key as string);
			this._styles.setProperty(`--${name}`, value.toString());
		}
	}

	getHex(name: ThemeColor, shade: ThemeColorShade, alpha?: number): Hex|null {
		let value = this._store.get(name, shade)?.hex ?? null;
		if (value && alpha != null) {
			value += floatToHex(alpha);
		}

		return value;
	}

	getRgb(name: ThemeColor, shade: ThemeColorShade): RGB|null {
		return this._store.get(name, shade)?.rgb ?? null;
	}
}
