import { TestBed } from "@angular/core/testing";

import {
	COLOR_SCHEME_NAME,
	ThemeDefinition,
	THEME_DEFINITION,
} from "../theme.types";
import { ThemeModule } from "../theme.module";
import { ThemeService } from "../theme.service";

const TEST_THEME: ThemeDefinition = {
	dark: {
		colors: {},
		vars: {
			testvar: `"Hello world!"`,
		},
	},
};

describe("ThemeModule", () => {
	describe("withTheme", () => {
		beforeEach(() => {
			TestBed.configureTestingModule({
				imports: [ThemeModule.withTheme(TEST_THEME, "dark")],
			});
		});

		it("should provide a ThemeService instance", () => {
			expect(() => TestBed.inject(ThemeService)).toBeTruthy();
		});

		it("should provide the given ThemeDefinition", () => {
			let theme = TestBed.inject(THEME_DEFINITION);
			expect(theme).toEqual(TEST_THEME);
		});

		it("should provide the given initial color scheme", () => {
			let scheme = TestBed.inject(COLOR_SCHEME_NAME);
			expect(scheme).toMatch("dark");
		});
	});
});
