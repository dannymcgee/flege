import { Color, ThemeColorStore } from "../theme.store";

describe("Color", () => {
	it("should store a hex representation of its value", () => {
		let color = new Color("#fff");

		expect(color.hex).toMatch(/^#ffffff/i);
	});

	it("should store an RGB representation of its value", () => {
		let color = new Color("#fff");

		expect(color.rgb).toEqual([255, 255, 255]);
	});

	it("should throw an error if initialized with an invalid string", () => {
		expect(() => new Color("Hello world!")).toThrow();
	});
});

describe("ThemeColorStore", () => {

	let store: ThemeColorStore;
	beforeEach(() => {
		store = new ThemeColorStore();
	});

	describe("set", () => {
		it("should create a new Color class from hex input and return it", () => {
			let color = store.set("primary", 500, "#FFF");

			expect(color).toBeInstanceOf(Color);
		});

		it("should store the color for later retrieval", () => {
			store.set("primary", 500, "#FFF");

			let data = store["_data"];
			expect(data.get("primary")).toBeTruthy();

			let color = data.get("primary")?.get(500);
			expect(color).toBeTruthy();
			expect(color).toBeInstanceOf(Color);
			expect(color!.rgb).toEqual([255, 255, 255]);
		});

		it("should not create duplicate name entries", () => {
			store.set("primary", 100, "#FFF");
			store.set("primary", 200, "#FFF");
			store.set("primary", 300, "#fff");
			let data = store["_data"];

			expect(data.size).toEqual(1);
			expect(data.get("primary")?.size).toEqual(3);
		});

		it("should not create duplicate shade entries", () => {
			store.set("primary", 100, "#fff");
			store.set("primary", 100, "#000");
			let data = store["_data"];

			expect(data.size).toEqual(1);
			expect(data.get("primary")?.size).toEqual(1);
		});
	});

	describe("get", () => {
		it("should return the stored Color class", () => {
			store.set("primary", 100, "#fff");
			let color = store.get("primary", 100);

			expect(color).toBeTruthy();
			expect(color).toBeInstanceOf(Color);
			expect(color!.rgb).toEqual([255, 255, 255]);
		});

		it("should return `null` when the requested color hasn't been set", () => {
			let color = store.get("accent", 400);

			expect(color).toBe(null);
		});
	});

});
