import { ModuleWithProviders, NgModule } from "@angular/core";

import {
	ColorSchemeName,
	COLOR_SCHEME_NAME,
	ThemeDefinition,
	THEME_DEFINITION,
} from "./theme.types";
import { ThemeService } from "./theme.service";

@NgModule({})
export class ThemeModule {
	static withTheme(
		theme: ThemeDefinition,
		initialScheme: ColorSchemeName
	): ModuleWithProviders<ThemeModule> {
		return {
			ngModule: ThemeModule,
			providers: [
				{
					provide: THEME_DEFINITION,
					useValue: theme,
				},
				{
					provide: COLOR_SCHEME_NAME,
					useValue: initialScheme,
				},
				ThemeService,
			],
		};
	}

}
