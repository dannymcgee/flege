import { ElementRef, InjectionToken } from "@angular/core";
import { MenuOverlayManager } from "./menu-overlay.service";
import { MenuComponent } from "./menu.component";

export enum MenuType {
	Menu,
	ContextMenu,
	Submenu,
	Menubar,
}

export interface MenuTrigger {
	elementRef: ElementRef<HTMLElement>;
	menu: MenuComponent;
	overlay: MenuOverlayManager;
	initialize?(): void;
}

export const MENU_TRIGGER = new InjectionToken<MenuTrigger>("MENU_TRIGGER");
export const MENUBAR = new InjectionToken("MENUBAR");
