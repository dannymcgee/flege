import {
	AfterViewInit,
	Directive,
	ElementRef,
	HostBinding,
	HostListener,
	Input,
	OnDestroy,
	Self,
} from "@angular/core";

import { Orientation } from "../shared/types";
import { MenuComponent } from "./menu.component";
import { MenuOverlayManager } from "./menu-overlay.service";
import { MenuTrigger, MenuType, MENU_TRIGGER } from "./menu.types";
import { MenuCoordinator } from "./menu-coordinator.service";

@Directive({
	selector: "[uiMenuTriggerFor]",
	providers: [{
		provide: MENU_TRIGGER,
		useExisting: MenuTriggerDirective,
	},
		MenuOverlayManager,
	],
	exportAs: "uiMenuTrigger",
})
export class MenuTriggerDirective implements MenuTrigger, OnDestroy {

	@Input("uiMenuTriggerFor")
	menu: MenuComponent;

	@Input()
	get orientation(): Orientation { return this.overlay.orientation; }
	set orientation(value: Orientation) { this.overlay.orientation = value; }

	@HostBinding("class")
	readonly hostClass = "ui-dropdown-trigger";

	@HostBinding("attr.aria-haspopup")
	readonly ariaHaspopup = "menu";

	constructor(
		public elementRef: ElementRef<HTMLElement>,
		@Self() public overlay: MenuOverlayManager,
		private _coordinator: MenuCoordinator,
	) {}

	ngOnDestroy(): void {
		this._coordinator.unregister(this);
	}

	@HostListener("mouseenter")
	@HostListener("focus")
	initialize(): void {
		this._coordinator.register(MenuType.Menu, this, this.overlay);
		this.overlay.initialize({
			origin: this.elementRef,
			menuPanelClass: this.menu.panelClass,
			encapsulationId: this.menu.encapsulationId,
		});
	}
}

@Directive({
	selector: "[uiContextMenuTriggerFor]",
	providers: [{
		provide: MENU_TRIGGER,
		useExisting: ContextMenuTriggerDirective,
	},
		MenuOverlayManager,
	],
})
export class ContextMenuTriggerDirective implements MenuTrigger, AfterViewInit {

	@Input("uiContextMenuTriggerFor")
	menu: MenuComponent;

	constructor(
		public elementRef: ElementRef<HTMLElement>,
		@Self() public overlay: MenuOverlayManager,
		private _coordinator: MenuCoordinator,
	) { }

	ngAfterViewInit(): void {
		this._coordinator.register(MenuType.ContextMenu, this, this.overlay);
	}
}

@Directive({
	selector: "[uiSubmenuTriggerFor]",
	providers: [{
		provide: MENU_TRIGGER,
		useExisting: SubmenuTriggerDirective,
	},
		MenuOverlayManager,
	],
})
export class SubmenuTriggerDirective implements MenuTrigger {

	@Input("uiSubmenuTriggerFor")
	menu: MenuComponent;

	@HostBinding("attr.aria-haspopup")
	readonly ariaHaspopup = "menu";

	constructor(
		public elementRef: ElementRef<HTMLElement>,
		@Self() public overlay: MenuOverlayManager,
		private _coordinator: MenuCoordinator,
		private _parentMenu: MenuComponent,
	) { }

	@HostListener("mouseenter")
	@HostListener("focus")
	initialize(): void {
		this._coordinator.register(MenuType.Submenu, this, this.overlay, this._parentMenu);
		this.overlay.initialize({
			origin: this.elementRef,
			menuPanelClass: this.menu.panelClass,
			encapsulationId: this.menu.encapsulationId,
			orientation: "horizontal",
		});
	}
}
