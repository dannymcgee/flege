import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OverlayModule } from "@angular/cdk/overlay";

import { IconModule } from "../icon";
import { MenuPanelComponent } from "./menu-panel/menu-panel.component";
import { MenuComponent } from "./menu.component";
import {
	ContextMenuTriggerDirective,
	MenuTriggerDirective,
	SubmenuTriggerDirective,
} from "./menu-trigger.directive";
import { MenuItemComponent } from "./menu-item/menu-item.component";
import { MenubarComponent } from "./menubar/menubar.component";

@NgModule({
	imports: [
		CommonModule,
		IconModule,
		OverlayModule,
	],
	declarations: [
		MenuComponent,
		MenuItemComponent,
		MenuPanelComponent,
		MenubarComponent,
		MenuTriggerDirective,
		ContextMenuTriggerDirective,
		SubmenuTriggerDirective,
	],
	exports: [
		MenuComponent,
		MenuItemComponent,
		MenuPanelComponent,
		MenubarComponent,
		MenuTriggerDirective,
		ContextMenuTriggerDirective,
		SubmenuTriggerDirective,
	]
})
export class MenuModule { }
