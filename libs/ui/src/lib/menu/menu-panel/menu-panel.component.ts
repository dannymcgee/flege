import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	HostBinding,
	TemplateRef,
	ViewEncapsulation,
} from "@angular/core";

@Component({
	templateUrl: "./menu-panel.component.html",
	styleUrls: ["./menu-panel.component.scss"],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuPanelComponent {

	@HostBinding("class")
	readonly hostClass = "ui-menu-panel";

	@HostBinding("attr.role")
	readonly role = "menu";

	get template(): TemplateRef<any> | undefined { return this._template; }
	set template(value: TemplateRef<any> | undefined) {
		this._template = value;
		this._changeDetectorRef.markForCheck();
	}
	private _template?: TemplateRef<any>;

	constructor(
		private _changeDetectorRef: ChangeDetectorRef,
		public elementRef: ElementRef<HTMLElement>,
	) { }

}
