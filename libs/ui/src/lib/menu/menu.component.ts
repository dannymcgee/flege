import {
	Component,
	ChangeDetectionStrategy,
	ElementRef,
	Input,
	ViewChild,
	TemplateRef,
	ContentChildren,
} from "@angular/core";

import { findEncapsulationId, QueryList$ } from "@flege/utils";

import { MenuItemComponent } from "./menu-item/menu-item.component";

@Component({
	selector: "ui-menu",
	templateUrl: "./menu.component.html",
	styleUrls: ["./menu.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush,
	exportAs: "uiMenu",
})
export class MenuComponent {

	@Input("class") panelClass?: string;

	@ViewChild(TemplateRef)
	readonly template: TemplateRef<any>;

	get encapsulationId(): string | null {
		return findEncapsulationId(this._elementRef);
	}

	@ContentChildren(MenuItemComponent)
	items: QueryList$<MenuItemComponent>;

	constructor(
		private _elementRef: ElementRef<HTMLElement>,
	) { }

}
