import {
	AfterContentInit,
	ChangeDetectionStrategy,
	Component,
	ContentChildren,
	ElementRef,
	HostBinding,
	HostListener,
	Inject,
	OnDestroy,
	ViewEncapsulation,
} from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { FocusKeyManager, FocusMonitor, FocusOrigin } from "@angular/cdk/a11y";

import { fromKeydown, QueryList$ } from "@flege/utils";
import { combineLatest, fromEvent, merge, Observable, Subject } from "rxjs";
import {
	debounceTime,
	distinctUntilChanged,
	filter,
	map,
	shareReplay,
	startWith,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from "rxjs/operators";

import { MENUBAR, MenuTrigger, MENU_TRIGGER } from "../menu.types";
import { MenuItemComponent } from "../menu-item/menu-item.component";


@Component({
	selector: "ui-menubar",
	templateUrl: "./menubar.component.html",
	styleUrls: ["./menubar.component.scss"],
	providers: [{
		provide: MENUBAR,
		useExisting: MenubarComponent,
	}],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenubarComponent implements AfterContentInit, OnDestroy {

	@HostBinding("class")
	readonly hostClass = "ui-menubar";

	@HostBinding("attr.role")
	readonly role = "menubar";

	@HostBinding("tabIndex")
	tabIndex = 0;

	@ContentChildren(MenuItemComponent, { descendants: false })
	private _menuItems: QueryList$<MenuItemComponent>;
	private _menuItems$: Observable<QueryList$<MenuItemComponent>>;

	@ContentChildren(MENU_TRIGGER, { descendants: false })
	private _menuTriggers: QueryList$<MenuTrigger>;
	private _menuTriggers$: Observable<QueryList$<MenuTrigger>>;

	private _openMenuIndex$: Observable<number>;
	private _keyPresses$: Observable<KeyboardEvent>;
	private _keyManager: FocusKeyManager<MenuItemComponent>;

	private _onDestroy$ = new Subject<void>();

	constructor(
		@Inject(DOCUMENT) private _document: Document,
		private _elementRef: ElementRef<HTMLElement>,
		private _focusMonitor: FocusMonitor,
	) {}

	ngAfterContentInit(): void {
		this._menuItems$ = this._menuItems.changes.pipe(startWith(this._menuItems));
		this._menuTriggers$ = this._menuTriggers.changes.pipe(startWith(this._menuTriggers));
		this._keyPresses$ = fromKeydown(this._document, /^Arrow(Left|Right)$/);

		this._keyManager = new FocusKeyManager(this._menuItems)
			.withHorizontalOrientation("ltr")
			.withWrap();

		let focusOrigin$ = this._focusMonitor.monitor(this._elementRef, true);
		this._keyPresses$.pipe(
			withLatestFrom(focusOrigin$),
			filter(([, origin]) => origin !== null),
			map(([event]) => event),
			takeUntil(this._onDestroy$),
		).subscribe((event) => {
			this._keyManager.setFocusOrigin("keyboard");
			this._keyManager.onKeydown(event);
		});

		this._openMenuIndex$ = this._menuTriggers$.pipe(
			switchMap((triggers) => {
				let events$ = triggers.map((trigger) => trigger.overlay.events$.pipe(
					startWith({ isOpen: false }),
					map((event) => event.isOpen),
				));

				return combineLatest([...events$]);
			}),
			map((bools) => bools.findIndex((isOpen) => isOpen)),
			distinctUntilChanged(),
			tap((index) => this._menuItems.forEach((item, i) => {
				let { classList } = item.elementRef.nativeElement;

				if (i === index) classList.add("active");
				else if (classList.contains("active")) classList.remove("active");
			})),
			debounceTime(70),
			shareReplay(),
			takeUntil(this._onDestroy$),
		);

		this._menuItems$.pipe(
			switchMap((items) => merge(...items.map(({ elementRef }, index) => {
				return fromEvent(elementRef.nativeElement, "mouseenter").pipe(map(() => index));
			}))),
			withLatestFrom(this._openMenuIndex$),
			filter(([hoveredIndex, openIndex]) => openIndex !== -1 && hoveredIndex !== openIndex),
			map(([index]) => index),
			takeUntil(this._onDestroy$),
		).subscribe((index) => {
			this.openMenu(index, "mouse");
		});

		this._keyPresses$.pipe(
			map((event) => /Right$/.test(event.key) ? 1 : -1),
			withLatestFrom(this._openMenuIndex$),
			filter(([, openIndex]) => openIndex !== -1),
		).subscribe(([delta, openIndex]) => {
			let length = this._menuTriggers.length;
			let index = openIndex + delta;

			if (index >= length) index = 0;
			else if (index < 0) index = length - 1;

			this.openMenu(index, "keyboard");
		});
	}

	ngOnDestroy(): void {
		this._onDestroy$.next();
		this._onDestroy$.complete();
		this._focusMonitor.stopMonitoring(this._elementRef);
	}

	@HostListener("focus")
	onFocus(): void {
		this._keyManager.setFocusOrigin("keyboard");
		this._keyManager.setFirstItemActive();
	}

	private openMenu(menuIndex: number, focusOrigin: FocusOrigin): void {
		let trigger = this._menuTriggers.toArray()[menuIndex];
		trigger.initialize?.();
		setTimeout(() => {
			trigger.overlay.open(trigger.menu.template, focusOrigin);
		});
	}
}
