import { FocusKeyManager } from "@angular/cdk/a11y";
import { Injectable, TemplateRef } from "@angular/core";

import {
	fromEvent,
	merge,
	noop,
	Observable,
	Subject,
	Subscription,
	timer,
} from "rxjs";

import {
	delay,
	filter,
	map,
	shareReplay,
	startWith,
	switchMap,
	take,
	takeUntil,
	withLatestFrom,
} from "rxjs/operators";

import {
	ElementId,
	elementId,
	entries,
	fromKeydown,
	isNotNull,
	QueryList$,
} from "@flege/utils";

import { MenuItemComponent } from "./menu-item/menu-item.component";
import { MenuCloseEvent, MenuOpenEvent, MenuOverlayManager } from "./menu-overlay.service";
import { MenuComponent } from "./menu.component";
import { MenuTrigger, MenuType } from "./menu.types";

/**
 * This service serves as a registry for all menus in the app, manages parent /
 * child relationships between menus and submenus, and ensures that only one
 * menu is ever open at a time.
 */
@Injectable({
	providedIn: "root",
})
export class MenuCoordinator {
	private _menus = new Map<ElementId, MenuBase>();
	private _topLevelMenus = new Map<ElementId, MenuBase>();

	private _menuTriggerIdLookup = new Map<MenuTrigger, ElementId>();
	private _menuComponentIdLookup = new Map<MenuComponent, ElementId>();

	private _subscription?: Subscription;

	register(
		type: MenuType,
		trigger: MenuTrigger,
		overlay: MenuOverlayManager,
		parent?: MenuComponent,
	): void {
		if (this._menuTriggerIdLookup.has(trigger)) return;

		let id = elementId();
		this._menuTriggerIdLookup.set(trigger, id);
		this._menuComponentIdLookup.set(trigger.menu, id);

		let menu: MenuBase;
		switch (type) {
			case MenuType.Menu:
				menu = new Menu(id, trigger, overlay);
				break;
			case MenuType.ContextMenu:
				menu = new ContextMenu(id, trigger, overlay);
				break;
			case MenuType.Submenu:
				menu = new Submenu(id, trigger, overlay);
				break;
			default:
				console.warn(`Case for ${type} not implemented yet!`);
				return;
			// throw new Error(`Case not handled for MenuType: ${type}`);
		}
		this._menus.set(id, menu);

		if (parent) {
			let parentId: ElementId | undefined;
			if ((parentId = this._menuComponentIdLookup.get(parent))) {
				this._menus.get(parentId)?.registerChild(menu);

				let parentMenu: MenuBase | undefined;
				if ((parentMenu = this._menus.get(parentId))) {
					menu.registerParent(parentMenu);
				}
			}
		} else {
			this._topLevelMenus.set(id, menu);
			this.updateSubscription();
		}
	}

	unregister(trigger: MenuTrigger): void {
		let id: ElementId | undefined;
		if ((id = this._menuTriggerIdLookup.get(trigger))) {
			this._menus.get(id)?.destroy();
			this._menus.delete(id);
			this._topLevelMenus.delete(id);
			this.updateSubscription();
		}
	}

	private updateSubscription(): void {
		this._subscription?.unsubscribe();

		let menus = entries(this._topLevelMenus);
		let topLevelMenuOpened$ = merge(...menus.map(([, menu]) => menu.opened$));
		this._subscription = topLevelMenuOpened$.subscribe(openedId => {
			menus.forEach(([id, menu]) => {
				if (id !== openedId) menu.close();
			});
		});
	}
}

/**
 * This class and its derivatives serve as the interface between the various
 * UI classes that make up a single menu (the trigger directive, the menu
 * component, the menu items, and the directive-scoped overlay manager service)
 * and the root-level menu coordinator service. Its primary responsibility is
 * bundling all of those UI classes into a single object, and defining and
 * handling the menu's open/close events.
 *
 * This particular class is the pseudo-abstract base class from which all of the
 * concrete implementations derive; it defines the generic behavior which is
 * shared between all variants.
 */
class MenuBase {
	readonly id: ElementId;

	protected overlay: MenuOverlayManager;
	protected trigger: MenuTrigger;
	protected get triggerElement(): HTMLElement {
		return this.trigger.elementRef.nativeElement;
	}
	protected get template(): TemplateRef<any> {
		return this.trigger.menu.template;
	}

	protected items: QueryList$<MenuItemComponent>;
	protected items$: Observable<QueryList$<MenuItemComponent>>;
	protected keydown$: Observable<KeyboardEvent>;
	protected keyManager: FocusKeyManager<MenuItemComponent>;

	protected parent?: MenuBase;
	protected children: MenuBase[] = [];

	opened$: Observable<ElementId>;
	protected isOpen$: Observable<boolean>;
	protected _opened$: Observable<MenuOpenEvent>;
	protected _closed$: Observable<MenuCloseEvent>;

	// Virtual
	protected get openEvents$(): Observable<Event> {
		return undefined as any;
	}
	protected get closeEvents$(): Observable<void> {
		return this._opened$.pipe(
			delay(100),
			withLatestFrom(this.items$),
			switchMap(([{ menuPanel }, items]) => {
				return merge(
					fromKeydown(menuPanel.elementRef, /^(Tab|Escape)$/),
					fromEvent(document, "click").pipe(
						filter(event => !event
							.composedPath()
							.includes(menuPanel.elementRef.nativeElement)
						),
					),
					merge(...items.map(item => item.pressed$)),
					this._closed$,
				).pipe(
					take(1),
					takeUntil(this.onDestroy$),
				);
			}),
			map(noop),
			takeUntil(this.onDestroy$),
		);
	}

	protected onDestroy$ = new Subject<void>();

	constructor(
		id: ElementId,
		trigger: MenuTrigger,
		overlay: MenuOverlayManager,
	) {
		this.id = id;
		this.trigger = trigger;
		this.overlay = overlay;
		this.items = trigger.menu.items;

		this.keyManager = new FocusKeyManager(this.items)
			.withVerticalOrientation()
			.withWrap();

		this.isOpen$ = this.overlay.events$.pipe(
			startWith({ isOpen: false }),
			map(event => event.isOpen),
			shareReplay({ refCount: true }),
			takeUntil(this.onDestroy$),
		);
		this._opened$ = overlay.events$.pipe(
			filter<MenuOpenEvent>(event => event.isOpen),
			takeUntil(this.onDestroy$),
		);
		this._closed$ = overlay.events$.pipe(
			filter<MenuCloseEvent>(event => !event.isOpen),
			takeUntil(this.onDestroy$),
		);
		this.opened$ = this._opened$.pipe(
			map(() => this.id),
			takeUntil(this.onDestroy$),
		);
		this.items$ = this.items.changes.pipe(
			startWith(this.items),
			takeUntil(this.onDestroy$),
		);
		this.keydown$ = this._opened$.pipe(
			switchMap(({ menuPanel }) => fromKeydown(menuPanel.elementRef)),
			takeUntil(this.onDestroy$),
		);

		this.onInit();
	}

	registerParent(menu: MenuBase): void {
		this.parent = menu;

		this._opened$.pipe(
			withLatestFrom(this.parent.items$),
			switchMap(([, items]) => merge(...items.map((item) => item.hovered$))),
			filter(element => element !== this.triggerElement),
			takeUntil(this.onDestroy$),
		).subscribe(() => this.close());
	}

	registerChild(menu: MenuBase): void {
		this.children.push(menu);
	}

	close(): void {
		this.children.forEach(child => child.close());
		this.overlay.close();
	}

	destroy(): void {
		this.onDestroy$.next();
		this.onDestroy$.complete();
	}

	protected onInit(): void {
		if (this.closeEvents$) this.closeEvents$.subscribe(() => this.close());

		this.isOpen$.subscribe(isOpen => {
			if (isOpen) {
				this.triggerElement.setAttribute("aria-expanded", "true");
			} else {
				this.triggerElement.removeAttribute("aria-expanded");
			}
		});

		this._opened$.subscribe(({ originalEvent, focusOrigin }) => {
			this.keyManager.setFocusOrigin(focusOrigin);
			if (
				originalEvent instanceof KeyboardEvent
				&& originalEvent.key === "ArrowUp"
			) {
				this.keyManager.setLastItemActive();
			} else {
				this.keyManager.setFirstItemActive();
			}
		});

		this.keydown$
			.pipe(filter(event => /^Arrow(Down|Up)$/.test(event.key)))
			.subscribe(event => {
				this.keyManager.setFocusOrigin("keyboard");
				this.keyManager.onKeydown(event);
			});
	}
}

/**
 * This class represents an ordinary menu which is opened by pressing a trigger
 * button.
 */
class Menu extends MenuBase {
	protected get openEvents$(): Observable<Event> {
		let click$ = fromEvent(this.triggerElement, "click");
		let arrowupdown$ = fromKeydown(this.triggerElement, /^Arrow(Up|Down)$/);

		return merge(click$, arrowupdown$)
			.pipe(takeUntil(this.onDestroy$));
	}

	protected onInit(): void {
		this.openEvents$.subscribe(event => this.open(event));
		super.onInit();

		this.isOpen$.subscribe(isOpen => {
			if (isOpen) {
				this.triggerElement.setAttribute("aria-expanded", "true");
			} else {
				this.triggerElement.removeAttribute("aria-expanded");
			}
		});
	}

	open(event: Event): void {
		this.overlay.open(this.template, event);
	}
}

/**
 * This class represents a context menu, opened by right-clicking (or pressing
 * the "menu" key) within a defined area.
*/
class ContextMenu extends MenuBase {
	protected get openEvents$(): Observable<Event> {
		return fromEvent(this.triggerElement, "contextmenu")
			.pipe(takeUntil(this.onDestroy$));
	}

	protected onInit(): void {
		super.onInit();
		this.openEvents$.subscribe(event => this.open(event as MouseEvent));
	}

	open(event: MouseEvent): void {
		event.preventDefault();
		event.stopPropagation();

		let { x, y } = event;
		this.overlay.initialize({
			origin: { x, y },
			menuPanelClass: this.trigger.menu.panelClass,
			encapsulationId: this.trigger.menu.encapsulationId,
		});

		setTimeout(() => this.overlay.open(this.template, event));
	}
}

/**
 * This class represents a submenu, which is opened by hovering or pressing a
 * menu item within its parent menu.
 */
class Submenu extends MenuBase {
	protected get openEvents$(): Observable<Event> {
		let hovered$ = fromEvent(this.triggerElement, "mouseenter").pipe(
			startWith(((): MouseEvent|void => {
				// Since the submenu trigger doesn't register itself until the first
				// time it's moused over or focused, we'll likely already be
				// hovering by the time this class is constructed.

				// To catch that first hover, we check whether it's already being
				// hovered in a `startWith` IIFE and immediately emit a synthetic
				// 'mouseenter' event.

				// The X and Y coordinates need to be randomized to avoid the
				// Overlay Manager calling our bullshit and setting the focus origin
				// to 'keyboard'.
				if (this.triggerElement.webkitMatchesSelector(":hover")) {
					let event = new MouseEvent("mouseenter", {
						clientX: Math.round(Math.random() * 512),
						clientY: Math.round(Math.random() * 512),
					});

					return event;
				}
			})()),
			filter(isNotNull),
			switchMap(event => timer(250).pipe(
				map(() => event),
				take(1),
				takeUntil(fromEvent(this.triggerElement, "mouseleave")),
			)),
		);
		let keypresses$ = fromKeydown(
			this.triggerElement,
			/^(Enter| |ArrowRight)$/
		);

		return merge(hovered$, keypresses$)
			.pipe(takeUntil(this.onDestroy$));
	}

	protected onInit(): void {
		super.onInit();

		this._opened$.pipe(
			switchMap(({ menuPanel }) => fromKeydown(
				menuPanel.elementRef,
				"ArrowLeft"
			).pipe(
				take(1),
				takeUntil(this.onDestroy$),
			)),
			takeUntil(this.onDestroy$),
		).subscribe(() => {
			this.close();
		});
		this.openEvents$.subscribe(event => this.open(event));
	}

	open(event: Event): void {
		this.overlay.open(this.template, event);
	}
}
