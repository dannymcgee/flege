import { ConnectedPosition } from "@angular/cdk/overlay";

// Vertical orientation
const VERTICAL_DEFAULT: ConnectedPosition = {
	originX: "start",
	originY: "bottom",
	overlayX: "start",
	overlayY: "top",
	offsetY: 4,
	panelClass: "from-top",
};
const VERTICAL_FALLBACK_VERTICAL: Partial<ConnectedPosition> = {
	originY: "top",
	overlayY: "bottom",
	offsetY: -4,
	panelClass: "from-bottom",
};
const VERTICAL_FALLBACK_HORIZONTAL: Partial<ConnectedPosition> = {
	originX: "end",
	overlayX: "end",
};
export const VERTICAL_POSITIONS = [{
	...VERTICAL_DEFAULT,
}, {
	...VERTICAL_DEFAULT,
	...VERTICAL_FALLBACK_HORIZONTAL,
}, {
	...VERTICAL_DEFAULT,
	...VERTICAL_FALLBACK_VERTICAL,
}, {
	...VERTICAL_FALLBACK_VERTICAL,
	...VERTICAL_FALLBACK_HORIZONTAL,
} as ConnectedPosition];

// Horizontal orientation
const HORIZONTAL_DEFAULT: ConnectedPosition = {
	originX: "end",
	originY: "top",
	overlayX: "start",
	overlayY: "top",
	offsetX: 4,
	offsetY: -8,
	panelClass: "from-left",
};
const HORIZONTAL_FALLBACK_VERTICAL: Partial<ConnectedPosition> = {
	originY: "bottom",
	overlayY: "bottom",
	offsetY: 8,
};
const HORIZONTAL_FALLBACK_HORIZONTAL: Partial<ConnectedPosition> = {
	originX: "start",
	overlayX: "end",
	offsetX: -4,
	panelClass: "from-right",
};
export const HORIZONTAL_POSITIONS = [{
	...HORIZONTAL_DEFAULT,
}, {
	...HORIZONTAL_DEFAULT,
	...HORIZONTAL_FALLBACK_VERTICAL,
}, {
	...HORIZONTAL_DEFAULT,
	...HORIZONTAL_FALLBACK_HORIZONTAL,
}, {
	...HORIZONTAL_FALLBACK_VERTICAL,
	...HORIZONTAL_FALLBACK_HORIZONTAL,
} as ConnectedPosition];
