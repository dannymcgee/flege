import { ElementRef, Inject, Injectable, OnDestroy, TemplateRef } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { FocusMonitor, FocusOrigin } from "@angular/cdk/a11y";
import { coerceElement } from "@angular/cdk/coercion";
import { ConnectedPosition, FlexibleConnectedPositionStrategyOrigin, Overlay, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";

import { Observable, Subject } from "rxjs";
import { delay, filter, share, takeUntil } from "rxjs/operators";

import { Orientation } from "../shared/types";
import { MenuPanelComponent } from "./menu-panel/menu-panel.component";
import { HORIZONTAL_POSITIONS, VERTICAL_POSITIONS } from "./menu.positions";

export interface MenuOverlayConfig {
	origin: FlexibleConnectedPositionStrategyOrigin;
	menuPanelClass?: string;
	encapsulationId: string | null;
	orientation?: Orientation;
}

export interface MenuCloseEvent {
	isOpen: false;
}
export interface MenuOpenEvent {
	isOpen: true;
	originalEvent?: Event;
	focusOrigin: FocusOrigin;
	menuPanel: MenuPanelComponent;
}
export type MenuEvent = MenuCloseEvent | MenuOpenEvent;

@Injectable()
export class MenuOverlayManager implements OnDestroy {

	get orientation(): Orientation { return this._orientation; }
	set orientation(value: Orientation) {
		this._orientation = value;
		this._positions = value === "vertical"
			? VERTICAL_POSITIONS
			: HORIZONTAL_POSITIONS;
	}
	private _orientation: Orientation = "vertical";

	private _onDestroy$ = new Subject<void>();
	private _events$ = new Subject<MenuEvent>();
	events$: Observable<MenuEvent> = this._events$.pipe(share());
	ready$ = new Subject<void>();

	private _positions: ConnectedPosition[] = VERTICAL_POSITIONS;
	private _origin?: FlexibleConnectedPositionStrategyOrigin;
	private _originElement?: HTMLElement;
	private _focusOrigin: FocusOrigin = "program";
	private _overlayRef?: OverlayRef;
	private _portal = new ComponentPortal(MenuPanelComponent);
	private _menuPanel?: MenuPanelComponent;
	private get _menuPanelElement(): HTMLElement | undefined {
		return this._menuPanel?.elementRef.nativeElement;
	}
	private _menuPanelClass?: string;
	private _encapsulationId?: string;

	constructor(
		@Inject(DOCUMENT) private _document: Document,
		private _focusMonitor: FocusMonitor,
		private _overlay: Overlay,
	) { }

	ngOnDestroy(): void {
		this._onDestroy$.next();
		this._onDestroy$.complete();
		this._events$.complete();
		this.detach();
		this._overlayRef?.dispose();
	}

	initialize({
		origin,
		menuPanelClass,
		encapsulationId,
		orientation,
	}: MenuOverlayConfig): void {
		if (this._overlayRef) {
			if (this._origin !== origin) {
				this.updatePositionStrategy(origin);
			}
			return;
		}

		this._origin = origin;
		if (origin instanceof ElementRef || origin instanceof Element) {
			this._originElement = coerceElement(origin);
		}
		if (menuPanelClass) this._menuPanelClass = menuPanelClass;
		if (encapsulationId) this._encapsulationId = encapsulationId;
		if (orientation) this.orientation = orientation;

		let positionStrategy = this._overlay.position()
			.flexibleConnectedTo(this._origin)
			.withPositions(this._positions);
		this._overlayRef = this._overlay.create({ positionStrategy });

		this.ready$.next();
		this.ready$.complete();

		this.watchForLostUserFocus();
	}

	open(template: TemplateRef<any>): void;
	open(template: TemplateRef<any>, event: Event): void;
	open(template: TemplateRef<any>, focusOrigin: FocusOrigin): void;

	open(template: TemplateRef<any>, eventOrOrigin?: Event | FocusOrigin): void {
		if (!this._overlayRef) throw new Error("OverlayRef is undefined!");
		if (this._portal.isAttached) return;

		if (eventOrOrigin) {
			if (typeof eventOrOrigin === "string") {
				this._focusOrigin = eventOrOrigin;
			} else {
				this._focusOrigin = this.isFromKeyboard(eventOrOrigin) ? "keyboard" : "program";
				if (!this._originElement && eventOrOrigin?.target instanceof HTMLElement) {
					this._originElement = eventOrOrigin.target;
				}
			}
		}
		this._menuPanel = this._portal.attach(this._overlayRef).instance;

		if (this._menuPanelClass) {
			this._menuPanelElement!.classList.add(...this._menuPanelClass.split(" "));

			if (this._encapsulationId) {
				this._menuPanelElement!.setAttribute(`_ngcontent-${this._encapsulationId}`, "");
			}
		}
		this._menuPanel.template = template;

		setTimeout(() => {
			this._events$.next({
				isOpen: true,
				originalEvent: event,
				focusOrigin: this._focusOrigin,
				menuPanel: this._menuPanel!,
			});
		});
	}

	close(): void {
		this.detach();
		this._events$.next({ isOpen: false });
	}

	private updatePositionStrategy(origin: FlexibleConnectedPositionStrategyOrigin): void {
		this._origin = origin;

		let strategy = this._overlay.position()
			.flexibleConnectedTo(this._origin)
			.withPositions(this._positions);

		this._overlayRef?.updatePositionStrategy(strategy);
	}

	private detach(): void {
		if (this._portal.isAttached) this._portal.detach();
	}

	private watchForLostUserFocus(): void {
		this._events$.pipe(
			filter(({ isOpen }) => !isOpen),
			delay(67),
			takeUntil(this._onDestroy$),
		).subscribe(() => {
			if (
				this._originElement
				&& this._document.activeElement === this._document.body
			) {
				this._focusMonitor.focusVia(this._originElement, this._focusOrigin);
			}
		});
	}

	private isFromKeyboard(event?: Event): boolean {
		return (
			event instanceof KeyboardEvent
			|| event instanceof MouseEvent && (
				event.x === 0 && event.y === 0
				|| event?.type === "contextmenu" && event.button === 0
			)
		);
	}
}
