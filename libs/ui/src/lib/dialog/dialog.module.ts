import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IconModule } from "../icon/icon.module";

import {
	DialogComponent,
	DialogFooterDirective,
	DialogHeadingComponent,
} from "./dialog.component";

import { DialogTriggerDirective } from "./dialog.directive";

@NgModule({
	declarations: [
		DialogComponent,
		DialogFooterDirective,
		DialogHeadingComponent,
		DialogTriggerDirective,
	],
	imports: [
		CommonModule,
		IconModule,
	],
	exports: [
		DialogComponent,
		DialogFooterDirective,
		DialogHeadingComponent,
		DialogTriggerDirective,
	]
})
export class DialogModule {}
