import {
	ConfigurableFocusTrap,
	ConfigurableFocusTrapFactory,
} from "@angular/cdk/a11y";

import { coerceBooleanProperty } from "@angular/cdk/coercion";
import { DragDrop, DragRef } from "@angular/cdk/drag-drop";

import {
	AfterContentInit,
	ChangeDetectionStrategy,
	Component,
	ContentChild,
	ContentChildren,
	Directive,
	ElementRef,
	EventEmitter,
	forwardRef,
	HostBinding,
	HostListener,
	Input,
	OnDestroy,
	OnInit,
	Optional,
	Output,
	ViewEncapsulation,
} from "@angular/core";

import { GlobalFocusManager, QueryList$ } from "@flege/utils";
import { ButtonComponent } from "../button";
import { IconName } from "../icon";

@Component({
	selector: "ui-dialog",
	templateUrl: "./dialog.component.html",
	styleUrls: ["./dialog.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class DialogComponent implements OnInit, AfterContentInit, OnDestroy {
	@HostBinding("class") hostClass = "ui-dialog";

	@HostBinding()
	@Input() role: "dialog"|"alert" = "dialog";

	@HostBinding("class.loader")
	@Input()
	get loader() { return this._loader; }
	set loader(value: boolean|string|null) {
		this._loader = coerceBooleanProperty(value);
	}
	private _loader = false;

	@Input()
	get indeterminate() { return this._indeterminate; }
	set indeterminate(value: boolean|string|null) {
		this._indeterminate = coerceBooleanProperty(value);
	}
	private _indeterminate = true;

	@Input() total?: number;
	@Input() completed?: number;

	// eslint-disable-next-line @angular-eslint/no-output-native
	@Output() close = new EventEmitter<void>();

	@ContentChild(forwardRef(() => DialogHeadingComponent))
	_heading?: DialogHeadingComponent;

	@ContentChild(forwardRef(() => DialogFooterDirective))
	_footer?: DialogFooterDirective;

	private _dragRef: DragRef;
	private _focusTrap: ConfigurableFocusTrap;

	constructor(
		private _dragDrop: DragDrop,
		private _elementRef: ElementRef<HTMLElement>,
		private _focusTrapFactory: ConfigurableFocusTrapFactory,
		@Optional() private _globalFocusManager: GlobalFocusManager,
	) {}

	ngOnInit(): void {
		this._focusTrap = this._focusTrapFactory.create(
			this._elementRef.nativeElement,
			{ defer: true },
		);
	}

	ngAfterContentInit(): void {
		if (this._heading) {
			this._dragRef = this._dragDrop
				.createDrag(this._elementRef)
				.withBoundaryElement(document.body)
				.withHandles([this._heading._elementRef]);
		}

		let primaryButtons = this._footer?._buttons
			?.filter(btn => btn.variant === "primary");

		if (primaryButtons?.length) {
			this._focusTrap.attachAnchors();
			setTimeout(() => {
				primaryButtons?.[0]?.focus("keyboard");
			});
		} else {
			this._focusTrap.attachAnchors();
			this._focusTrap.focusFirstTabbableElement();
		}
	}

	ngOnDestroy(): void {
		this._focusTrap.destroy();
		this._dragRef.dispose();

		setTimeout(() => {
			if (this._globalFocusManager) {
				let lastFocus = this._globalFocusManager.getMostRecentValidFocusTarget();
				lastFocus?.focus();
			}
		});
	}

	@HostListener("window:keydown.esc")
	requestClose(): void {
		this.close.emit();
	}
}

@Component({
	selector: "ui-dialog-heading",
	template: `
		<ui-icon *ngIf="icon"
			class="ui-dialog-heading__icon"
			[icon]="icon"
		></ui-icon>

		<h3 class="ui-dialog-heading__title">
			<ng-content></ng-content>
		</h3>
	`,
	styles: [``],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogHeadingComponent {
	@HostBinding("class") hostClass ="ui-dialog-heading";
	@Input() icon?: IconName;

	constructor(
		public _elementRef: ElementRef,
	) {}
}

@Directive({
	// eslint-disable-next-line @angular-eslint/directive-selector
	selector: "ui-dialog-footer",
})
export class DialogFooterDirective {
	@HostBinding("class") hostClass = "ui-dialog-footer";

	@ContentChildren(ButtonComponent)
	_buttons: QueryList$<ButtonComponent>;
}
