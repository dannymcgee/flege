import {
	ChangeDetectionStrategy,
	Component,
	ElementRef,
	HostBinding,
	Input,
	OnDestroy,
	OnInit,
	ViewEncapsulation,
} from "@angular/core";

import { FocusMonitor, FocusOrigin } from "@angular/cdk/a11y";

import { IconName } from "../icon";
import { ButtonSize, ButtonVariant } from "./button.types";

@Component({
	selector: "[ui-btn]",
	templateUrl: "./button.component.html",
	styleUrls: ["./button.component.scss"],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent implements OnInit, OnDestroy {
	@Input("ui-btn")
	get variant(): ButtonVariant | "" { return this._variant; }
	set variant(value: ButtonVariant | "") {
		if (value) this._variant = value;
	}
	private _variant: ButtonVariant = "tertiary";

	@Input() size: ButtonSize = "md";
	@Input() icon?: IconName;

	@HostBinding("attr.role")
	@Input() role = "button";

	@HostBinding("class")
	get hostClasses(): string[] {
		let base = "ui-btn";
		let size = `${base}--${this.size}`;
		let variant = `${base}--${this.variant}`;

		return [base, size, variant];
	}

	constructor(
		private _elementRef: ElementRef<HTMLElement>,
		private _focusMonitor: FocusMonitor,
	) {}

	ngOnInit(): void {
		this._focusMonitor.monitor(this._elementRef);
	}

	ngOnDestroy(): void {
		this._focusMonitor.stopMonitoring(this._elementRef);
	}

	focus(origin?: FocusOrigin): void {
		this._focusMonitor.focusVia(this._elementRef, origin ?? null);
	}
}
