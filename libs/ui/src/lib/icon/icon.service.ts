import { Inject, Injectable } from "@angular/core";
import { IconName, SvgIconsConfig, SVG_ICONS_CONFIG } from "./icon.types";

class SvgIcon {
	appliedAttributes = false;
	constructor(
		public content: string
	) {}
}

@Injectable()
export class SvgIconRegistry {

	private _svgMap = new Map<string, SvgIcon>();
	private _XMLSerializer: XMLSerializer | undefined;

	constructor(@Inject(SVG_ICONS_CONFIG) config: SvgIconsConfig) {
		if (config.icons) {
			this.register(config.icons);
		}
	}

	private get _lazyXMLSerializer(): XMLSerializer {
		if (!this._XMLSerializer) {
			this._XMLSerializer = new XMLSerializer();
		}

		return this._XMLSerializer;
	}

	getAll(): Map<string, SvgIcon> {
		return this._svgMap;
	}

	get(
		key: string,
		options = { setDimensions: true }
	): string|null {
		let svg = this._svgMap.get(key);
		if (!svg) return null;

		if (!svg.appliedAttributes) {
			this.addAttributes(svg, options);
		}

		return svg.content;
	}

	register(svg: Record<IconName, string>): void {
		for (let [key, content] of Object.entries(svg)) {
			this.addIcon(key, content);
		}
	}

	getSvgElement(iconName: string): SVGSVGElement {
		let svgString = this.get(iconName);
		let div = document.createElement("DIV");
		div.innerHTML = svgString ?? "";
		let svg = div.querySelector("svg");

		if (!svg) throw Error("<svg> tag not found");

		return svg;
	}

	hasSvg(key: string): boolean {
		return this._svgMap.has(key);
	}

	private addAttributes(
		config: SvgIcon,
		options: { setDimensions: boolean; }
	): void {
		let svg = this.toElement(config.content);
		this.setAttributes(svg, options);
		config.content = this.asRaw(svg);
		config.appliedAttributes = true;
	}

	private asRaw(svgElement: SVGElement): string {
		let content = svgElement.outerHTML;

		// Handle IE11
		if (content === undefined) {
			content = this._lazyXMLSerializer.serializeToString(svgElement);
		}

		return content;
	}

	private toElement(content: string): SVGElement {
		let div = document.createElement("DIV");
		div.innerHTML = content;
		let svgElement = div.querySelector("svg") as SVGElement;

		if (!svgElement) throw Error("<svg> tag not found");

		return svgElement;
	}

	private setAttributes(
		svg: SVGElement,
		options: { setDimensions: boolean; }
	): SVGElement {
		if (!svg.getAttribute("xmlns")) {
			svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
		}

		svg.setAttribute("fit", "");

		if (options.setDimensions) {
			svg.setAttribute("height", "100%");
			svg.setAttribute("width", "100%");
		}

		svg.setAttribute("preserveAspectRatio", "xMidYMid meet");

		// Disable IE11 default behavior to make SVGs focusable.
		svg.setAttribute("focusable", "false");

		return svg;
	}

	private addIcon(name: string, data: string): void {
		let config = new SvgIcon(data);
		this._svgMap.set(name, config);
	}
}
