import { InjectionToken } from "@angular/core";
import { ICONS } from "./icon-definitions";

export interface SvgIconsConfig {
	icons?: typeof ICONS;
	color?: string;
	sizes: {
		xs?: string;
		sm?: string;
		md?: string;
		lg?: string;
	};
}
export const SVG_ICONS_CONFIG = new InjectionToken("SVG_ICONS_CONFIG");

export type IconName = keyof typeof ICONS;
export type IconSize = keyof SvgIconsConfig["sizes"];
