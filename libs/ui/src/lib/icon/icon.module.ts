import { NgModule } from "@angular/core";

import { ICONS } from "./icon-definitions";
import { IconComponent } from "./icon.component";
import { SvgIconRegistry } from "./icon.service";
import { SVG_ICONS_CONFIG } from "./icon.types";

@NgModule({
	declarations: [
		IconComponent,
	],
	exports: [
		IconComponent,
	],
	providers: [
		{
			provide: SVG_ICONS_CONFIG,
			useValue: { icons: ICONS },
		},
		SvgIconRegistry,
	],
})
export class IconModule { }
