import { SpectatorHost, createHostFactory } from "@ngneat/spectator/jest";

import { IconComponent } from "../icon.component";
import { IconModule } from "../icon.module";

describe("SvgIconComponent", () => {
	let spectator: SpectatorHost<IconComponent>;
	let createHost = createHostFactory({
		component: IconComponent,
		imports: [IconModule],
		declareComponent: false,
	});

	it("should render the icon", () => {
		spectator = createHost(`
			<ui-icon icon="MoreVertical"></ui-icon>
		`);

		expect(spectator.element).toExist();
		expect(spectator.element).toHaveDescendant("svg");
	});

	it("should support size", () => {
		spectator = createHost(`
			<ui-icon icon="MoreVertical" size="lg"></ui-icon>
		`);

		expect(spectator.element).toHaveStyle({ fontSize: "24px" });
	});

	it("should support dynamic", () => {
		let spectator = createHost(`
			<ui-icon [icon]="icon"></ui-icon>
		`, {
			hostProps: { icon: "MoreVertical" },
		});
		spectator.detectComponentChanges();
		expect(spectator.element).toHaveClass("ui-icon--more-vertical");

		spectator.setHostInput({ icon: "ChevronRightSmall" });
		expect(spectator.element).toHaveClass(`ui-icon--chevron-right-small`);
		expect(spectator.element).not.toHaveClass("ui-icon--more-vertical");
	});
});
