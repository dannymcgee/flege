import {
	ChangeDetectionStrategy,
	Component,
	ElementRef,
	HostBinding,
	Inject,
	Input,
	isDevMode,
	ViewEncapsulation,
} from "@angular/core";

import { a11y } from "@flege/styles";
import { camelToKebabCase } from "@flege/utils";
import { SvgIconRegistry } from "./icon.service";
import {
	IconName,
	IconSize,
	SvgIconsConfig,
	SVG_ICONS_CONFIG,
} from "./icon.types";

@Component({
	selector: "ui-icon",
	template: "",
	styleUrls: ["./icon.component.scss"],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {

	@Input()
	get icon(): IconName | undefined { return this._icon; }
	set icon(value: IconName | undefined) {
		this._icon = value;
		if (value) this.render();
	}
	private _icon?: IconName;

	@Input() size: IconSize = "md";

	@HostBinding("class")
	get hostClasses(): string[] {
		let base = "ui-icon";
		let iconClass = this.icon
			? `ui-icon--${camelToKebabCase(this.icon)}`
			: "";

		return [base, iconClass];
	}

	@HostBinding("attr.aria-hidden")
	readonly ariaHidden = "true";

	@HostBinding("style.fontSize")
	get fontSize(): string | null {
		return this._mergedConfig.sizes[this.size] ?? null;
	}

	private _mergedConfig: SvgIconsConfig;
	private get _element(): HTMLElement {
		return this._elementRef.nativeElement;
	}

	constructor(
		private _elementRef: ElementRef<HTMLElement>,
		private _registry: SvgIconRegistry,
		@Inject(SVG_ICONS_CONFIG) private _config: SvgIconsConfig,
	) {
		this._mergedConfig = this.createConfig();
	}

	private render(): void {
		if (this.icon && this._registry.hasSvg(this.icon)) {
			this._element.innerHTML = this._registry.get(this.icon)!;
		} else if (isDevMode()) {
			console.warn(`⚠️ ${this.icon} is missing!`);
		}
	}

	private createConfig(): SvgIconsConfig {
		let defaults: SvgIconsConfig = {
			sizes: {
				xs: a11y.rem(16),
				sm: a11y.rem(18),
				md: a11y.rem(20),
				lg: a11y.rem(24),
			},
		};

		return {
			...defaults,
			...this._config,
		};
	}
}
