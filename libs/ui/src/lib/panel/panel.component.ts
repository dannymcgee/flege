import {
	Component,
	ViewEncapsulation,
	ChangeDetectionStrategy,
	Input,
	Output,
	EventEmitter,
	HostBinding,
	Directive,
	TemplateRef,
	ContentChildren,
	AfterContentInit,
	OnDestroy,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { QueryList$ } from "@flege/utils";
import { Subject } from "rxjs";
import { startWith, takeUntil } from "rxjs/operators";

@Directive({
	selector: "[uiPanelWidget]"
})
export class PanelWidgetDirective {
	@Input("uiPanelWidget") widgetName: string;

	constructor(
		public _template: TemplateRef<void>,
	) {}
}

@Component({
	selector: "ui-panel",
	templateUrl: "./panel.component.html",
	styleUrls: ["./panel.component.scss"],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PanelComponent implements OnChanges, AfterContentInit, OnDestroy {
	@HostBinding("class") hostClass = "ui-panel";

	@Input() widgets: readonly string[];

	@Input() activeWidget: string;
	@Output() activeWidgetChange = new EventEmitter<string>();

	@Input() locked: boolean;
	@Output() lockedChange = new EventEmitter<boolean>();

	activeTemplate: TemplateRef<void>|null = null;

	@ContentChildren(PanelWidgetDirective)
	_panelWidgets: QueryList$<PanelWidgetDirective>;

	private _onDestroy$ = new Subject<void>();

	ngOnChanges(changes: SimpleChanges): void {
		if (!this._panelWidgets) return;
		if (!changes.activeWidget) return;

		this.activeTemplate = this._panelWidgets
			.find(w => w.widgetName === this.activeWidget)
			?._template
			?? null;
	}

	ngAfterContentInit(): void {
		this._panelWidgets.changes.pipe(
			startWith(this._panelWidgets),
			takeUntil(this._onDestroy$),
		)
			.subscribe(widgets => {
				if (!this.activeWidget) return;

				this.activeTemplate = widgets
					.find(w => w.widgetName === this.activeWidget)
					?._template
					?? null;
			});
	}

	ngOnDestroy(): void {
		this._onDestroy$.next();
		this._onDestroy$.complete();
	}
}
