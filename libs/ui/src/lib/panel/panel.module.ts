import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IconModule } from "../icon";

import { PanelComponent, PanelWidgetDirective } from "./panel.component";


@NgModule({
	imports: [
		CommonModule,
		IconModule,
	],
	declarations: [
		PanelComponent,
		PanelWidgetDirective,
	],
	exports: [
		PanelComponent,
		PanelWidgetDirective,
	]
})
export class PanelModule { }
