import {
	ChangeDetectionStrategy,
	Component,
	ElementRef,
	EventEmitter,
	HostBinding,
	HostListener,
	Input,
	OnDestroy,
	Output,
	ViewChild,
	ViewEncapsulation,
} from "@angular/core";

import { clamp, Coerce } from "@flege/utils";
import { animationFrameScheduler, fromEvent, merge, Subject } from "rxjs";
import { map, takeUntil, throttleTime } from "rxjs/operators";
import { Stepper } from "./stepper";

@Component({
	selector: "ui-float-control",
	templateUrl: "./float-control.component.html",
	styleUrls: ["./float-control.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None,
})
export class FloatControlComponent implements OnDestroy {
	@HostBinding("class") hostClass = "ui-float-control";

	@Input() @Coerce(Number) min: any = null;
	@Input() @Coerce(Number) max: any = null;

	@HostBinding("class.ui-float-control--readonly")
	@Input() @Coerce(Boolean) readonly: any = false;

	@Input("aria-label") ariaLabel: string|null = null;

	@Input() value: number;
	@Output() valueChange = new EventEmitter<number>();

	step = new Stepper([1, 5, 10], 1);

	@ViewChild("adjustmentHandle", { static: true })
	adjustmentHandle: ElementRef<HTMLElement>;

	private _onDestroy$ = new Subject<void>();

	ngOnDestroy(): void {
		this._onDestroy$.next();
		this._onDestroy$.complete();
	}

	@HostListener("window:keydown.control", ["$event"])
	@HostListener("window:keyup.shift", ["$event"])
	decStep(event: KeyboardEvent) {
		if (event.repeat) return;
		this.step.dec();
	}

	@HostListener("window:keydown.shift", ["$event"])
	@HostListener("window:keyup.control", ["$event"])
	incStep(event: KeyboardEvent) {
		if (event.repeat) return;
		this.step.inc();
	}

	startMouseAdjustment(event: PointerEvent): void {
		event.preventDefault();

		let { nativeElement: element } = this.adjustmentHandle;
		element.requestPointerLock();

		fromEvent<PointerEvent>(element, "pointermove").pipe(
			throttleTime(0, animationFrameScheduler),
			map(({ movementX, movementY }) => ({ dx: movementX, dy: movementY })),
			takeUntil(merge(
				fromEvent(element, "pointerup"),
				this._onDestroy$,
			)),
		).subscribe({
			next: ({ dx }) => {
				let change = dx / screen.width * this.step.get();
				let value = this.value! + change;

				if (this.min != null && this.max != null) {
					value = clamp(value, [this.min, this.max]);
				};

				if (value !== this.value) {
					this.valueChange.emit(value);
					this.value = value;
				}
			},
			complete: () => document.exitPointerLock(),
		});
	}
}
