import {
	ChangeDetectionStrategy,
	Component,
	HostBinding,
	Input,
	ViewEncapsulation,
} from "@angular/core";

import { elementId } from "@flege/utils";

@Component({
	selector: "ui-legend",
	templateUrl: "./legend.component.html",
	styleUrls: ["./legend.component.scss"],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegendComponent {
	@HostBinding("class") hostClass = "ui-legend";

	@HostBinding()
	@Input() id = elementId("legend");
}
