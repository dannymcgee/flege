import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { FieldsetComponent } from "./fieldset/fieldset.component";
import { LegendComponent } from "./legend/legend.component";
import { FloatControlComponent } from "./float-control/float-control/float-control.component";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
	],
	declarations: [
		FieldsetComponent,
		LegendComponent,
		FloatControlComponent,
	],
	exports: [
		FieldsetComponent,
		LegendComponent,
		FloatControlComponent,
	],
})
export class UiFormsModule {}
