import {
	ChangeDetectionStrategy,
	Component,
	ContentChild,
	HostBinding,
	OnInit,
	ViewEncapsulation,
} from "@angular/core";
import { LegendComponent } from "../legend/legend.component";

@Component({
	selector: "ui-fieldset",
	templateUrl: "./fieldset.component.html",
	styleUrls: ["./fieldset.component.scss"],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class FieldsetComponent implements OnInit {
	@HostBinding("class") hostClass = "ui-fieldset";

	@HostBinding("attr.role") readonly role = "group";
	@HostBinding("attr.aria-labelledby") ariaLabelledBy: string;

	@ContentChild(LegendComponent, { static: true })
	legend: LegendComponent;

	ngOnInit(): void {
		if (this.legend) {
			this.ariaLabelledBy = this.legend.id;
		}
	}
}
