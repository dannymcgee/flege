use anyhow::Result;
use api::{request, response, MessageType};
use bevy::{
	prelude::*,
	reflect::{Reflect, TypeRegistry},
	scene::serde::SceneSerializer,
};
use serde::Serialize;

use crate::{ipc::IpcSender, patch_values::MatchQuery, PatchValues};

pub fn generate_scene(world: &mut World) {
	let registry = world.get_resource::<TypeRegistry>().unwrap();
	let scene = DynamicScene::from_world(world, registry);
	let content = serialize_json(SceneSerializer::new(&scene, registry)).unwrap();
	let tx = world.get_resource::<IpcSender>().unwrap();

	tx.send(&response::Header {
		msg_type: MessageType::SceneDescription as i32,
	});
	tx.send(&response::SceneDescription { content });
}

pub fn patch_values(
	mut events: EventReader<request::PatchValue>,
	mut query: Query<(Entity, &mut Transform)>,
) {
	for evt in events.iter() {
		for (_, mut transform) in query
			.iter_mut()
			.filter(|(ent, cmp)| evt.matches(ent, cmp.type_name()))
		{
			transform.patch(&evt.values);
		}
	}
}

fn serialize_json<S>(serialize: S) -> Result<String, serde_json::Error>
where S: Serialize {
	serde_json::to_string(&serialize)
}
