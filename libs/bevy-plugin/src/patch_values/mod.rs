use api::request::{self, PatchedValue};
use bevy::prelude::Entity;
use std::collections::HashMap;

mod transform;
pub use transform::*;

pub trait PatchValues {
	fn patch(&mut self, values: &HashMap<String, PatchedValue>);
}

pub trait FromBytes {
	fn from_bytes(bytes: &[u8]) -> Self;
}

pub trait MatchQuery {
	fn matches(&self, entity: &Entity, type_name: &str) -> bool;
}

impl MatchQuery for request::PatchValue {
	fn matches(&self, entity: &Entity, type_name: &str) -> bool {
		entity.id() == self.entity && type_name == self.component
	}
}
