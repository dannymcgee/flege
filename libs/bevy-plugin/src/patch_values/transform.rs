use std::collections::HashMap;

use api::request::PatchedValue;
use bevy::{
	math::{Quat, Vec3},
	prelude::Transform,
};
use bytes::Buf;

use crate::{FromBytes, PatchValues};

impl PatchValues for Transform {
	fn patch(&mut self, values: &HashMap<String, PatchedValue>) {
		for (field_name, v) in values.iter() {
			match &field_name[..] {
				"translation" => {
					self.translation = Vec3::from_bytes(&v.value);
				}
				"rotation" => {
					self.rotation = Quat::from_bytes(&v.value);
				}
				"scale" => {
					self.scale = Vec3::from_bytes(&v.value);
				}
				_ => unreachable!(),
			}
		}
	}
}

impl FromBytes for Vec3 {
	fn from_bytes(bytes: &[u8]) -> Self {
		let x = (&bytes[..4]).get_f32();
		let y = (&bytes[4..8]).get_f32();
		let z = (&bytes[8..12]).get_f32();

		Vec3::new(x, y, z)
	}
}

impl FromBytes for Quat {
	fn from_bytes(bytes: &[u8]) -> Self {
		let x = (&bytes[..4]).get_f32();
		let y = (&bytes[4..8]).get_f32();
		let z = (&bytes[8..12]).get_f32();
		let w = (&bytes[12..16]).get_f32();

		Quat::from_xyzw(x, y, z, w)
	}
}
