use api::request;
use bevy::prelude::*;

use win::FlegeWindowHandle;

mod ipc;
mod patch_values;
mod scene;
mod win;

pub use patch_values::{FromBytes, PatchValues};

#[derive(Debug)]
pub struct FlegeViewportPlugin;

impl Plugin for FlegeViewportPlugin {
	fn build(&self, app: &mut AppBuilder) {
		app.add_event::<request::SetViewportRect>()
			.add_event::<request::PatchValue>()
			.insert_resource(FlegeWindowHandle(None))
			.insert_resource(WindowDescriptor {
				decorations: false,
				..Default::default()
			})
			.add_startup_system(ipc::startup.system())
			.add_system(ipc::recv_ipc_messages.system().label("ipc-input"))
			.add_system_set(
				SystemSet::new()
					.after("ipc-input")
					.before("ipc-output")
					.with_system(win::update_window_rect.system())
					.with_system(win::return_focus_to_flege.system()),
			)
			.add_system_set(
				SystemSet::new()
					.label("ipc-output")
					.with_system(win::send_bevy_window_handle.system()),
			);
	}
}

#[derive(Debug)]
pub struct FlegeEditorPlugin;

impl Plugin for FlegeEditorPlugin {
	fn build(&self, app: &mut AppBuilder) {
		app.add_system(scene::patch_values.system())
			.add_startup_stage_after(
				StartupStage::Startup,
				"Editor",
				SystemStage::single_threaded(),
			)
			.add_startup_system_to_stage(
				"Editor",
				scene::generate_scene.exclusive_system(),
			);
	}
}
