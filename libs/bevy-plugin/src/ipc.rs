use anyhow::{bail, Result};
use bevy::prelude::{Commands, EventWriter, Res, ResMut};
use bytes::{BufMut, BytesMut};
use prost::Message;
use std::{
	io::{self, ErrorKind, Read, Write},
	process,
	sync::{Arc, Mutex},
	thread,
};

use api::{request, MessageType};

use crate::win::FlegeWindowHandle;

const MSB: u8 = 1 << 7;

#[derive(Debug, Copy, Clone)]
pub struct IpcSender;

impl IpcSender {
	pub fn send<M: Message>(&self, msg: &M) {
		let mut bytes = vec![];
		let mut stdout = io::stdout();

		msg.encode_length_delimited(&mut bytes).unwrap();

		stdout.lock().write_all(&bytes).unwrap();
		stdout.flush().unwrap();
	}
}

#[derive(Default, Debug)]
pub struct IpcReceiver {
	set_viewport_rect: Arc<Mutex<Vec<request::SetViewportRect>>>,
	window_handle: Arc<Mutex<Vec<request::WindowHandle>>>,
	pub(crate) patch_value: Arc<Mutex<Vec<request::PatchValue>>>,
	stdin_buffer: Arc<Mutex<BytesMut>>,
}

impl Clone for IpcReceiver {
	fn clone(&self) -> Self {
		IpcReceiver {
			set_viewport_rect: Arc::clone(&self.set_viewport_rect),
			window_handle: Arc::clone(&self.window_handle),
			patch_value: Arc::clone(&self.patch_value),
			stdin_buffer: Arc::clone(&self.stdin_buffer),
		}
	}
}

impl IpcReceiver {
	pub fn new() -> Self {
		let rx = IpcReceiver {
			stdin_buffer: Arc::new(Mutex::new(BytesMut::with_capacity(1024))),
			..Default::default()
		};

		rx.process_stdin();
		rx.process_messages();

		rx
	}

	fn process_stdin(&self) {
		let rx = self.clone();

		thread::spawn(move || loop {
			let mut buffer = [0u8; 16];

			match io::stdin().lock().read(&mut buffer) {
				Ok(n) if n > 0 => {
					let mut stdin_buffer = rx.stdin_buffer.lock().unwrap();
					stdin_buffer.put(&buffer[..n]);
				}
				Err(err) if err.kind() != ErrorKind::Interrupted => {
					eprintln!("read error: {:#?}", err);
					process::exit(1);
				}
				_ => {}
			}
		});
	}

	fn process_messages(&self) {
		let rx = self.clone();

		thread::spawn(move || loop {
			debug::log_heading("Header");
			let msg_type: MessageType = rx.recv::<request::Header>().msg_type.into();

			match msg_type {
				MessageType::Exit => {
					eprintln!("Rceived Exit message");
					process::exit(0);
				}
				MessageType::SetViewportRect => {
					debug::log_heading("SetViewportRect");
					let msg = rx.recv::<request::SetViewportRect>();
					rx.set_viewport_rect.lock().unwrap().push(msg);
				}
				MessageType::WindowHandle => {
					debug::log_heading("WindowHandle");
					let msg = rx.recv::<request::WindowHandle>();
					rx.window_handle.lock().unwrap().push(msg);
				}
				MessageType::PatchValue => {
					debug::log_heading("PatchValue");
					let msg = rx.recv::<request::PatchValue>();
					rx.patch_value.lock().unwrap().push(msg);
				}
				other => panic!("Unrecognized MessageType: {:?}", other),
			}
		});
	}

	fn recv<M>(&self) -> M
	where M: Message + Default {
		let msg_len = loop {
			let mut stdin_buffer = self.stdin_buffer.lock().unwrap();
			let mut len = 0;

			for (idx, byte) in stdin_buffer.iter().enumerate() {
				if (byte & MSB) == 0 {
					len = idx + 1;
					break;
				}
			}

			let varint = stdin_buffer.split_to(len);

			if let Ok(msg_len) = read_varint(&varint) {
				debug::bytes("varint", &varint);
				break msg_len;
			}
		};

		let mut message = loop {
			let mut stdin_buffer = self.stdin_buffer.lock().unwrap();

			if stdin_buffer.len() >= msg_len {
				break stdin_buffer.split_to(msg_len);
			}
		};

		debug::bytes("messsage", &message);

		M::decode(&mut message).unwrap()
	}
}

fn read_varint(buf: &[u8]) -> Result<usize> {
	if buf.is_empty() {
		bail!("varint buffer is empty");
	}

	Ok(buf.iter().enumerate().fold(0, |acc, (idx, cur)| {
		let byte = (*cur & !MSB) as usize;
		acc + (byte << (7 * idx))
	}))
}

pub fn startup(mut commands: Commands) {
	commands.insert_resource(IpcSender);
	commands.insert_resource(IpcReceiver::new());
}

pub fn recv_ipc_messages(
	mut set_viewport_rect: EventWriter<request::SetViewportRect>,
	mut patch_value: EventWriter<request::PatchValue>,
	mut window_handle: ResMut<FlegeWindowHandle>,
	rx: Res<IpcReceiver>,
) {
	for rect in rx.set_viewport_rect.lock().unwrap().drain(..) {
		set_viewport_rect.send(rect);
	}
	for evt in rx.patch_value.lock().unwrap().drain(..) {
		patch_value.send(evt);
	}
	for msg in rx.window_handle.lock().unwrap().drain(..) {
		window_handle.0 = Some(msg.handle);
	}
}

#[allow(dead_code, unused_variables)]
mod debug {

	#[cfg(debug_assertions)]
	pub fn bytes<'b, I>(label: &'static str, bytes: I)
	where I: IntoIterator<Item = &'b u8> {
		let mut msg = format!("{:12} : ", label);
		let bytes = super::fmt::bytes(bytes)
			.split(' ')
			.map(|s| s.to_string())
			.collect::<Vec<_>>()
			.chunks(16)
			.collect::<Vec<_>>()
			.iter()
			.map(|chunk| chunk.to_vec().join(" "))
			.enumerate()
			.map(|(idx, line)| {
				if idx == 0 {
					line
				} else {
					format!("{:15}{}", " ", line)
				}
			})
			.collect::<Vec<_>>()
			.join("\n");

		msg.push_str(&bytes);

		eprintln!("{}", msg);
	}

	#[cfg(not(debug_assertions))]
	pub fn bytes<'b, I>(label: &'static str, bytes: I)
	where I: IntoIterator<Item = &'b u8> {
		// noop
	}

	#[cfg(debug_assertions)]
	pub fn log_heading(title: &'static str) {
		eprintln!();
		let t = format!("-- {} --", title);
		let msg = format!("{:-<80}", t);

		eprintln!("{}", msg);
	}

	#[cfg(not(debug_assertions))]
	pub fn log_heading(title: &'static str) {
		// noop
	}
}

#[allow(dead_code)]
mod fmt {
	pub fn bits(b: &u8) -> String {
		format!("{:#010b}", b)
	}

	pub fn bytes<'b, I>(bytes: I) -> String
	where I: IntoIterator<Item = &'b u8> {
		bytes
			.into_iter()
			.map(|b| (&format!("{:#04x}", b)[2..]).to_string())
			.collect::<Vec<String>>()
			.join(" ")
	}
}
