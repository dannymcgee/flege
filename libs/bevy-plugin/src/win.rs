use bevy::{
	prelude::{EventReader, IVec2, Res, ResMut},
	window::{WindowCreated, WindowFocused, Windows},
	winit::WinitWindows,
};
use libc::c_void;
use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};

use crate::ipc::IpcSender;
use api::{request, response, MessageType};

pub struct FlegeWindowHandle(pub Option<u32>);

/// Updates the window position and size when a `SetViewportRect` event is received
pub fn update_window_rect(
	mut set_viewport_rect: EventReader<request::SetViewportRect>,
	mut windows: ResMut<Windows>,
) {
	let rect = match set_viewport_rect.iter().last() {
		Some(evt) => evt,
		None => {
			return;
		}
	};

	if let Some(window) = windows.get_primary_mut() {
		window.set_position(IVec2::new(rect.x, rect.y));
		window.set_resolution(rect.width as f32, rect.height as f32);
	}
}

/// Returns window focus back to the front-end when the window is first created
/// and whenever it receives focus thereafter
pub fn return_focus_to_flege(
	mut window_focused: EventReader<WindowFocused>,
	mut window_created: EventReader<WindowCreated>,
	handle: Res<FlegeWindowHandle>,
) {
	if handle.0.is_some()
		&& (window_created.iter().last().is_some()
			|| window_focused.iter().last().is_some())
	{
		unsafe {
			ffi::SetForegroundWindow(handle.0.unwrap() as *mut c_void);
		}
	}
}

/// Sends our native window handle to the front-end when the window is first created
pub fn send_bevy_window_handle(
	mut window_focused: EventReader<WindowFocused>,
	windows: Res<WinitWindows>,
	tx: Res<IpcSender>,
) {
	let window = match window_focused.iter().last() {
		Some(evt) => windows.get_window(evt.id).unwrap(),
		None => {
			return;
		}
	};
	let handle = match window.raw_window_handle() {
		RawWindowHandle::Windows(handle) => handle.hwnd as u32,
		_ => {
			return;
		}
	};

	tx.send(&response::Header {
		msg_type: MessageType::WindowHandle as i32,
	});
	tx.send(&response::WindowHandle { handle });
}

mod ffi {
	use libc::c_void;

	extern crate libc;

	#[link(name = "user32")]
	extern "stdcall" {
		pub fn SetForegroundWindow(hwnd: *mut c_void);
	}
}
